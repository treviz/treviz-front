#!/bin/sh

echo "Building Treviz frontend"
echo "==============="
echo ""

export LANG=${LANG:="en"}

if [ $LANG = "fr" ]
then
    echo "Building the application in french"
    mv ./src/assets/info.fr.md ./src/assets/info.md
    mv ./src/assets/home.fr.md ./src/assets/home.md
    npm run build-fr
    mv ./dist/fr-FR/* ./dist
else
    echo "No language is specified, building the application in english"
    rm ./src/assets/info.fr.md
    rm ./src/assets/home.fr.md
    npm run build
fi
