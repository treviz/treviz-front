# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.8.0] - 2020-12-31
### Added
- Allow redirect links to open up the notification dialog in community and project details
- Users can specify their organization and create one if none matches

### Fixed
- Correctly display the "discard candidacy" button
- Correctly translates the "Quit" job button
- Change the way project invitations are displayed in the project detail page

## [0.7.0] (2020-08-22)
## Added
- Allow user to quit projects and jobs
- Brainstorming sessions can now be closed
- Uploaded images intended for thumbnails are resized to reduce their weight
- Project, community thumbnails and avatars now work as links

### Fix
- Fixed missing translations

## [0.6.1] (2020-06-23)
### Style
- Increase job creation dialog size on large displays

### Removed
- Removed references to crypto assets and rewards

## [0.6.0] (2020-06-21)
### Added
- Filter communities by name
- Add a component to upload a file by drag-n-drop, or by browsing specific mime-types.
- Display a load spinner when the application is being downloaded

### Changed
- Cache all the application using the Service Worker
- Prevent Service Worker from catching SSE requests for notifications.

### Style
- Make the application more mobile-friendly
- Only display "My communities" if the user has at least one. The same goes for projects and jobs.

## [0.5.6] (2020-04-24)
### Changed
- Migrate to Angular 9
- Refactored chat component
- Handle real time chat update and message deletion

### Fixed
- Correctly unsubscribe from observables; enhance memory usage

## [0.5.5] - (2020-02-04)
### Changed
- Fonts are now fetched from assets instead of google servers

### Fixed
- Spelling mistakes in home/info french translations.

## [0.5.4] - (2019-10-27)
### Added
- Users can search for specific communities when updating projects
- Users can delete jobs
- Columns now have a maximum height
- Prevent user from typing too long task names
- Search communities when creating of updating projects
- Users can delete project and community roles

### Fixed
- Correctly paginate users in admin module

## [0.5.3] (2019-09-14)
### Refactor
- Allow API Urls to be defined at runtime
- Use Gitlab CI/CD to build and push images to registry

### Fixed
- Bug preventing users from creating an account

## [0.5.2] (2019-07-29)
### Added
- Username and email validation on registration form
- In chat room list, displays the first letter of the project/community if no logo is found

### Changed
- Load all user personal communities in search section (up to 1000)
- Load communities per group of 20

### Fixed
- Correctly displays the chat rooms for communities

## [0.5.1] (2019-03-24)
### Changed
- Projects and communities the user is member of are not displayed in the "available" section
- Enhance error message on login, registration, confirmation and password reset failures

## Fixed
- Prevent the community page from crashing if a role is not correctly set for a membership

## [0.5.0] (2019-02-10)
### Added
* Users can to load more posts, only display a limited number of comments
* Handle notification preferences
* Display terms of service and Privacy Notice
* Users can to be linked to organizations
* Administrators can white/black list domains for registration
* Administrators can user accounts

## [0.4.6] (2018-12-03)
### Added
* Display an error message when a call to create/update/delete some entity fails
* Display a welcome dialog on first connection

### Removed
* Remove the monthly reward form field for jobs

### Fixed
* Disable buttons when waiting for server response on multiple forms and pages
* Create a chat room with a user from his/her profile
* Display candidacies and invitations on home page
* Better error handling

## [0.4.5] (2018-11-09)
### Fixed
* Only display notification on navbar if it was not already handled by a displayed component

## 0.4.4 (2018-11-02)
### Fixed
* Use EventSource for server-sent notification instead of websockets
* Display error message on log-in failure

## 0.4.3 (2018-10-07)
### Added
* New board view
* New display for Sidenav on large screens
* Adds service worker to make Treviz a PWA
* Projects can be forked

### Fixed
* Open communities can be joined without prior candidacies
* When a candidacy is accepted, the memberships of a community are correctly updated
* When a task is updated, it does not disappear from the kanban
* Project and Community memberships can have their role updated
* When switching between different roles of a project/community, the checkboxes are correctly updated

### Chore
* Migrate the application to Angular 6
* Use Docker for Deployment
* Community candidacies can be accepted from the main community page

### Fixed
* User candidacies now display correctly on home screen
* Community admins can now correctly access the team management option

## 0.4.2 (2018-04-22)
### Fixed (3 change)
* Allow scroll on sidenav on small screens
* Sets a default background for community thumbnails when no logo is specified
* Updated Changelog with guidelines for fixes

## 0.4.1 (2018-01-15)
### Added (1 change)
* Add a Changelog

### Fixed (2 changes)
* Fix user list component so that newly loaded users do not overwrite the previously loaded ones
* Fix sidenav so that it does not open on application start when the user is not yet logged in
