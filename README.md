Treviz Frontend 
========================

![Treviz Logo](https://gitlab.com/treviz/treviz-back/-/raw/dev/web/images/treviz-logo.png)

## Description

Treviz is an open-source web platform that helps you set up and join open, collaborative organizations, in which you 
can work on the projects you want and get rewarded for your work.

This repository contains the code of the frontend of the platform. It is developed around [Angular 9](https://angular.io), with the
[Angular Material components](http://material.angular.io/), and is design to interact with the [Treviz API](https://github.com/trevizxyz/treviz-back).

Once installed and configured, it grants access to the following features:
* User registration and account management
* Manage skills and domains of interests (referred to as "tags")
* Launch communities in which users can discuss, share documents and ideas
* Create brainstorming sessions
* Launch and manage projects
* Manages tasks thanks to a Kanban
* Create "jobs" linked to projects
* Post news and comments
* Chat

Treviz is still in beta, so use it carefully.

## Installation

See our [documentation](https://doc.treviz.xyz) to learn more about how to install Treviz on your
own server.

If you do not have a web server you can host Treviz on, feel free to checkout out [our hosting offers](https://treviz.xyz#four)
and [contact us](https://treviz.xyz/contact) if you are interested. We'll grant you a 30 day free trial.

## Setup the development environment

**Requirements**
* NodeJS >= 10.0 with NPM
* Angular-Cli >= 7.2

Simply clone the repository, and execute these commands:
```
npm install
```

If you want to develop on your local machine, specify the url of the API you want to connect
to in the `src/environment/environment.ts` file, and run the stand-alone web server with
```
ng serve
```

## Contributing

Check out our [Contributing Guide](./CONTRIBUTING.md) if you are interested in joining the development team, if you want to submit a bug, etc.

## Public instances

The list of public Treviz instances is available on [our website](https://treviz.xyz/instances). If you want to
list your, simply add it to the form, we'll check it out and display it!

## Privacy

Privacy is one of our main concerns when it comes to developing Treviz. We do not sell any information about our users, nor
do we try to analyze their behaviour. If a security breach was to be found, we would hunt it down and fix it as soon as we could.

## Contact

You can learn more about Treviz on:
* our website: [treviz.xyz](https://treviz.xyz)
* our documentation: [doc.treviz.xyz](https://doc.treviz.xyz)
* our blog: [blog.treviz.xyz](https://blog.treviz.xyz)

Feel free to [contact us](https://treviz.xyz/contact) if you have any question.

## To Do:
* Write test cases
* Implement OAuth 2.0 with OpenID Connect frameworks for Identification, authentication and authorization.
