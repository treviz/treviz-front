function generateArray (acc: Array<any>, iterator: IterableIterator<any>): Array<any> {
  const element = iterator.next();
  if (element.done) {
    return acc;
  }
  acc.push(element.value);
  return generateArray(acc, iterator);
}

export function mapToArray (map: Map<any, any>): Array<any> {
  return generateArray([], map.values());
}

/**
 * Updates a map by incrementing a specific value. If none is already set, it creates a new entry.
 * @param map Map to update
 * @param key Key to update
 * @param increment Value that should be added to the map
 */
export function incrementMapValues (map: Map<string, number>, key: string, increment: number): void {
  map.set(key, (map.get(key) ?? 0) + (increment ?? 0));
}

export function reduceToNumber (map: Map<any, number>): number {
  return mapToArray(map).reduce((acc, el) => acc + el, 0);
}
