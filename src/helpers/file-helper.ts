export function resizeImage(image: File, maxWidth: number): Promise<File> {
  return new Promise<File>((resolve, reject) => {
    try {
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        const resizedImage = new Image();
        resizedImage.onload = () => {
          const canvas = document.createElement('canvas');
          const ctx = canvas.getContext('2d');
          canvas.width = maxWidth > resizedImage.width ? resizedImage.width : maxWidth;
          canvas.height = canvas.width * resizedImage.height / resizedImage.width;
          ctx.drawImage(resizedImage, 0, 0, canvas.width, canvas.height);
          ctx.canvas.toBlob((blob) => {
            const file = new File([blob], image.name, {
              type: 'image/jpeg',
              lastModified: Date.now()
            });
            resolve(file);
          }, 'image/jpeg');
        };
        resizedImage.src = (e.target as any).result;
      };

      fileReader.readAsDataURL(image);
    } catch (error) {
      reject(error);
    }
  });
}
