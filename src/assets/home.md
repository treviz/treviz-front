Treviz
====

[Treviz](https://treviz.xyz) is an open-source web platform that helps you set up and join open,
collaborative organizations, in which you can work on the projects you want and get rewarded for
your work.

It comes with all the tools you need to work on projects, from a brainstorming module to a kanban
board, a news feed, an inner chat, a cloud for documents, while allowing you to recruit new members
for your teams, and much more.
