Treviz
====

[Treviz](https://treviz.xyz) est une application Open Source vous permettant de créer et de rejoindre
des organisations ouvertes et collaboratives, dans lesquelles vous pouvez travailler sur les projets
qui vous intéressent et être récompensé pour votre travail.

Elle intègre tous les outils nécessaires à la gestion de projet, que ce soit un module de brainstorming,
des tableaux kanban, un fil d'actualité, un chat interne, un cloud pour vos documents... tout en vous
permettant de recruter de nouveaux membres dans vos équipes, et bien plus encore.
