export const environment = {
  production: true,
  apiHost: '$API_HOST',
  apiUrl: 'https://$API_HOST/v1',
  notificationApiUrl: 'https://$NOTIFICATION_API_HOST'
};
