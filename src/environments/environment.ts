export const environment = {
  production: false,
  apiHost: 'localhost:8000',
  apiUrl: 'http://localhost:8000/v1',
  notificationApiUrl: 'https://events.preprod.treviz.org'
};
