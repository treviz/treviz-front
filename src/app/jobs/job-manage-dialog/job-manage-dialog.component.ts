import {Component, Inject, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {ProjectJob} from '../../shared/models/project/project-job.model';
import {ProjectJobDto} from '../../shared/models/project/project-job.model.dto';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProjectJobService} from '../../core/services/project/project-job.service';
import {ProjectCandidacyService} from '../../core/services/project/project-candidacy.service';
import {ProjectCandidacy} from '../../shared/models/project/project-candidacy.model';
import {SkillService} from '../../core/services/skill.service';
import {TagService} from '../../core/services/tag.service';
import {User} from '../../shared/models/users/user.model';
import {UserService} from '../../core/services/user.service';
import {debounceTime, distinctUntilChanged, finalize, take, takeUntil} from 'rxjs/operators';
import {AbstractTagSkillComponent} from '../../shared/abstract/abstract-tag-skill-component';

@Component({
  selector: 'app-job-manage-dialog',
  templateUrl: 'job-manage-dialog.component.html',
  styleUrls: ['job-manage-dialog.component.scss']
})
export class JobManageDialogComponent extends AbstractTagSkillComponent implements OnInit {
  public readonly jobDto: ProjectJobDto;
  public jobSubmitted = false;
  public candidacies: ProjectCandidacy[] = [];
  public holder: User;
  public filteredHolder: User[];
  public holderCtrl: FormControl;
  public contact: User;
  public filteredContact: User[];
  public contactCtrl: FormControl;

  constructor (private dialogRef: MatDialogRef<JobManageDialogComponent>,
              private projectJobService: ProjectJobService,
              private userService: UserService,
              protected skillService: SkillService,
              protected tagService: TagService,
              private projectCandidacyService: ProjectCandidacyService,
              @Inject(MAT_DIALOG_DATA) public data: ProjectJob) {
    super(skillService, tagService);

    this.jobDto = ProjectJobDto.fromProjectJob(data);
    this.holder = data.holder;
    this.contact = data.contact;
    this.holderCtrl = new FormControl();
    this.holderCtrl.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        name => {
          this.filteredHolder = [];
          if (name != null && name.length >= 2) {
            this.userService.getUsers({ name: name })
              .subscribe(
                users => this.filteredHolder = users,
                err => console.log(err)
              );
          }
        }
      );

    this.contactCtrl = new FormControl();
    this.contactCtrl.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        name => {
          this.filteredContact = [];
          if (name != null && name.length >= 2) {
            this.userService.getUsers({ name: name })
              .pipe(takeUntil(this.destroyed$))
              .subscribe(
                users => this.filteredContact = users,
                err => console.log(err)
              );
          }
        }
      );
  }

  ngOnInit () {
    this.projectCandidacyService.getProjectCandidacies({ job: this.data.hash })
      .pipe(take(1), takeUntil(this.destroyed$))
      .subscribe(
        candidacies => this.candidacies = candidacies,
        err => console.log(err)
      );
  }

  setSkills (skills: string[]) {
    this.jobDto.skills = skills;
  }

  setTags (tags: string[]) {
    this.jobDto.tags = tags;
  }

  setHolder (username: string): void {
    const holder = this.filteredHolder.find(user => user.username === username);
    if (holder) {
      this.holder = holder;
      this.jobDto.holder = username;
    }
  }

  removeHolder (): void {
    this.holder = null;
    this.jobDto.holder = undefined;
  }

  setContact (username: string): void {
    const contact = this.filteredContact.find(user => user.username === username);
    if (contact) {
      this.contact = contact;
      this.jobDto.contact = username;
    }
  }

  removeContact (): void {
    this.contact = null;
    this.jobDto.contact = undefined;
  }

  removeCandidacy (candidacy: ProjectCandidacy): void {
    this.candidacies.splice(this.candidacies.indexOf(candidacy), 1);
  }

  onSubmit () {
    this.jobSubmitted = true;
    this.projectJobService.putProjectJob(this.data.hash, this.jobDto)
      .pipe(
        finalize(() => this.jobSubmitted = false),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        job => this.dialogRef.close(job),
        () => this.dialogRef.close(false)
      );
  }

  onDelete () {
    this.jobSubmitted = true;
    this.projectJobService.deleteProjectJob(this.data.hash)
      .pipe(
        finalize(() => this.jobSubmitted = false),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        () => this.dialogRef.close(null),
        () => this.dialogRef.close(false)
      );
  }
}
