import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ProjectJob } from '../../shared/models/project/project-job.model';
import { ProjectJobService } from '../../core/services/project/project-job.service';
import { finalize, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { SearchFilters } from '../../shared/filters/filter.component';

@Component({
  selector: 'app-job-list',
  templateUrl: 'job-list.component.html',
  styleUrls: ['job-list.component.scss']
})
export class JobListComponent implements OnInit, OnDestroy {
  jobs: ProjectJob[] = [];
  personalJobs: ProjectJob[] = [];

  private params = {
    holder: '',
    tags: [],
    skills: [],
    attributed: false,
    name: '',
    nb: 20,
    offset: 0
  };

  personalJobsFetched = false;
  jobsFetched = false;
  moreToLoad = false;

  private destroyed$ = new Subject<void>();

  constructor (private jobService: ProjectJobService,
              public dialog: MatDialog) {}

  ngOnInit (): void {
    this.getJobs(false);

    this.jobService.getJobs({ attributed: true, holder: localStorage.getItem('username') })
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        data => this.personalJobs = data,
        err => console.log(err),
        () => this.personalJobsFetched = true
      );
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  search (params: SearchFilters) {
    this.params.name = params.name;
    this.params.skills = params.skills;
    this.params.tags = params.tags;
    this.getJobs(false);
  }

  loadMore () {
    if (this.jobsFetched) {
      this.getJobs(true);
    }
  }

  getJobs (append = true) {
    this.jobsFetched = false;
    if (append) {
      this.params.offset += this.jobs.length;
    }
    this.jobService.getJobs(this.params)
      .pipe(
        finalize(() => this.jobsFetched = true),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        data => {
          if (append) {
            for (const job of data) {
              this.jobs.push(job);
            }
          } else {
            this.jobs = data;
          }
        },
        err => console.log(err),
        () => this.jobsFetched = true
      );
  }
}
