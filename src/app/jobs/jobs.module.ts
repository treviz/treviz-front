import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

import { JobsRoutingModule } from './jobs-routing.module';
import { JobListComponent } from './job-list/job-list.component';
import { JobDetailComponent } from './job-detail/job-detail.component';
import { JobManageDialogComponent } from './job-manage-dialog/job-manage-dialog.component';

@NgModule({
  imports: [
    SharedModule,
    JobsRoutingModule
  ],
  declarations: [
    JobDetailComponent,
    JobListComponent,
    JobManageDialogComponent
  ],
  exports: [],
  providers: []
})
export class JobsModule { }
