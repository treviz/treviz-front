import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {UserStateService} from '../../core/services/user-state.service';
import {ProjectJobService} from '../../core/services/project/project-job.service';
import {ProjectJob} from '../../shared/models/project/project-job.model';
import {ProjectCandidacyService} from '../../core/services/project/project-candidacy.service';
import {ProjectCandidacy} from '../../shared/models/project/project-candidacy.model';
import {CandidacyDialogComponent} from '../../shared/candidacy/candidacy-dialog/candidacy-dialog.component';
import {JobManageDialogComponent} from '../job-manage-dialog/job-manage-dialog.component';
import {MessageDialogComponent} from '../../shared/dialogs/message-dialog/message-dialog.component';
import {finalize, take, takeUntil} from 'rxjs/operators';
import {Location} from '@angular/common';
import {AbstractProjectConfirmComponent} from "../../shared/abstract/abstract-project-confirm-component";
import {ConfirmMessageModel} from "../../shared/models/confirm-message.model";

@Component({
  selector: 'app-job-detail',
  templateUrl: 'job-detail.component.html',
  styleUrls: ['job-detail.component.scss']
})
export class JobDetailComponent extends AbstractProjectConfirmComponent implements OnInit, OnDestroy {
  public job: ProjectJob;
  public jobFetched = false;
  public candidaciesFetched = false;
  public userCandidacy: ProjectCandidacy;
  public candidacies: ProjectCandidacy[];
  public get canEdit(): boolean {
    return this.job.contact?.username === localStorage.getItem('username')
    || (this.currentUserService.getCurrentUser()
        .projectMemberships
        ?.find(project => project.hash === this.job?.project?.hash)
        ?.role
        ?.permissions
        ?.some(permission => permission === 'MANAGE_JOBS') ?? false);
  }
  public get isHolder(): boolean {
    return this.job?.holder?.username === localStorage.getItem('username');
  }

  constructor (private jobService: ProjectJobService,
              private currentUserService: UserStateService,
              private projectCandidacyService: ProjectCandidacyService,
              private route: ActivatedRoute,
              private location: Location,
              public dialog: MatDialog) {
    super(dialog);
  }

  ngOnInit () {
    this.fetchData();
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  /**
   * Fetches the job identified by the current route.
   * Once the job is fetches, looks what kind of action the user should be displayed with:
   * manage applications, candidate, edit...
   */
  fetchData () {
    this.jobFetched = false;

    this.route.params
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        params => {
          const hash = params.hash;

          this.jobService.getProjectJob(hash)
            .pipe(
              finalize(() => this.jobFetched = true),
              takeUntil(this.destroyed$)
            )
            .subscribe(
              data => {
                this.job = data;

                /*
                 * Fetches the applications for this job.
                 * If the user cannot edit the job, and if no candidacy is found for him, he can candidate.
                 */
                const query = {
                  job: hash,
                  user: null
                };
                if (!this.canEdit) { query.user = localStorage.getItem('username'); }
                this.projectCandidacyService.getProjectCandidacies(query)
                  .pipe(
                    finalize(() => this.candidaciesFetched = true),
                    takeUntil(this.destroyed$)
                  )
                  .subscribe(
                    candidacies => {
                      if (this.canEdit) {
                        this.candidacies = candidacies;
                      } else if (candidacies.length > 0) {
                        this.userCandidacy = candidacies[0];
                      }
                    },
                    err => console.log(err)
                  );
              },
              err => console.log(err)
            );
        }
      );
  }

  openCandidacyDialog () {
    const config = new MatDialogConfig();
    config.data = {
      job: this.job
    };

    const dialogRef = this.dialog.open(CandidacyDialogComponent, config);

    dialogRef.afterClosed()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(result => {
        if (result) {
          this.userCandidacy = result;
        }
      });
  }

  openJobManagementDialog () {
    const config = new MatDialogConfig();
    config.data = this.job;

    const dialogRef = this.dialog.open(JobManageDialogComponent, config);

    dialogRef.afterClosed()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(result => {
        if (null != result) {
          if (result) {
            this.job = result;
          } else {
            this.displayErrorDialog('An error occurred while updating the job');
          }
        } else if (null === result) {
          this.location.back();
        }
      });
  }

  removeCandidacy (): void {
    this.projectCandidacyService.deleteProjectCandidacy(this.userCandidacy.hash)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        () => this.userCandidacy = null,
        err => console.log(err)
      );
  }

  displayErrorDialog (message: string) {
    const config = new MatDialogConfig();
    config.data = {
      message,
      failure: true,
      actionRequired: true
    };

    this.dialog.open(MessageDialogComponent, config);
  }

  quitJob() {
    this.confirmAction(
      ConfirmMessageModel.QUIT_JOB,
      () => this
        .jobService
        .quitJob(this.job.hash)
        .pipe(
          take(1),
        )
        .subscribe(() => this.job.holder = null)
    );
  }
}
