import { Component, OnDestroy } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { UserService } from '../../core/services/user.service';
import { ProjectService } from '../../core/services/project/project.service';
import { Project } from '../../shared/models/project/project.model';
import { User } from '../../shared/models/users/user.model';
import { debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-boards-search-dialog',
  templateUrl: 'boards-search-dialog.component.html',
  styleUrls: ['boards-search-dialog.component.scss']
})
export class BoardsSearchDialogComponent implements OnDestroy {
    public searchCtrl: FormControl;
    private destroyed$ = new Subject<void>();
    public filteredProjects: Project[] = [];
    public filteredUsers: User[] = [];
    public usersFetched = false;
    public projectsFetched = false;

    constructor (public dialogRef: MatDialogRef<BoardsSearchDialogComponent>,
                private userService: UserService,
                private projectService: ProjectService) {
      this.searchCtrl = new FormControl();
      this.searchCtrl.valueChanges
        .pipe(
          takeUntil(this.destroyed$),
          debounceTime(300),
          distinctUntilChanged()
        )
        .subscribe(
          input => {
            this.usersFetched = false;
            this.projectsFetched = false;
            this.userService.getUsers({ name: input })
              .pipe(takeUntil(this.destroyed$))
              .subscribe(
                users => this.filteredUsers = users,
                err => console.log(err),
                () => this.usersFetched = true
              );
            this.projectService.getProjects({ name: input })
              .pipe(takeUntil(this.destroyed$))
              .subscribe(
                users => this.filteredProjects = users,
                err => console.log(err),
                () => this.projectsFetched = true
              );
          }
        );
    }

    ngOnDestroy () {
      this.destroyed$.next();
      this.destroyed$.complete();
    }

    select (result: Project | User) {
      this.dialogRef.close(result);
    }
}
