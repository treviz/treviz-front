import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { BoardsRoutingModule } from './boards-routing.module';
import { BoardsComponent } from './boards.component';
import { BoardsSearchDialogComponent } from './boards-search-dialog/boards-search-dialog.component';

@NgModule({
  imports: [
    SharedModule,
    BoardsRoutingModule
  ],
  declarations: [
    BoardsComponent,
    BoardsSearchDialogComponent
  ],
  exports: [],
  providers: []
})
export class BoardsModule { }
