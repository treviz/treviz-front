import { Component, OnInit, OnDestroy } from '@angular/core';
import { BoardService } from '../core/services/kanban/board.service';
import { UserStateService } from '../core/services/user-state.service';
import { Board } from '../shared/models/kanban/board.model';
import { Project } from '../shared/models/project/project.model';
import { MatDialog } from '@angular/material/dialog';
import { BoardsSearchDialogComponent } from './boards-search-dialog/boards-search-dialog.component';
import { User } from '../shared/models/users/user.model';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-boards',
  templateUrl: 'boards.component.html',
  styleUrls: ['./boards.component.scss']
})
export class BoardsComponent implements OnInit, OnDestroy {
    private destroyed$ = new Subject<void>();
    public boardsFetched = false;
    public personalBoardsFetched = false;
    public selectedElementBoardsFetched = false;
    public query = false;
    public groupedPersonalBoards: {project: Project; boards: Board[]}[] = [];
    public groupedBoards: {project: Project; boards: Board[]}[] = [];
    public selectedElement: {project?: Project; boards: Board[]; user?: User} = null;

    constructor (public dialog: MatDialog,
                private boardsService: BoardService,
                private currentUserService: UserStateService) {}

    ngOnInit () {
      // Firstly, fetch the boards of a user
      this.boardsService.getBoards({ user: this.currentUserService.getCurrentUser().username })
        .pipe(takeUntil(this.destroyed$))
        .subscribe(
          boards => {
            // Group those boards according to their project
            boards.forEach((board: Board) => {
              if (this.groupedPersonalBoards.some((element) => element.project.hash === board.project.hash)) {
                this.groupedPersonalBoards.find((element) => element.project.hash === board.project.hash).boards.push(board);
              } else {
                this.groupedPersonalBoards.push({
                  project: board.project,
                  boards: [board]
                });
              }
            });

            // Then fetch all the boards, group them, and removes those in which the user takes part.
            this.boardsService.getBoards({})
              .subscribe(
                generalBoards => generalBoards.forEach((board: Board) => {
                  if (!boards.some((personalBoard) => personalBoard.hash === board.hash)) {
                    if (this.groupedBoards.some((element) => element.project.hash === board.project.hash)) {
                      this.groupedBoards.find((element) => element.project.hash === board.project.hash)
                        .boards.push(board);
                    } else {
                      this.groupedBoards.push({
                        project: board.project,
                        boards: [board]
                      });
                    }
                  }
                }),
                err => console.log(err),
                () => this.boardsFetched = true
              );
          },
          err => console.log(err),
          () => this.personalBoardsFetched = true
        );
    }

    ngOnDestroy () {
      this.destroyed$.next();
      this.destroyed$.complete();
    }

    openSearchDialog (): void {
      const dialogRef = this.dialog.open(BoardsSearchDialogComponent);

      dialogRef.afterClosed()
        .pipe(takeUntil(this.destroyed$))
        .subscribe(
          result => {
            if (result == null) { return; }
            if (result.firstName != null) {
              // The result is a user
              this.loadBoard({ user: result });
            } else if (result.hash != null) {
              // The result is a project
              this.loadBoard({ project: result });
            }
          }
        );
    }

    clearSearch (): void {
      this.selectedElement = null;
    }

    loadBoard ({ project = null, user = null }: { project?: Project; user?: User }): void {
      this.selectedElement = { project, user, boards: [] };
      this.selectedElementBoardsFetched = false;
      const params = { project: '', user: '' };
      if (null != project) { params.project = project.hash; }
      if (null != user) { params.user = user.username; }
      this.boardsService.getBoards(params)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(
          boards => this.selectedElement.boards = boards,
          err => console.log(err),
          () => this.selectedElementBoardsFetched = true
        );
    }
}
