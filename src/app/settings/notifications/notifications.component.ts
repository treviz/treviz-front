import { Component, OnDestroy } from '@angular/core';
import { UserPreferences } from '../../shared/models/users/user-preferences.model';
import { UserPreferencesService } from '../../core/services/user-preferences.service';
import { UserStateService } from '../../core/services/user-state.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { finalize, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-settings-notifications',
  templateUrl: 'notifications.component.html',
  styleUrls: ['notifications.component.scss']
})
export class NotificationsComponent implements OnDestroy {
    public userPreferences = new UserPreferences();
    public loading = true;
    private username: string;
    private destroyed$ = new Subject<void>();

    constructor (private userPreferencesService: UserPreferencesService,
                private currentUserService: UserStateService,
                public snackBar: MatSnackBar) {
      this.username = currentUserService.getCurrentUser().username;
      this.userPreferencesService.getUserPreferences(this.username)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(
          data => {
            this.userPreferences = data;
            this.loading = false;
          },
          err => console.log(err)
        );
    }

    ngOnDestroy () {
      this.destroyed$.next();
      this.destroyed$.complete();
    }

    public updateUserPreferences () {
      this.loading = true;
      this.userPreferencesService.updatePreferences(this.username, this.userPreferences)
        .pipe(
          finalize(() => this.loading = false),
          takeUntil(this.destroyed$)
        )
        .subscribe(
          () => {
            this.snackBar.open('Your preferences have been updated !', 'Close', { duration: 3000 });
          },
          err => {
            console.error(err);
            this.snackBar.open('We could not update your preferences, please try again later.', 'Close', { duration: 3000 });
          }
        );
    }
}
