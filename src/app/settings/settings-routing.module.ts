import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SettingsComponent } from './settings.component';
import { AccountPreferencesComponent } from './account-preferences/account-preferences.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { AccountsComponent } from './accounts/accounts.component';
import { OrganizationsComponent } from './organizations/organizations.component';
import { MailDomainsComponent } from './mail-domains/mail-domains.component';
import { AdminGuard } from '../core/services/auth/admin.guard';

export const routes: Routes = [
  {
    path: '',
    component: SettingsComponent,
    children: [
      { path: '', component: AccountPreferencesComponent },
      { path: 'account', component: AccountPreferencesComponent },
      { path: 'notifications', component: NotificationsComponent },
      { path: 'accounts', component: AccountsComponent, canActivate: [AdminGuard] },
      { path: 'organizations', component: OrganizationsComponent, canActivate: [AdminGuard] },
      { path: 'domains', component: MailDomainsComponent, canActivate: [AdminGuard] }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
