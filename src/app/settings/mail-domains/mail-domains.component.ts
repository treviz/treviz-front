import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MailDomainService } from '../../core/services/mail-domain.service';
import { finalize, take, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ConfirmDialogComponent } from '../../shared/dialogs/confirm-dialog/confirm-dialog.component';
import { MessageDialogComponent } from '../../shared/dialogs/message-dialog/message-dialog.component';
import { PlatformService } from '../../core/services/platform.service';
import MailDomain from '../../shared/mail-domain.model';
import { ConfirmMessageModel } from '../../shared/models/confirm-message.model';

@Component({
  selector: 'app-mail-domains',
  templateUrl: './mail-domains.component.html',
  styleUrls: ['./mail-domains.component.scss']
})
export class MailDomainsComponent implements OnInit, OnDestroy {
  public mailDomains: MailDomain[] = [];
  public mailDomain: MailDomain = { domain: '' };
  public indexesToEdit = [];
  public mailDomainsToEdit: MailDomain[] = [];
  public loading = false;
  public createModeEnabled = false;
  private destroyed$ = new Subject<void>();

  constructor (public snackBar: MatSnackBar,
              public dialog: MatDialog,
              public platformService: PlatformService,
              private mailDomainService: MailDomainService) { }

  ngOnInit () {
    this.loadMailDomains();
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  toggleCreateMode () {
    this.createModeEnabled = !this.createModeEnabled;
  }

  createMailDomain () {
    this.loading = true;
    this.mailDomainService.postMailDomains(this.mailDomain)
      .pipe(
        take(1),
        finalize(() => this.loading = false),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        data => {
          this.mailDomain.domain = '';
          this.mailDomains.push(data);
          this.toggleCreateMode();
        },
        () => this.snackBar.open('An error occurred while creating the mailDomain.', '', { duration: 3000 })
      );
  }

  isMailDomainEdited (index: number): boolean {
    return this.indexesToEdit[index];
  }

  toggleEditMode (index?: number) {
    if (!this.isMailDomainEdited(index)) {
      this.indexesToEdit[index] = true;
      this.mailDomainsToEdit[index] = { domain: '' };
      this.mailDomainsToEdit[index].domain = this.mailDomains[index].domain;
    } else {
      delete this.mailDomainsToEdit[index];
      this.indexesToEdit[index] = false;
    }
  }

  editMailDomain (index: number) {
    this.loading = true;
    const name = this.mailDomains[index].domain;
    this.mailDomainService
      .putMailDomain(name, this.mailDomainsToEdit[index])
      .pipe(
        take(1),
        finalize(() => {
          this.loading = false;
          this.toggleEditMode(index);
        }),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        data => this.mailDomains[index] = data,
        () => this.snackBar.open('An error occurred while updating the mailDomain.', '', { duration: 3000 })
      );
  }

  openDeleteDialog (index: number) {
    const name = this.mailDomains[index].domain;
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { data: ConfirmMessageModel.MAIL_DOMAIN_DELETE });

    dialogRef.afterClosed()
      .pipe(
        take(1),
        takeUntil(this.destroyed$)
      )
      .subscribe(result => {
        if (result) {
          this.loading = true;
          this.mailDomainService.deleteMailDomain(name)
            .pipe(
              take(1),
              takeUntil(this.destroyed$),
              finalize(() => this.loading = false)
            )
            .subscribe(
              () => this.mailDomains.splice(index, 1),
              () => this.snackBar.open('An error occurred while deleting this mailDomain.', '', { duration: 3000 })
            );
        }
      });
  }

  displayErrorDialog (message: string) {
    const config = new MatDialogConfig();
    config.data = {
      message,
      failure: true,
      actionRequired: true
    };

    this.dialog.open(MessageDialogComponent, config);
  }

  private loadMailDomains () {
    this.loading = true;
    this.mailDomainService.getMailDomains()
      .pipe(
        take(1),
        takeUntil(this.destroyed$),
        finalize(() => this.loading = false)
      )
      .subscribe(
        data => this.mailDomains = data,
        () => this.snackBar.open('An error occurred while fetching the mailDomains.', '', { duration: 3000 })
      );
  }
}
