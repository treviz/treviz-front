import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserStateService } from '../core/services/user-state.service';
import { PlatformService } from '../core/services/platform.service';

@Component({
  selector: 'app-settings',
  templateUrl: 'settings.component.html',
  styleUrls: ['settings.component.scss']
})
export class SettingsComponent {
  get isAdmin () {
    return this.currentUserService.isOrganizationAdmin();
  }

  constructor (public platformService: PlatformService,
              private router: Router,
              private currentUserService: UserStateService) { }

  goTo (route: string): void {
    this.router.navigate([`/settings/${route}`]);
  }

  isActive (name: string): boolean {
    return this.router.url === `/settings${name}`;
  }
}
