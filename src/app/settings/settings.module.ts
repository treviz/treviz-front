// Angular Imports
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

// This Module's Components
import { AccountsComponent } from './accounts/accounts.component';
import { AccountCreationDialogComponent } from './account-creation-dialog/account-creation-dialog.component';
import { AccountPreferencesComponent } from './account-preferences/account-preferences.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsComponent } from './settings.component';

// Other UI Components
import { MatSidenavModule } from '@angular/material/sidenav';
import { OrganizationsComponent } from './organizations/organizations.component';
import { MailDomainsComponent } from './mail-domains/mail-domains.component';

@NgModule({
  imports: [
    MatSidenavModule,
    SharedModule,
    SettingsRoutingModule
  ],
  declarations: [
    SettingsComponent,
    NotificationsComponent,
    AccountPreferencesComponent,
    AccountsComponent,
    AccountCreationDialogComponent,
    OrganizationsComponent,
    MailDomainsComponent
  ],
  exports: []
})
export class SettingsModule {}
