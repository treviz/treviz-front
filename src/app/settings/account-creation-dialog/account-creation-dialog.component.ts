import { Component, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AuthService } from '../../core/services/auth/auth.service';
import { UserRoles } from '../../shared/models/users/user.model';
import { UserDto } from '../../shared/models/users/user.model.dto';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-account-creation-dialog',
  templateUrl: './account-creation-dialog.component.html',
  styleUrls: ['./account-creation-dialog.component.scss']
})
export class AccountCreationDialogComponent implements OnDestroy {
  public user = new UserDto();
  public admin = false;
  public submitted = false;
  private destroyed$ = new Subject<void>();

  constructor (public dialogRef: MatDialogRef<AccountCreationDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: string,
              public auth: AuthService) { }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  onSubmit () {
    if (this.admin) {
      this.user.roles = [UserRoles.ROLE_ADMIN];
    } else {
      this.user.roles = null;
    }

    this.submitted = true;

    this.auth.register(this.user)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        data => this.dialogRef.close(data),
        () => this.dialogRef.close(false)
      );
  }
}
