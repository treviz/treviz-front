import { Component, OnDestroy, OnInit } from '@angular/core';
import { User, UserRoles } from '../../shared/models/users/user.model';
import { UserService } from '../../core/services/user.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subject } from 'rxjs';
import { UserEditDialogComponent } from '../../shared/users/user-edit-dialog/user-edit-dialog.component';
import { ConfirmDialogComponent } from '../../shared/dialogs/confirm-dialog/confirm-dialog.component';
import { AccountCreationDialogComponent } from '../account-creation-dialog/account-creation-dialog.component';
import { MessageDialogComponent } from '../../shared/dialogs/message-dialog/message-dialog.component';
import { debounceTime, distinctUntilChanged, finalize, takeUntil } from 'rxjs/operators';
import { ConfirmMessageModel } from '../../shared/models/confirm-message.model';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss']
})
export class AccountsComponent implements OnInit, OnDestroy {
  public searchUserStream = new Subject<string>();
  public users: User[] = [];
  public name = '';
  public displayProgressBar = false;
  public index = 0;

  get canGoBack () {
    return this.index > 0;
  }

  get canGoFurther () {
    return (this.displayedUsers.length % this.NB_DISPLAYED_USERS === 0 && this.index < this.users.length);
  }

  get displayedUsers (): User[] {
    return this.users.slice(this.index, this.index + this.NB_DISPLAYED_USERS);
  }

  private destroyed$ = new Subject<void>();
  private readonly NB_DISPLAYED_USERS = 10;

  constructor (public snackBar: MatSnackBar,
              public dialog: MatDialog,
              private userService: UserService) {
    this.searchUserStream
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        term => {
          this.name = term;
          this.loadUsers();
        }
      );
  }

  ngOnInit () {
    this.loadUsers();
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  openEditDialog (index: number) {
    const config = new MatDialogConfig();
    const username = this.users[this.index + index].username;

    this.userService.getUser(username)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        user => {
          config.data = {
            user: user,
            admin: true
          };

          const dialogRef = this.dialog.open(UserEditDialogComponent, config);

          dialogRef.afterClosed()
            .pipe(takeUntil(this.destroyed$))
            .subscribe(result => {
              if (result != null) {
                if (result) {
                  this.users[this.index + index] = result;
                } else {
                  this.snackBar.open('An error occurred while uploading the user.', '', { duration: 3000 });
                }
              }
            });
        },
        () => this.snackBar.open('An error occurred while fetching this users\'s information', '', { duration: 3000 })
      );
  }

  openDeleteDialog (index: number) {
    const userIndex = this.index + index;

    const dialogRef = this.dialog.open(ConfirmDialogComponent, { data: ConfirmMessageModel.USER_DELETE });

    dialogRef.afterClosed()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(result => {
        if (result) {
          this.userService.deleteUser(this.users[userIndex].username)
            .pipe(takeUntil(this.destroyed$))
            .subscribe(
              () => this.users.splice(userIndex, 1),
              () => this.snackBar.open('An error occurred while deleting this user.', '', { duration: 3000 })
            );
        }
      });
  }

  openAccountCreationDialog () {
    const dialogRef = this.dialog.open(AccountCreationDialogComponent);

    dialogRef.afterClosed()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        data => {
          if (data != null) {
            if (data) {
              this.users.unshift(data);
            } else {
              this.displayErrorDialog('An error occurred while creating the user');
            }
          }
        }
      );
  }

  displayErrorDialog (message: string) {
    const config = new MatDialogConfig();
    config.data = {
      message,
      failure: true,
      actionRequired: true
    };

    this.dialog.open(MessageDialogComponent, config);
  }

  isAdmin (user: User): boolean {
    return user.roles.some((role) => role === UserRoles.ROLE_ADMIN);
  }

  search (term: string) {
    this.searchUserStream.next(term);
  }

  goBack () {
    this.index = this.index - this.NB_DISPLAYED_USERS;
  }

  goFurther () {
    this.index = this.index + this.NB_DISPLAYED_USERS;

    if (this.users.length <= this.index) {
      this.displayProgressBar = true;
      this.userService.getUsers({ offset: this.index, name: this.name, nb: this.NB_DISPLAYED_USERS })
        .pipe(
          finalize(() => this.displayProgressBar = false),
          takeUntil(this.destroyed$)
        )
        .subscribe(
          data => {
            this.users = this.users.concat(data);
          },
          () => this.snackBar.open('An error occurred while fetching the users.', '', { duration: 3000 })
        );
    }
  }

  private loadUsers () {
    this.index = 0;
    this.displayProgressBar = true;
    this.userService.getUsers({ offset: this.index, name: this.name, nb: this.NB_DISPLAYED_USERS })
      .pipe(
        finalize(() => this.displayProgressBar = false),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        data => {
          this.users = data;
        },
        () => this.snackBar.open('An error occurred while fetching the users.', '', { duration: 3000 })
      );
  }
}
