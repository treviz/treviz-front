import { Component, OnDestroy } from '@angular/core';
import { UserService } from '../../core/services/user.service';
import { UserStateService } from '../../core/services/user-state.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConfirmDialogComponent } from '../../shared/dialogs/confirm-dialog/confirm-dialog.component';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ConfirmMessageModel } from '../../shared/models/confirm-message.model';

@Component({
  selector: 'app-account-preferences',
  templateUrl: 'account-preferences.component.html',
  styleUrls: ['account-preferences.component.scss']
})
export class AccountPreferencesComponent implements OnDestroy {
  private destroyed$ = new Subject<void>();

  constructor (public dialog: MatDialog,
              public snackBar: MatSnackBar,
              private userService: UserService,
              private router: Router,
              private currentUserService: UserStateService) {}

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  downloadArchive () {
    this.userService.getArchive(this.currentUserService.getCurrentUser().username)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        data => {
          const blob = new Blob([JSON.stringify(data)], { type: 'application/json' });
          const url = window.URL.createObjectURL(blob);
          window.open(url);
        },
        () => this.snackBar.open('An error occurred while creating your personal archive.', '', { duration: 3000 })
      );
  }

  deleteAccount () {
    const username = this.currentUserService.getCurrentUser().username;

    const dialogRef = this.dialog.open(ConfirmDialogComponent, { data: ConfirmMessageModel.ACCOUNT_DELETE });

    dialogRef.afterClosed()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(result => {
        if (result) {
          this.userService.deleteUser(username)
            .pipe(takeUntil(this.destroyed$))
            .subscribe(
              () => this.router.navigate(['/logout']),
              () => this.snackBar.open('An error occurred while deleting this user.', '', { duration: 3000 })
            );
        }
      });
  }
}
