import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConfirmDialogComponent } from '../../shared/dialogs/confirm-dialog/confirm-dialog.component';
import { MessageDialogComponent } from '../../shared/dialogs/message-dialog/message-dialog.component';
import Organization from '../../shared/models/organization.model';
import { OrganizationService } from '../../core/services/organization.service';
import { finalize, take, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ConfirmMessageModel } from '../../shared/models/confirm-message.model';

@Component({
  selector: 'app-organizations',
  templateUrl: './organizations.component.html',
  styleUrls: ['./organizations.component.scss']
})
export class OrganizationsComponent implements OnInit, OnDestroy {
  public organizations: Organization[] = [];
  public organization = new Organization();
  public indexesToEdit = [];
  public organizationsToEdit: Organization[] = [];
  public loading = false;
  public createModeEnabled = false;
  private destroyed$ = new Subject<void>();

  constructor (public snackBar: MatSnackBar,
              public dialog: MatDialog,
              private organizationService: OrganizationService) { }

  ngOnInit () {
    this.loadOrganizations();
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  toggleCreateMode () {
    this.createModeEnabled = !this.createModeEnabled;
  }

  createOrganization () {
    this.loading = true;
    this.organizationService.postOrganization(this.organization)
      .pipe(
        take(1),
        finalize(() => this.loading = false),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        data => {
          this.organization.name = '';
          this.organizations.push(data);
          this.toggleCreateMode();
        },
        () => this.snackBar.open('An error occurred while creating the organization.', '', { duration: 3000 })
      );
  }

  isOrganizationEdited (index: number): boolean {
    return this.indexesToEdit[index];
  }

  toggleEditMode (index?: number) {
    if (!this.isOrganizationEdited(index)) {
      this.indexesToEdit[index] = true;
      this.organizationsToEdit[index] = new Organization();
      this.organizationsToEdit[index].name = this.organizations[index].name;
    } else {
      delete this.organizationsToEdit[index];
      this.indexesToEdit[index] = false;
    }
  }

  editOrganization (index: number) {
    this.loading = true;
    const name = this.organizations[index].name;
    this.organizationService.putOrganization(name, this.organizationsToEdit[index])
      .pipe(
        takeUntil(this.destroyed$),
        finalize(() => {
          this.loading = false;
          this.toggleEditMode(index);
        })
      )
      .subscribe(
        data => this.organizations[index] = data,
        () => this.snackBar.open('An error occurred while updating the organization.', '', { duration: 3000 })
      );
  }

  openDeleteDialog (index: number) {
    const name = this.organizations[index].name;

    const dialogRef = this.dialog.open(ConfirmDialogComponent, { data: ConfirmMessageModel.ORGANIZATION_DELETE });

    dialogRef.afterClosed()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(result => {
        if (result) {
          this.loading = true;
          this.organizationService.deleteOrganization(name)
            .pipe(
              takeUntil(this.destroyed$),
              finalize(() => this.loading = false)
            )
            .subscribe(
              () => this.organizations.splice(index, 1),
              () => this.snackBar.open('An error occurred while deleting this organization.', '', { duration: 3000 })
            );
        }
      });
  }

  displayErrorDialog (message: string) {
    const config = new MatDialogConfig();
    config.data = {
      message,
      failure: true,
      actionRequired: true
    };

    this.dialog.open(MessageDialogComponent, config);
  }

  private loadOrganizations () {
    this.loading = true;
    this.organizationService.getOrganizations()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        data => this.organizations = data,
        () => this.snackBar.open('An error occurred while fetching the organizations.', '', { duration: 3000 }),
        () => this.loading = false
      );
  }
}
