import { Component, OnInit } from '@angular/core';
import { AuthService } from './core/services/auth/auth.service';
import { UserStateService } from './core/services/user-state.service';
import { NotificationService } from './core/services/notification.service';
import { PlatformService } from './core/services/platform.service';
import { DispatcherService } from './core/services/dispatcher.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  constructor (private auth: AuthService,
              private notificationService: NotificationService,
              private dispatcherService: DispatcherService,
              private currentUserService: UserStateService,
              private platformService: PlatformService
  ) {
    dispatcherService.init();
  }

  /*
   * If the user is already logged in, load the current user in the appropriate service and trigger notifications.
   */
  ngOnInit (): void {
    this.platformService.loadPlatformSettings();

    if (this.auth.loggedIn()) {
      this.currentUserService.loadCurrentUser();
    }
  }
}
