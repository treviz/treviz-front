import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// components that we will create routes for
import { AuthGuard } from './core/services/auth/auth.gard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('app/home/home.module').then(m => m.HomeModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'chat',
    loadChildren: () => import('app/chat/chat.module').then(m => m.ChatModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'communities',
    loadChildren: () => import('app/communities/communities.module').then(m => m.CommunitiesModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'jobs',
    loadChildren: () => import('app/jobs/jobs.module').then(m => m.JobsModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'projects',
    loadChildren: () => import('app/projects/projects.module').then(m => m.ProjectsModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'boards',
    loadChildren: () => import('app/boards/boards.module').then(m => m.BoardsModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'users',
    loadChildren: () => import('app/users/users.module').then(m => m.UsersModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'settings',
    loadChildren: () => import('app/settings/settings.module').then(m => m.SettingsModule),
    canActivate: [AuthGuard]
  },
  {
    path: '',
    loadChildren: () => import('app/account/account.module').then(m => m.AccountModule)
  },
  {
    path: '',
    loadChildren: () => import('app/error/error.module').then(m => m.ErrorModule),
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
