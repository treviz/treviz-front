import {NgModule} from '@angular/core'
import {JwtModule, JwtModuleOptions} from '@auth0/angular-jwt' // Authentication with JWT
import {environment} from 'environments/environment'

export function tokenGetter (): string {
  return localStorage.getItem('access_token')
}

export const jwtModuleConfig: JwtModuleOptions = {
  config: {
    tokenGetter: tokenGetter,
    skipWhenExpired: true,
    whitelistedDomains: [
      environment.apiHost,
    ],
    blacklistedRoutes: [
      `${environment.apiUrl}/login_check`,
      `${environment.apiUrl}/password`,
      `${environment.apiUrl}/reset-password`,
      `${environment.apiUrl}/reset-password`,
      new RegExp(`${environment.apiUrl}/users/[A-z-0-9]*/confirm`),
      new RegExp(`${environment.apiUrl}/users/[A-z-0-9]*/reset`)
    ]
  }
}

@NgModule({
  imports: [JwtModule.forRoot(jwtModuleConfig)],
  exports: [JwtModule]
})
export class AuthModule {}
