import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from '../../core/services/auth/auth.service';
import { ActivatedRoute } from '@angular/router';

/**
 * Created by Bastien on 20/03/2017.
 */
@Component({
  selector: 'app-reset-password',
  templateUrl: 'reset-password.component.html',
  styleUrls: ['../shared/not-logged-style.scss']
})
export class ResetPasswordComponent implements OnInit, OnDestroy {
  password1: '';
  password2: '';
  isSubmitted = false;
  displaySuccess = false;
  displayErrorMessage = false;
  message = '';
  invalidLink = false;
  invalidUser = false;
  expiredToken = false;
  defaultErrorMessage = false;

  private destroyed$ = new Subject<void>();

  constructor (public auth: AuthService,
    private route: ActivatedRoute) {}

  ngOnInit (): void {
    this.route.queryParams
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        params => {
          if (params.user == null || params.token == null) {
            this.displayErrorMessage = true;
            this.invalidLink = true;
          }
        }
      );
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  closeErrorMessage () {
    this.displayErrorMessage = false;
  }

  sendResetRequest () {
    this.isSubmitted = true;
    this.defaultErrorMessage = false;
    this.invalidLink = false;
    this.expiredToken = false;
    this.invalidUser = false;
    this.route.queryParams
      .pipe(takeUntil(this.destroyed$))
      .subscribe(params => {
        this.auth.sendResetPassword(params.user, params.token, this.password1).pipe(
          takeUntil(this.destroyed$))
          .subscribe(
            () => this.displaySuccess = true,
            error => {
              this.displayErrorMessage = true;
              this.isSubmitted = false;
              switch (error.status) {
                case 403:
                  this.invalidLink = true;
                  break;
                case 404:
                  this.invalidUser = true;
                  break;
                case 408:
                  this.expiredToken = true;
                  break;
                default:
                  this.defaultErrorMessage = true;
              }
            }
          );
      });
  }
}
