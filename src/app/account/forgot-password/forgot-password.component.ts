import { Component, OnDestroy } from '@angular/core';
import { AuthService } from '../../core/services/auth/auth.service';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

/**
 * Created by Bastien on 20/03/2017.
 */
@Component({
  selector: 'app-forgot-password',
  templateUrl: 'forgot-password.component.html',
  styleUrls: ['../shared/not-logged-style.scss']
})
export class ForgotPasswordComponent implements OnDestroy {
  email: '';
  displayErrorMessage = false;
  invalidEmail = false;
  isSubmitted = false;
  emailSent = false;
  private destroyed$ = new Subject<void>();

  constructor (public auth: AuthService,
              private router: Router) { }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  closeErrorMessage () {
    this.displayErrorMessage = false;
  }

  sendResetRequest () {
    this.isSubmitted = true;

    this.auth.sendResetRequest(this.email)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        () => this.emailSent = true,
        err => {
          this.isSubmitted = false;
          this.displayErrorMessage = true;
          this.invalidEmail = (err.status === 404);
        }
      );
  }

  goToLogin () {
    this.router.navigate(['/login']);
  }
}
