import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Component, OnDestroy } from '@angular/core';
import { AuthService } from '../../core/services/auth/auth.service';
import { UserStateService } from '../../core/services/user-state.service';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { NotificationService } from '../../core/services/notification.service';
import { PlatformService } from '../../core/services/platform.service';

@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['../shared/not-logged-style.scss']
})
export class LoginComponent implements OnDestroy {
  isSubmitted = false;
  invalidCredentials = false;
  displayErrorMessage = false;
  credentials = {
    username: '',
    password: ''
  };

  private destroyed$ = new Subject<void>();

  constructor (public auth: AuthService,
              public jwtHelper: JwtHelperService,
              public platformService: PlatformService,
              private router: Router,
              private notificationService: NotificationService,
              private currentUserService: UserStateService) {}

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  closeErrorMessage () {
    this.displayErrorMessage = false;
  }

  onLogin () {
    this.isSubmitted = true;

    this.auth.login(this.credentials.username, this.credentials.password)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        // We're assuming the response will be an object
        // with the JWT on an 'token' key
        data => {
          this.isSubmitted = false;
          localStorage.setItem('access_token', data.token);
          localStorage.setItem('username', this.jwtHelper.decodeToken(data.token).username);

          this.currentUserService.loadCurrentUser();
          this.router.navigate(['/']);
        },
        error => {
          this.displayErrorMessage = true;
          this.isSubmitted = false;
          this.invalidCredentials = (error.status === 401);
        }
      );
  }
}
