import { NgModule } from '@angular/core';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { RegisterConfirmationComponent } from './register-confirmation/register-confirmation.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { SharedModule } from '../shared/shared.module';
import { AccountRoutingModule } from './account-routing.module';

@NgModule({
  imports: [
    SharedModule,
    AccountRoutingModule
  ],
  exports: [
    ForgotPasswordComponent,
    LoginComponent,
    RegisterComponent,
    RegisterConfirmationComponent,
    ResetPasswordComponent
  ],
  declarations: [
    ForgotPasswordComponent,
    LoginComponent,
    RegisterComponent,
    RegisterConfirmationComponent,
    ResetPasswordComponent
  ],
  providers: []
})
export class AccountModule {
}
