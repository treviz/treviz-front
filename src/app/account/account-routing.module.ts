import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { RegisterComponent } from './register/register.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { RegisterConfirmationComponent } from './register-confirmation/register-confirmation.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'password-recover', component: ForgotPasswordComponent },
  { path: 'password-reset', component: ResetPasswordComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'confirm-registration', component: RegisterConfirmationComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule { }
