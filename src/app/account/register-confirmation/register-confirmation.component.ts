import { takeUntil, finalize } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from '../../core/services/auth/auth.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-register-confirmation',
  templateUrl: 'register-confirmation.component.html',
  styleUrls: ['../shared/not-logged-style.scss']
})
export class RegisterConfirmationComponent implements OnInit, OnDestroy {
  displayErrorMessage = false;
  displaySuccessMessage = false;
  displayGenericError = false;
  isUserNotFound = false;
  isUserAlreadyEnabled = false;
  isTokenExpired = false;
  isTokenInvalid = false;
  loading = true;

  private destroyed$ = new Subject<void>();

  constructor (private route: ActivatedRoute,
              public auth: AuthService) {}

  ngOnInit (): void {
    this.route.queryParams
      .subscribe(params => {
        this.auth.confirmUser(params.user, params.token)
          .pipe(
            takeUntil(this.destroyed$),
            finalize(() => this.loading = false)
          )
          .subscribe(
            () => this.displaySuccessMessage = true,
            (error) => {
              this.displayErrorMessage = true;
              switch (error.status) {
                case 400:
                  this.isTokenInvalid = true;
                  break;
                case 401:
                  this.isTokenExpired = true;
                  break;
                case 404:
                  this.isUserNotFound = true;
                  break;
                case 408:
                  this.isTokenExpired = true;
                  break;
                case 409:
                  this.isUserAlreadyEnabled = true;
                  break;
                default:
                  this.displayGenericError = true;
                  break;
              }
            }
          );
      });
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  closeErrorMessage () {
    this.displayErrorMessage = false;
  }
}
