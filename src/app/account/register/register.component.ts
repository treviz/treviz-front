import { finalize, take, takeUntil } from 'rxjs/operators';
import { Component, OnDestroy } from '@angular/core';
import { AuthService } from '../../core/services/auth/auth.service';
import { PlatformService } from '../../core/services/platform.service';
import { AbstractControl, FormBuilder, ValidatorFn, Validators } from '@angular/forms';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-register',
  templateUrl: 'register.component.html',
  styleUrls: ['../shared/not-logged-style.scss']
})
export class RegisterComponent implements OnDestroy {
  displayErrorMessage = false;
  isUsernameAlreadyTaken = false;
  isSubmitted = false;
  registrationSuccessFull = false;

  registrationForm = this.formBuilder.group({
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    username: ['', [Validators.required, this.usernameValidator()]],
    email: ['', [Validators.email, Validators.email]],
    password: ['', Validators.required],
    agreedWithTerms: [false, Validators.requiredTrue]
  });

  get username () { return this.registrationForm.get('username'); }
  get email () { return this.registrationForm.get('email'); }

  private destroyed$ = new Subject<void>();

  constructor (public auth: AuthService,
              public platformService: PlatformService,
              private formBuilder: FormBuilder) {}

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  closeErrorMessage () {
    this.displayErrorMessage = false;
  }

  onRegister () {
    this.displayErrorMessage = false;
    this.isSubmitted = true;

    this.auth
      .register(this.registrationForm.value)
      .pipe(
        take(1),
        finalize(() => this.isSubmitted = false),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        () => {
          this.registrationSuccessFull = true;
        },
        (error) => {
          this.displayErrorMessage = true;
          this.isUsernameAlreadyTaken = (error.status === 409);
        }
      );
  }

  usernameValidator (): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
      const authorizedUsername = /^[a-zA-Z0-9_-]{3,20}$/.test(control.value);
      return authorizedUsername ? null : { forbiddenUsername: { value: control.value } };
    };
  }
}
