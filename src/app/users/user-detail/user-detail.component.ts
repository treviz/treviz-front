import { Component, OnDestroy, OnInit } from '@angular/core';
import { UserService } from '../../core/services/user.service';
import { User, UserRoles } from '../../shared/models/users/user.model';
import { ProjectService } from '../../core/services/project/project.service';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material/dialog';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Project } from '../../shared/models/project/project.model';
import { Community } from '../../shared/models/community/community.model';
import { CommunityService } from '../../core/services/community/community.service';
import { ProjectInvitation } from '../../shared/models/project/project-invitation.model';
import { ProjectCandidacy } from '../../shared/models/project/project-candidacy.model';
import { ProjectCandidacyService } from '../../core/services/project/project-candidacy.service';
import { ProjectInvitationService } from '../../core/services/project/project-invitation.service';
import { CommunityCandidacyService } from '../../core/services/community/community-candidacy.service';
import { CommunityInvitationService } from '../../core/services/community/community-invitation.service';
import { UserEditDialogComponent } from '../../shared/users/user-edit-dialog/user-edit-dialog.component';
import { CommunityInvitation } from '../../shared/models/community/community-invitation.model';
import { CommunityCandidacy } from '../../shared/models/community/community-candidacy.model';
import { ChatRoomCreateDialogComponent } from 'app/shared/dialogs/chat-room-create-dialog/chat-room-create-dialog.component';
import { ProjectJob } from '../../shared/models/project/project-job.model';
import { ProjectJobService } from '../../core/services/project/project-job.service';
import { UserStateService } from '../../core/services/user-state.service';
import { MessageDialogComponent } from 'app/shared/dialogs/message-dialog/message-dialog.component';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

/**
 * Created by Bastien on 26/02/2017.
 */
@Component({
  selector: 'app-user-detail',
  templateUrl: 'user-detail.component.html',
  styleUrls: ['user-detail.component.scss']
})
export class UserDetailComponent implements OnInit, OnDestroy {
  user: User;

  projects: Project[] = [];
  projectInvitations: ProjectInvitation[] = [];
  projectCandidacies: ProjectCandidacy[] = [];

  communities: Community[] = [];
  communityInvitations: CommunityInvitation[] = [];
  communityCandidacies: CommunityCandidacy[] = [];

  jobs: ProjectJob[] = [];

  newRoomRef: MatDialogRef<ChatRoomCreateDialogComponent>;

  editPossible = false;
  avatarBeingUploaded = false;
  backgroundBeingUploaded = false;
  projectsFetched = false;
  communitiesFetched = false;
  userFetched = false;

  private destroyed$ = new Subject<void>();

  constructor (private userService: UserService,
              private projectService: ProjectService,
              private projectCandidacyService: ProjectCandidacyService,
              private projectInvitationService: ProjectInvitationService,
              private projectJobService: ProjectJobService,
              private communityService: CommunityService,
              private communityCandidacyService: CommunityCandidacyService,
              private communityInvitationService: CommunityInvitationService,
              private currentUserService: UserStateService,
              private route: ActivatedRoute,
              public dialog: MatDialog,
              public router: Router) { }

  ngOnInit () {
    this.fetchData();
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  private fetchData () {
    this.route.params
      .pipe(takeUntil(this.destroyed$))
      .subscribe((params: Params) => {
        this.clear();

        const username = params.username;

        this.editPossible = (username === localStorage.getItem('username'));

        this.userService.getUser(username)
          .pipe(takeUntil(this.destroyed$))
          .subscribe(
            data => this.user = data,
            () => {
              this.displayErrorDialog('An error occurred while fetching the user');
              this.router.navigate(['/users']);
            },
            () => {
              this.userFetched = true;
            }
          );

        this.projectService.getProjects({ user: username })
          .pipe(takeUntil(this.destroyed$))
          .subscribe(
            data => this.projects = data,
            err => console.log(err),
            () => {
              this.projectsFetched = true;
            }
          );

        this.projectInvitationService.getProjectInvitations({ user: username })
          .pipe(takeUntil(this.destroyed$))
          .subscribe(
            data => this.projectInvitations = data,
            err => console.log(err)
          );

        this.projectCandidacyService.getProjectCandidacies({ user: username })
          .pipe(takeUntil(this.destroyed$))
          .subscribe(
            data => this.projectCandidacies = data,
            err => console.log(err)
          );

        this.projectJobService.getJobs({ holder: username })
          .pipe(takeUntil(this.destroyed$))
          .subscribe(
            data => this.jobs = data,
            err => console.log(err)
          );

        this.communityService.getCommunities({ user: username })
          .pipe(takeUntil(this.destroyed$))
          .subscribe(
            data => this.communities = data,
            err => console.log(err),
            () => {
              this.communitiesFetched = true;
            }
          );

        this.communityInvitationService.getCommunityInvitations({ user: username })
          .pipe(takeUntil(this.destroyed$))
          .subscribe(
            data => this.communityInvitations = data,
            err => console.log(err)
          );

        this.communityCandidacyService.getCommunityCandidacies({ user: username })
          .pipe(takeUntil(this.destroyed$))
          .subscribe(
            data => this.communityCandidacies = data,
            err => console.log(err)
          );
      });
  }

  goToCommunity (community: Community): void {
    this.router.navigate(['/communities', community.hash]);
  }

  openEditDialog () {
    const config: MatDialogConfig = {
      maxWidth: '90vw',
      maxHeight: '80vh',
      data: {
        user: this.user,
        admin: this.user.roles.some((role) => role === UserRoles.ROLE_ADMIN)
      }
    };

    const dialogRef = this.dialog.open(UserEditDialogComponent, config);

    dialogRef.afterClosed()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(result => {
        if (result != null) {
          if (result) {
            this.user = result;
          } else {
            this.displayErrorDialog('An error occurred while updating your profile.');
          }
        }
      });
  }

  changeAvatar (event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];

      this.avatarBeingUploaded = true;

      this.userService.postAvatar(file, this.user)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(
          data => {
            this.user = data;
            this.currentUserService.getCurrentUser().avatarUrl = data.avatarUrl;
            this.avatarBeingUploaded = false;
          },
          () => {
            this.displayErrorDialog('The image could not be uploaded, it must not be over 1MB.');
            this.avatarBeingUploaded = false;
          }
        );
    }
  }

  changeBackgroundPicture (event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];

      this.backgroundBeingUploaded = true;

      this.userService.postBackgroundImage(file, this.user)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(
          data => {
            this.user = data;
            this.backgroundBeingUploaded = false;
          },
          () => {
            this.displayErrorDialog('The image could not be uploaded, it must not be over 1MB.');
            this.backgroundBeingUploaded = false;
          }
        );
    }
  }

  openChatCreation () {
    const config = new MatDialogConfig();
    config.data = {
      project: '',
      community: ''
    };

    this.newRoomRef = this.dialog.open(ChatRoomCreateDialogComponent, config);

    this.newRoomRef.componentInstance.addUser(this.user);

    this.newRoomRef.afterClosed().pipe(takeUntil(this.destroyed$))
      .subscribe(
        data => {
          if (data != null) {
            if (data) {
              this.router.navigate(['/chat']);
            } else {
              this.displayErrorDialog('An error occurred while creating the chatroom.');
            }
          }
        }
      );
  }

  /**
   * This function removes all the data that was stored in this component.
   * When the user is redirected from /users/x to /users/y, it therefore refreshes all the data.
   */
  clear () {
    this.user = null;
    this.projects = [];
    this.projectInvitations = [];
    this.projectCandidacies = [];
    this.communities = [];
    this.communityInvitations = [];
    this.communityCandidacies = [];
    this.editPossible = false;
    this.userFetched = false;
    this.projectsFetched = false;
    this.communitiesFetched = false;
    this.avatarBeingUploaded = false;
  }

  displayErrorDialog (message: string) {
    const config = new MatDialogConfig();
    config.data = {
      message,
      failure: true,
      actionRequired: true
    };

    this.dialog.open(MessageDialogComponent, config);
  }
}
