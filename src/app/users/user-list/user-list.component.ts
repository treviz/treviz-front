/**
 * Created by Bastien on 15/02/2017.
 */

import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../../core/services/user.service';
import { User } from '../../shared/models/users/user.model';
import { Subject } from 'rxjs';
import { finalize, take, takeUntil } from 'rxjs/operators';
import { OrganizationService } from '../../core/services/organization.service';
import { ActivatedRoute } from '@angular/router';
import { SearchFilters } from '../../shared/filters/filter.component';

@Component({
  selector: 'app-user-list',
  templateUrl: 'user-list.component.html',
  styleUrls: ['user-list.component.scss']
})
export class UserListComponent implements OnInit, OnDestroy {
  // Search by name
  users: User[] = [];
  organizations: string[] = [];
  params = {
    name: '',
    skills: [],
    tags: [],
    organization: '',
    offset: 0,
    nb: 20
  };

  displayNotFoundMessage = false;
  displayProgressBar = true;
  moreToLoad = true;

  private destroyed$ = new Subject<void>();

  constructor (private userService: UserService,
              private organizationService: OrganizationService,
              private route: ActivatedRoute) {}

  ngOnInit (): void {
    this.route.queryParams
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        params => {
          this.params.organization = params.organization ? params.organization : '';
          this.getUsers(false);
        }
      );

    this.organizationService
      .getOrganizations()
      .pipe(take(1), takeUntil(this.destroyed$))
      .subscribe((organizations) => this.organizations = organizations.map(el => el.name));
  }

  ngOnDestroy (): void {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  search (params: SearchFilters) {
    this.params.skills = params.skills;
    this.params.tags = params.tags;
    this.params.name = params.name;
    this.getUsers(false);
  }

  selectOrganization (val: string[]) {
    const nbValues = val.length;
    this.params.organization = nbValues > 0 ? val[nbValues - 1] : '';
    this.getUsers(false);
  }

  getUsers (append = true) {
    this.displayProgressBar = true;
    if (append) {
      this.params.offset = this.users.length;
    } else {
      this.params.offset = 0;
    }
    this.userService
      .getUsers(this.params)
      .pipe(
        take(1),
        finalize(() => this.displayProgressBar = false),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        data => {
          if (append) {
            for (const user of data) {
              this.users.push(user);
            }
          } else {
            this.users = data;
          }
          this.moreToLoad = data.length === 20;
          this.displayNotFoundMessage = (data.length === 0);
        },
        err => console.log(err)
      );
  }
}
