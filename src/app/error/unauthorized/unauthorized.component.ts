/**
 * Created by Bastien on 21/02/2017.
 */

import { Component } from '@angular/core';

@Component({
  selector: 'unauthorized',
  templateUrl: 'unauthorized.component.html',
  styleUrls: ['unauthorized.component.css']
})

export class UnauthorizedComponent {}
