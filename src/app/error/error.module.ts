import { NgModule } from '@angular/core';
import { NotFoundComponent } from './not-found/not-found.component';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';
import { SharedModule } from '../shared/shared.module';
import { ErrorRoutingModule } from './error-routing.module';

@NgModule({
  imports: [
    SharedModule,
    ErrorRoutingModule
  ],
  exports: [],
  declarations: [
    NotFoundComponent,
    UnauthorizedComponent
  ],
  providers: []
})
export class ErrorModule {
}
