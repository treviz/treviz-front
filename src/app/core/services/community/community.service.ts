
import { catchError } from 'rxjs/operators';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Community } from '../../../shared/models/community/community.model';
import { CommunityDto } from '../../../communities/shared/community.model.dto';

/**
 * Created by Bastien on 12/03/2017.
 */

@Injectable()
export class CommunityService {
  private communitiesUrl = environment.apiUrl + '/communities';

  constructor (public http: HttpClient) {}

  /**
   * Fetches the communities matching the parameters
   *
   * @param {string} name Name of the community
   * @param {number} offset Offset of the search
   * @param {number} nb Number of results
   * @param {string} user Username of a user who must take part in the community
   * @param {string} role Name of a role the specified user must have in the community
   * @param {string} community Hash of a project that must be linked to this community
   * @returns {Observable<Project[]>}
   */
  getCommunities ({
    name = '',
    offset = 0,
    nb = 20,
    user = '',
    exclude = '',
    role = '',
    project = ''
  }:
                 { name?: string;
                   offset?: number;
                   nb?: number;
                   user?: string;
                   exclude?: string;
                   role?: string;
                   project?: string;}): Observable<Community[]> {
    let params: HttpParams = new HttpParams()
      .set('offset', offset.toString())
      .set('nb', nb.toString());

    if (name !== '') {
      params = params.set('name', name);
    }

    if (user !== '') {
      params = params.set('user', user);
    }

    if (exclude !== '') {
      params = params.set('exclude', exclude);
    }

    if (project !== '') {
      params = params.set('project', project);
    }

    if (role !== '') {
      params = params.set('role', role);
    }

    return this.http.get<Community[]>(this.communitiesUrl, { params: params }).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Fetches a community.
   *
   * @param {string} community Hash of the community to fetch
   * @returns {Observable<Community>}
   */
  getCommunity (community: string): Observable<Community> {
    const url = this.communitiesUrl + '/' + community;

    return this.http.get<Community>(url).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Creates a new community
   *
   * @param {CommunityDto} community Community to create
   * @returns {Observable<Community>}
   */
  postCommunity (community: CommunityDto): Observable<Community> {
    return this.http.post<Community>(this.communitiesUrl, community).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Updates an existing community
   *
   * @param {string} community hash of the community to update
   * @param {CommunityDto} updatedCommunity Updated Community
   * @returns {Observable<Community>}
   */
  putCommunity (community: string, updatedCommunity: CommunityDto): Observable<Community> {
    const url = this.communitiesUrl + '/' + community;

    let body;

    if (updatedCommunity.logo != null) {
      body = new FormData();
      body.append('name', updatedCommunity.name);
      body.append('description', updatedCommunity.description);
      body.append('isVisible', updatedCommunity.isVisible);
      body.append('open', updatedCommunity.isOpen);
      body.append('website', updatedCommunity.website);
      body.append('logo', updatedCommunity.logo, updatedCommunity.logo.name);
    } else {
      body = updatedCommunity;
    }

    return this.http.put<Community>(url, body).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Deletes a community
   *
   * @param {string} community hash of the community to delete
   * @returns {Observable<any | any>}
   */
  deleteCommunity (community: string): Observable<any> {
    const url = this.communitiesUrl + '/' + community;

    return this.http.delete(url).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  postLogo (community: string, file: File): Observable<Community> {
    const formData: FormData = new FormData();
    formData.append('image', file, file.name);

    const url = this.communitiesUrl + '/' + community + '/logo';

    return this.http.post<Community>(url, formData).pipe(
      catchError(error => observableThrowError(error)));
  }

  postBackgroundImage (community: string, file: File): Observable<Community> {
    const formData: FormData = new FormData();
    formData.append('image', file, file.name);

    const url = this.communitiesUrl + '/' + community + '/background';

    return this.http.post<Community>(url, formData).pipe(
      catchError(error => observableThrowError(error)));
  }
}
