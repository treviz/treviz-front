import { Injectable } from '@angular/core';
import { NotificationType } from '../../shared/models/notifications/notifications-type.enum';
import { UserStateService } from './user-state.service';
import Subscription, { SubscriptionPriority } from 'app/shared/models/notifications/subscription.model';
import { NotificationService } from './notification.service';
import { Notification } from '../../shared/models/notifications/notification.model';
import { ChatStateService } from './chat-state.service';
import { Room } from '../../shared/models/chat/room.model';
import { Message } from '../../shared/models/chat/message.model';

@Injectable()
export class DispatcherService {
  private subscription: Subscription = {
    componentName: DispatcherService.name,
    notificationTypes: [
      NotificationType.CHAT_POST_MESSAGE,
      NotificationType.CHAT_UPDATE_MESSAGE,
      NotificationType.CHAT_DELETE_MESSAGE,
      NotificationType.CHAT_POST_ROOM,
      NotificationType.CHAT_UPDATE_ROOM,
      NotificationType.CHAT_DELETE_ROOM
    ],
    priority: SubscriptionPriority.NOTIFICATION_DISPATCHER_PRIORITY,
    callback: this.dispatchNotifications.bind(this)
  };

  constructor (private currentUserService: UserStateService,
              private notificationService: NotificationService,
              private chatStateService: ChatStateService) {
  }

  init () {
    this.notificationService.subscribe(this.subscription);
  }

  public dispatchNotifications (notification: Notification, handled: string[]) {
    switch (notification.type) {
      case NotificationType.CHAT_POST_ROOM:
        this.chatStateService.addRooms([notification.content as Room], true);
        break;
      case NotificationType.CHAT_UPDATE_ROOM:
        this.chatStateService.updateRoom(notification.content as Room);
        break;
      case NotificationType.CHAT_DELETE_ROOM:
        this.chatStateService.deleteRoom(notification.content as Room);
        break;
      case NotificationType.CHAT_POST_MESSAGE:
        // If the room is active, i.e. the user is currently viewing it, do not display any notification.
        const room = this
          .chatStateService
          .rooms
          .find((el) => el.hash === notification.content.room_hash);
        this.chatStateService.addMessage(notification.content as Message, !room.isActive);
        break;
      case NotificationType.CHAT_UPDATE_MESSAGE:
        this.chatStateService.updateMessage(notification.content as Message);
        break;
      case NotificationType.CHAT_DELETE_MESSAGE:
        this.chatStateService.deleteMessage(notification.content as Message);
        break;
      default:
        break;
    }
  }
}
