
import { catchError } from 'rxjs/operators';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Label } from '../../../shared/models/kanban/label.model';
import { LabelDto } from '../../../shared/models/kanban/label.model.dto';
import { environment } from 'environments/environment';

@Injectable()
export class LabelService {
  private labelsUrl = environment.apiUrl + '/boards/';

  constructor (private http: HttpClient) {}

  /**
   * Creates a new label
   * @param board Hash of the board to which create the label
   * @param {LabelDto} label Label to create
   * @returns {Observable<Label>}
   */
  postLabel (board: string, label: LabelDto): Observable<Label> {
    const url = this.labelsUrl + board + '/labels';
    return this.http.post<Label>(url, label).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Updates an existing label.
   * @param {string} label hash of the label to update
   * @param {LabelDto} updatedLabel Updated label
   * @returns {Observable<Label>}
   */
  putLabel (label: string, updatedLabel: LabelDto): Observable<Label> {
    const url = this.labelsUrl + 'labels/' + label;
    return this.http.put<Label>(url, updatedLabel).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Deletes an existing label.
   * @param {string} label Hash of the label to delete
   * @returns {Observable<any>}
   */
  deleteLabel (label: string): Observable<any> {
    const url = this.labelsUrl + 'labels/' + label;
    return this.http.delete(url).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }
}
