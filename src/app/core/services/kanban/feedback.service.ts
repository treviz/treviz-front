
import { catchError } from 'rxjs/operators';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Feedback } from '../../../shared/models/kanban/feedback.model';
import { FeedbackDto } from '../../../shared/models/kanban/feedback.model.dto';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class FeedbackService {
  private tasksUrl = environment.apiUrl + '/boards/columns/tasks';

  constructor (private http: HttpClient) {}

  getFeedback ({ given = false, task = '' }: {given?: boolean; task?: string}): Observable<Feedback[]> {
    const url = this.tasksUrl + '/feedback';
    let params = new HttpParams();

    if (given) {
      params = params.set('given', given.toString());
    }
    if (given) {
      params = params.set('task', task);
    }

    return this.http.get<Feedback[]>(url, { params: params }).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  postFeedback (task: string, feedback: FeedbackDto): Observable<Feedback> {
    const url = this.tasksUrl + '/' + task + '/feedback';

    return this.http.post<Feedback>(url, feedback).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }
}
