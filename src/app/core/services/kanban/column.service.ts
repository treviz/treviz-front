
import { catchError } from 'rxjs/operators';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Column } from '../../../shared/models/kanban/column.model';
import { ColumnDto } from '../../../shared/models/kanban/column.model.dto';
import { environment } from 'environments/environment';

@Injectable()
export class ColumnService {
  private columnsUrl = environment.apiUrl + '/boards/';

  constructor (private http: HttpClient) {}

  /**
   * Fetches the columns of a board.
   * @param {string} board Hash of the board from which fetch columns
   * @returns {Observable<Column[]>}
   */
  getColumns (board: string): Observable<Column[]> {
    const url = this.columnsUrl + board + '/columns';
    return this.http.get<Column[]>(url).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Creates a new column
   * @param board Hash of the board to which create the column
   * @param {ColumnDto} column Column to create
   * @returns {Observable<Column>}
   */
  postColumn (board: string, column: ColumnDto): Observable<Column> {
    const url = this.columnsUrl + board + '/columns';
    return this.http.post<Column>(url, column).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Deletes an existing column.
   * @param {string} column Hash of the column to delete
   * @returns {Observable<any>}
   */
  deleteColumn (column: string): Observable<any> {
    const url = this.columnsUrl + 'columns/' + column;
    return this.http.delete(url).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }
}
