
import { catchError } from 'rxjs/operators';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Task } from '../../../shared/models/kanban/task.model';
import { TaskDto } from '../../../shared/models/kanban/task.model.dto';

import { environment } from 'environments/environment';

@Injectable()
export class TaskService {
  private tasksUrl = environment.apiUrl + '/boards/columns/';

  constructor (private http: HttpClient) {}

  /**
   * Fetches the tasks of a column.
   * @param {string} column Hash of the column from which fetch tasks
   * @param assignee username of the user to whom the task is assigned
   * @param supervisor username of the user who supervises the task
   * @param to_approve Should the task be submitted to approval ?
   * @returns {Observable<Task[]>}
   */
  getTasks ({ column = '', assignee = '', supervisor = '', to_approve = false }:
             {column?: string; assignee?: string; supervisor?: string; to_approve?: boolean}): Observable<Task[]> {
    let url;
    column !== '' ? url = this.tasksUrl + column + '/tasks' : url = this.tasksUrl + 'tasks';

    let params = new HttpParams();
    if (assignee !== '') {
      params = params.set('assignee', assignee);
    }

    if (supervisor !== '') {
      params = params.set('supervisor', supervisor);
    }

    if (to_approve) {
      params = params.set('to_approve', 'true');
    }

    return this.http.get<Task[]>(url, { params: params }).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Fetches a specific task according to its hash.
   * @param {string} task Hash of the column to fetch
   * @returns {Observable<Task>}
   */
  getTask (task: string): Observable<Task> {
    const url = this.tasksUrl + 'tasks/' + task;
    return this.http.get<Task>(url).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Creates a new task
   * @param column Hash of the column to which create the task
   * @param {TaskDto} task Task to create
   * @returns {Observable<Task>}
   */
  postTask (column: string, task: TaskDto): Observable<Task> {
    const url = this.tasksUrl + column + '/tasks';
    return this.http.post<Task>(url, task).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Updates an existing task.
   * @param {string} task Hash of the task to update
   * @param {TaskDto} updatedTask JSON Containing the updated task
   * @returns {Observable<Task>}
   */
  putTask (task: string, updatedTask: TaskDto): Observable<Task> {
    const url = this.tasksUrl + 'tasks/' + task;
    return this.http.put<Task>(url, updatedTask).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Deletes an existing task.
   * @param {string} task Hash of the task to delete
   * @returns {Observable<any>}
   */
  deleteTask (task: string): Observable<any> {
    const url = this.tasksUrl + 'tasks/' + task;
    return this.http.delete(url).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Submits a task for approval.
   * @param {string} task
   * @returns {Observable<Task>}
   */
  submitTaskForApproval (task: string): Observable<Task> {
    const url = this.tasksUrl + 'tasks/' + task + '/submit';
    return this.http.post<Task>(url, {}).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }
}
