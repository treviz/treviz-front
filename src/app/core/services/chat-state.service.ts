import { Room } from '../../shared/models/chat/room.model';
import { Message } from '../../shared/models/chat/message.model';
import { incrementMapValues } from '../../../helpers/map';
import { AbstractStateService } from './abstract-state.service';

interface ChatState {
  chat?: {
    rooms: Room[];
    notifications: {
      unlinked: number;
      // The maps key is the community or project hash; the value is the number of notifications
      projects: Map<string, number>;
      communities: Map<string, number>;
    };
  };
}

export class ChatStateService extends AbstractStateService<ChatState> {
  private emptyState = {
    chat: {
      rooms: [],
      notifications: {
        unlinked: 0,
        projects: new Map<string, number>(),
        communities: new Map<string, number>()
      }
    }
  };

  get rooms (): Room[] {
    return this._state.value.chat?.rooms ?? [];
  }

  constructor () {
    super();
    this.initialize();
  }

  /* Synchronous mutations */

  addRooms (rooms: Room[], notify = false) {
    const newState = this._state.getValue();
    rooms.forEach((room) => {
      if (!newState.chat.rooms.some((el) => el.hash === room.hash)) {
        newState.chat.rooms.push(room);
        if (notify) {
          room.nbNotifications = 1;
          this.updateNotifications(newState, room, room.nbNotifications);
        }
      }
    });
    this.setState(newState);
  }

  updateRoom (updatedRoom: Room) {
    const state = this._state.getValue();
    const index = state.chat.rooms.findIndex((room) => room.hash === updatedRoom.hash);
    state.chat.rooms[index] = updatedRoom;
    this.setState(state);
  }

  deleteRoom (roomToDelete: Room) {
    const state = this._state.getValue();
    const index = state.chat.rooms.findIndex((room) => room.hash === roomToDelete.hash);
    state.chat.rooms.splice(index, 1);
    this.setState(state);
  }

  addMessage (message: Message, notify: boolean) {
    const state = this._state.getValue();
    const room = this.findRoom(state, message.room_hash);
    if (null == room.messages) {
      room.messages = [];
    }
    room.messages.push(message);
    if (notify) {
      if (null == room.nbNotifications) {
        room.nbNotifications = 0;
      }
      room.nbNotifications++;
      this.updateNotifications(state, room, 1);
    }
    this.setState(state);
  }

  addPreviousMessages (messages: Message[]) {
    const state = this._state.getValue();
    const room = this.findRoom(state, messages[0].room_hash);
    room.messages = messages.reverse().concat(room.messages ?? []);
    this.setState(state);
  }

  updateMessage (message: Message) {
    const state = this._state.getValue();
    const room = this.findRoom(state, message.room_hash);
    const index = room?.messages?.findIndex((el) => el.hash === message.hash);
    if (null != index) {
      room.messages[index] = message;
    }
    this.setState(state);
  }

  deleteMessage (message: Message) {
    const state = this._state.getValue();
    const room = this.findRoom(state, message.room_hash);
    const index = room?.messages?.findIndex((el) => el.hash === message.hash);
    if (null != index) {
      room.messages.splice(index, 1);
    }
    this.setState(state);
  }

  setActive (activeRoom: Room) {
    this.toggleRoomState(activeRoom, true);
    if (activeRoom.nbNotifications > 0) {
      const state = this._state.getValue();
      this.updateNotifications(state, activeRoom, -activeRoom.nbNotifications);
      const room = this.findRoom(state, activeRoom.hash);
      if (null != room) {
        room.nbNotifications = 0;
      }
      this.setState(state);
    }
  }

  setInactive (room: Room) {
    this.toggleRoomState(room, false);
  }

  toggleRoomState (room: Room, isActive: boolean) {
    const state = this._state.value;
    const roomToToggle = this.findRoom(state, room.hash);
    if (null != roomToToggle) {
      roomToToggle.isActive = isActive;
    }
    this.setState(state);
  }

  /* Helper functions */

  private updateNotifications (currentState: ChatState, room: Room, nbToAdd: number) {
    if (null != room.project) {
      incrementMapValues(currentState.chat.notifications.projects, room.project.hash, nbToAdd);
    } else if (null != room.community) {
      incrementMapValues(currentState.chat.notifications.communities, room.community.hash, nbToAdd);
    } else {
      currentState.chat.notifications.unlinked += (nbToAdd ?? 0);
    }
  }

  private findRoom (state: ChatState, roomHash: string): Room | undefined {
    return state.chat.rooms.find((el) => el.hash === roomHash);
  }

  private initialize () {
    this.setState(this.emptyState);
  }
}
