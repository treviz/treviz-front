
import { catchError } from 'rxjs/operators';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Tag } from '../../shared/models/tags/tag.model';
import { environment } from 'environments/environment';

@Injectable()
export class TagService {
  tagsUrl = environment.apiUrl + '/tags';

  constructor (public http: HttpClient) {}

  getTagsHttp (): Observable<Tag[]> {
    return this.http.get<Tag[]>(this.tagsUrl)
      .pipe(
        catchError((error: any) => observableThrowError(error || 'Server Error'))
      );
  }
}
