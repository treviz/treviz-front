
import { catchError } from 'rxjs/operators';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Comment } from '../../shared/models/newsfeed/comment.model';
import { CommentDto } from '../../shared/models/newsfeed/comment.model.dto';

/**
 * Created by Bastien on 18/03/2017.
 */
@Injectable()
export class CommentsService {
  postsUrl = environment.apiUrl + '/posts';

  constructor (private http: HttpClient) {}

  postComment (post: string, comment: CommentDto): Observable<Comment> {
    const url = this.postsUrl + '/' + post + '/comments';
    return this.http.post<Comment>(url, comment).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  putComment (comment: string, updatedComment: CommentDto): Observable<Comment> {
    const url = this.postsUrl + '/comments/' + comment;
    return this.http.put<Comment>(url, updatedComment).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  deleteComment (comment: string): Observable<any> {
    const url = this.postsUrl + '/comments/' + comment;
    return this.http.delete(url).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error'))
    );
  }
}
