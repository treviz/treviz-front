import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserStateService } from '../user-state.service';

@Injectable()
export class AdminGuard implements CanActivate {
  constructor (private currentUserService: UserStateService) {}

  canActivate (next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.currentUserService.isOrganizationAdmin();
  }
}
