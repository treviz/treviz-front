
import { catchError } from 'rxjs/operators';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { User } from '../../shared/models/users/user.model';
import { environment } from 'environments/environment';
import { UserDto } from '../../shared/models/users/user.model.dto';

@Injectable()
export class UserService {
  usersUrl = environment.apiUrl + '/users';

  constructor (public http: HttpClient) {}

  /**
   * Fetches an array of users according to a query.
   * @param {string} name Name of the user to fetch
   * @param {string} organization Name of the organization the users should belong to.
   * @param {string[]} tags Array of tag names the user should have.
   * @param {string[]} skills Array of skill names the user should have.
   * @param {number} offset
   * @param {number} nb
   * @returns {Observable<User[]>}
   */
  getUsers ({
    name = '',
    tags = [],
    skills = [],
    offset = 0,
    organization,
    nb = 10
  }:
             { name?: string;
               tags?: string[];
               skills?: string[];
               offset?: number;
               organization?: string;
               nb?: number;}): Observable<User[]> {
    let params: HttpParams = new HttpParams()
      .set('offset', offset.toString())
      .set('nb', nb.toString());

    if (name !== '' && name != null) {
      params = params.set('name', name);
    }

    params = (organization !== '' && organization != null) ? params.set('organization', organization) : params;

    for (const tag of tags) {
      params = params.append('tags[]', tag);
    }

    for (const skill of skills) {
      params = params.append('skills[]', skill);
    }

    return this.http.get<User[]>(this.usersUrl, { params: params }).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  putUser (username: string, updatedUser: UserDto): Observable<User> {
    const url = this.usersUrl + '/' + username;

    return this.http.put<User>(url, updatedUser).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  getUser (username: string): Observable<User> {
    return this.http.get<User>(this.usersUrl + '/' + username).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  deleteUser (username: string): Observable<any> {
    return this.http.delete<any>(this.usersUrl + '/' + username).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  getCurrentUser (): Observable<User> {
    return this.getUser(localStorage.getItem('username'));
  }

  postAvatar (file: File, user: User): Observable<User> {
    const formData: FormData = new FormData();
    formData.append('avatar', file, file.name);

    const url = this.usersUrl + '/' + user.username + '/avatar';

    return this.http.post<User>(url, formData).pipe(
      catchError(error => observableThrowError(error)));
  }

  postBackgroundImage (file: File, user: User): Observable<User> {
    const formData: FormData = new FormData();
    formData.append('background', file, file.name);

    const url = this.usersUrl + '/' + user.username + '/background';

    return this.http.post<User>(url, formData).pipe(
      catchError(error => observableThrowError(error)));
  }

  getArchive (username: string): Observable<any> {
    return this.http.get(`${this.usersUrl}/${username}/archive`);
  }
}
