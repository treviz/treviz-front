import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import Organization from '../../shared/models/organization.model';

@Injectable()
export class OrganizationService {
  public organizationsUrl = `${environment.apiUrl}/organizations`;

  constructor (private http: HttpClient) { }

  public getOrganizations (): Observable<Organization[]> {
    return this.http.get<Organization[]>(this.organizationsUrl);
  }

  public postOrganization (organization: Organization) {
    return this.http.post<Organization>(this.organizationsUrl, organization);
  }

  public putOrganization (name: string, updatedOrganization: Organization) {
    const url = `${this.organizationsUrl}/${name}`;
    return this.http.put<Organization>(url, updatedOrganization);
  }

  public deleteOrganization (name: string): Observable<any> {
    const url = `${this.organizationsUrl}/${name}`;
    return this.http.delete(url);
  }
}
