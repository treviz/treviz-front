import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import MailDomainModel from '../../shared/mail-domain.model';

@Injectable()
export class MailDomainService {
  public mailDomainUrl = `${environment.apiUrl}/platform/mail-domains`;

  constructor (private http: HttpClient) { }

  public getMailDomains (): Observable<MailDomainModel[]> {
    return this.http.get<MailDomainModel[]>(this.mailDomainUrl);
  }

  public postMailDomains (domain: MailDomainModel) {
    return this.http.post<MailDomainModel>(this.mailDomainUrl, domain);
  }

  public putMailDomain (name: string, updatedDomain: MailDomainModel) {
    const url = `${this.mailDomainUrl}/${name}`;
    return this.http.put<MailDomainModel>(url, updatedDomain);
  }

  public deleteMailDomain (name: string): Observable<any> {
    const url = `${this.mailDomainUrl}/${name}`;
    return this.http.delete(url);
  }
}
