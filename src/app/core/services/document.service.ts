import {catchError} from 'rxjs/operators';
import {Observable, throwError as observableThrowError} from 'rxjs';
import {Injectable} from '@angular/core';
import {environment} from 'environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';

import {DocumentDto} from '../../shared/models/document/document.model.dto';
import {Document} from '../../shared/models/document/document.model';

@Injectable()
export class DocumentService {
  private url = environment.apiUrl + '/documents';

  constructor (private http: HttpClient) {}

  /**
   * Fetch document from a specific project, community, idea or chat room
   *
   * @param {string} project Hash of the project from which fetch the document
   * @param {string} community Hash of the community from which fetch the document
   * @param {string} idea Hash of the idea from which fetch the document
   * @param {string} roomHash of the roomHash from which fetch the document
   * @returns {Observable<Document[]>}
   */
  getDocuments ({ project = '', community = '', idea = '', room = '' }:
                 {project: string; community: string; idea: string; room: string}): Observable<Document[]> {
    let params = new HttpParams();

    if (project !== '') { params = params.set('project', project); }
    if (community !== '') { params = params.set('community', community); }
    if (idea !== '') { params = params.set('idea', idea); }
    if (room !== '') { params = params.set('room', room); }

    return this.http.get<Document[]>(this.url, { params: params }).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Creates a new document.
   *
   * @param {DocumentDto} document Document to create
   * @returns {Observable<Document>}
   */
  postDocument (document: DocumentDto): Observable<Document> {
    const formData: FormData = new FormData();
    formData.append('name', document.name);
    formData.append('description', document.description ?? '');
    formData.append('file', document.file, document.file.name);

    if (document.community) {
      formData.append('community', document.community.toString());
    } else if (document.project) {
      formData.append('project', document.project.toString());
    } else if (document.idea) {
      formData.append('idea', document.idea.toString());
    } else if (document.room) {
      formData.append('room', document.room.toString());
    }

    return this.http.post<Document>(this.url, formData).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Deletes an existing document
   *
   * @param {string} hash
   * @returns {Observable<any>}
   */
  deleteDocument (hash: string): Observable<any> {
    const url = this.url + '/' + hash;

    return this.http.delete(url).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }
}
