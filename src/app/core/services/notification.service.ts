
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { EventSourceService } from './event-source.service';
import { UserStateService } from './user-state.service';
import { Notification } from '../../shared/models/notifications/notification.model';
import Subscription from 'app/shared/models/notifications/subscription.model';
import { Subscription as RxjsSubscription } from 'rxjs';

/**
 * Created by huber on 05/08/2017.
 */
@Injectable()
export class NotificationService {
  private NOTIFICATIONS_API_URL = environment.notificationApiUrl;
  public subscriptions: Subscription[] = [];
  private readonly loadingSubscription: RxjsSubscription;

  constructor (private eventSourceService: EventSourceService,
              private currentUserService: UserStateService) {
    this.loadingSubscription = this.currentUserService
      .loading
      .subscribe(
        state => {
          if (!(state.loadingRooms || state.loadingCommunities || state.loadingProjects)) {
            this.connectToNotificationEs();
          }
        }
      );
  }

  /**
   * Establishes the event-stream connection with the Events API.
   * Maps the received events and populate the notification variable.
   *
   * When an event is received, iterates over the subscription manifest to
   * trigger the appropriate callbacks.
   */
  public connectToNotificationEs (): void {
    this.loadingSubscription.unsubscribe();
    const jwt = localStorage.getItem('access_token');
    const username = this.currentUserService.getCurrentUser().username;
    const notificationUrl = `${this.NOTIFICATIONS_API_URL}/${username}?jwt=${jwt}&ngsw-bypass=true`;

    // The JWT for authentication will be automatically added to the request thanks to the Authentication module
    this.eventSourceService
      .connect(notificationUrl).pipe(
        map((response: MessageEvent): Notification => {
          try {
            const data = JSON.parse(response.data);
            return {
              type: data.type,
              content: data.content
            };
          } catch (e) {
            console.log('Message event could not be parsed');
          }
        }))
      .subscribe(
        (notification: Notification) => {
          const handled = [];
          this.subscriptions.forEach((subscription: Subscription) => {
            if (subscription.notificationTypes.includes(notification.type)) {
              subscription.callback(notification, handled);
              handled.push(subscription.componentName);
            }
          });
        }
      );
  }

  public subscribe (subscription: Subscription) {
    this.subscriptions.push(subscription);
    // sorts the subscriptions so that the ones with high priority are at the top of the list
    this.subscriptions.sort((a, b) => b.priority - a.priority);
  }

  public unsubscribe (subscriptionName: string) {
    const index = this.subscriptions.findIndex((sub) => sub.componentName === subscriptionName);
    if (index > -1) {
      this.subscriptions.splice(index, 1);
    }
  }

  disconnectToNotificationEs (): void {
    this.unsubscribe(NotificationService.name);
    this.eventSourceService.close();
  }
}
