import { Injectable } from '@angular/core';
import * as Rx from 'rxjs';

/**
 * Created by huber on 29/07/2017.
 */
@Injectable()
export class EventSourceService {
  private socket: Rx.Observable<MessageEvent>;
  private eventSource: EventSource;

  public connect (url): Rx.Observable<MessageEvent> {
    if (!this.socket) {
      this.socket = this.create(url);
    }
    return this.socket;
  }

  public close () {
    if (this.socket) {
      this.eventSource.close();
      this.socket = null;
    }
  }

  private create (url): Rx.Observable<MessageEvent> {
    this.eventSource = new EventSource(url);

    return Rx.Observable.create(
      (obs: Rx.Observer<MessageEvent>) => {
        this.eventSource.onmessage = obs.next.bind(obs);
        this.eventSource.onerror = obs.error.bind(obs);
        return this.eventSource.close.bind(this.eventSource);
      }
    );
  }
}
