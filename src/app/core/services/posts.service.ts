
import { catchError } from 'rxjs/operators';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Post } from '../../shared/models/newsfeed/post.model';
import { environment } from 'environments/environment';
import { PostDto } from '../../shared/models/newsfeed/post.model.dto';

/**
 * Created by Bastien on 18/03/2017.
 */
@Injectable()
export class PostsService {
  postUrl = environment.apiUrl + '/posts';

  constructor (private http: HttpClient) {}

  /**
   * Fetches the posts matching the query.
   *
   * @param {string} project Hash of the project from which fetch posts
   * @param {string} community Hash of the community from which fetch posts
   * @param {string} task Hash of the task from which fetch posts
   * @param {string} job Hash of the job from which fetch posts
   * @param {string} document Hash of the document from which fetch posts
   * @param {number} offset Offset from which fetch posts
   * @param {number} limit Number of posts to fetch
   * @returns {Observable<Post[]>}
   */
  getPosts ({ project = '', community = '', task = '', job = '', document = '', offset = 0, limit = 10 }:
           {
             project?: string;
             community?: string;
             task?: string;
             job?: string;
             document?: string;
             offset?: number;
             limit?: number;}
  ): Observable<Post[]> {
    let params = new HttpParams()
      .set('offset', offset.toString())
      .set('limit', limit.toString());

    if (project !== '') {
      params = params.set('project', project);
    }

    if (community !== '') {
      params = params.set('community', community);
    }

    if (task !== '') {
      params = params.set('task', task);
    }

    if (job !== '') {
      params = params.set('job', job);
    }

    if (document !== '') {
      params = params.set('document', document);
    }

    return this.http.get<Post[]>(this.postUrl, { params: params }).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Creates a new post
   *
   * @param {PostDto} post to create
   * @returns {Observable<Post>}
   */
  postPost (post: PostDto): Observable<Post> {
    return this.http.post<Post>(this.postUrl, post).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Updates an existing post
   *
   * @param {string} post Hash of the post to update
   * @param {PostDto} updatedPost Updated post
   * @returns {Observable<Post>}
   */
  putPost (post: string, updatedPost: PostDto): Observable<Post> {
    const url = this.postUrl + '/' + post;

    return this.http.put<Post>(url, updatedPost).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Deletes an existing post
   *
   * @param {Post} post Hash of the post to delete
   * @returns {Observable<any>}
   */
  deletePost (post: string): Observable<any> {
    const url = this.postUrl + '/' + post;
    return this.http.delete(url).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }
}
