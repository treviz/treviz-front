import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import PlatformSettings from '../../shared/platform-settings.model';

@Injectable()
export class PlatformService {
  public settings: PlatformSettings;

  constructor (private http: HttpClient) { }

  public loadPlatformSettings () {
    this.http.get<PlatformSettings>(environment.apiUrl.replace('/v1', ''))
      .subscribe(
        data => this.settings = data,
        err => console.log(err)
      );
  }
}
