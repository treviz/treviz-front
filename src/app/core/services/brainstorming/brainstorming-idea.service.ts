
import { catchError } from 'rxjs/operators';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';
import { BrainstormingIdea } from '../../../shared/models/brainstorming/brainstorming-idea.model';
import { BrainstormingIdeaDto } from '../../../shared/models/brainstorming/brainstorming-idea.model.dto';

/**
 * Created by huber on 26/08/2017.
 * Interacts with Brainstorming Ideas API.
 */
@Injectable()
export class BrainstormingIdeaService {
  private brainstormingUrl = environment.apiUrl + '/brainstorming-sessions/';

  constructor (private http: HttpClient) {}

  /**
   * Fetches the ideas of a brainstorming session.
   * @param {string} session Hash of the brainstorming session from which fetch the ideas
   * @returns {Observable<BrainstormingIdea[]>}
   */
  getIdeas (session: string): Observable<BrainstormingIdea[]> {
    const url = this.brainstormingUrl + session + '/ideas';
    return this.http.get<BrainstormingIdea[]>(url).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Fetches a specific idea.
   * @param {string} idea Hash of the idea to fetch
   * @returns {Observable<BrainstormingIdea>}
   */
  getIdea (idea: string): Observable<BrainstormingIdea> {
    const url = this.brainstormingUrl + 'ideas/' + idea;
    return this.http.get<BrainstormingIdea>(url).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Creates a new idea into an existing brainstorming session.
   * @param {string} session Hash of the session in which create the idea
   * @param {BrainstormingIdeaDto} idea Idea to create
   * @returns {Observable<BrainstormingIdea>}
   */
  postIdea (session: string, idea: BrainstormingIdeaDto): Observable<BrainstormingIdea> {
    const url = this.brainstormingUrl + session + '/ideas';
    return this.http.post<BrainstormingIdea>(url, idea).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Adds the current user as one of the members who like this idea.
   * @param {string} idea
   * @returns {Observable<any>}
   */
  likeIdea (idea: string): Observable<any> {
    const url = this.brainstormingUrl + 'ideas/' + idea + '/like';
    return this.http.get(url).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Removes the current user from the list of members who like this idea.
   * @param {string} idea
   * @returns {Observable<any>}
   */
  unlikeIdea (idea: string): Observable<any> {
    const url = this.brainstormingUrl + 'ideas/' + idea + '/unlike';
    return this.http.get(url).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  voteForIdea (idea: string, score: number): Observable<BrainstormingIdea> {
    const url = `${this.brainstormingUrl}ideas/${idea}/votes`;
    const body = {
      score
    };
    return this.http.post<BrainstormingIdea>(url, body).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }
}
