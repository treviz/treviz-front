import {catchError} from 'rxjs/operators';
import {Observable, throwError as observableThrowError} from 'rxjs';
import {Injectable} from '@angular/core';
import {environment} from 'environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';

import {BrainstormingSession} from '../../../shared/models/brainstorming/brainstorming-session.model';
import {BrainstormingSessionDto} from '../../../shared/models/brainstorming/brainstorming-session.model.dto';

/**
 * Created by huber on 26/08/2017.
 * CRUD for Brainstorming sessions.
 */
@Injectable()
export class BrainstormingSessionService {
  private brainstormingSessionUrl = environment.apiUrl + '/brainstorming-sessions';

  constructor (private http: HttpClient) {}

  /**
   * Fetches the sessions of a project or community.
   * @param {string} project Hash of the project from which fetch the sessions.
   * @param {string} community Hash of the community from which fetch the sessions.
   * @returns {Observable<BrainstormingSession[]>}
   */
  getSessions ({ project = '', community = '' }: {project?: string; community?: string}): Observable<BrainstormingSession[]> {
    let params = new HttpParams();
    if (project !== '') { params = params.append('project', project); }
    if (community !== '') { params = params.append('community', community); }

    return this.http.get<BrainstormingSession[]>(this.brainstormingSessionUrl, { params: params }).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Creates a new brainstorming session.
   * @param {BrainstormingSessionDto} sessionDto
   * @returns {Observable<BrainstormingSession>}
   */
  postSession (sessionDto: BrainstormingSessionDto): Observable<BrainstormingSession> {
    return this.http.post<BrainstormingSession>(this.brainstormingSessionUrl, sessionDto).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Updates a brainstorming session
   *
   * @param hash string
   * @param sessionDto Session to update
   */
  putSession (hash: string, sessionDto: BrainstormingSessionDto): Observable<BrainstormingSession> {
    const url = `${this.brainstormingSessionUrl}/${hash}`;
    return this.http.put<BrainstormingSession>(url, sessionDto).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));

  }
}
