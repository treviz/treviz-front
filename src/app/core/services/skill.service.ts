
import { catchError } from 'rxjs/operators';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Skill } from '../../shared/models/tags/skill.model';

@Injectable()
export class SkillService {
  skillUrl = environment.apiUrl + '/skills';

  constructor (public http: HttpClient) {}

  getSkillsHttp (): Observable<Skill[]> {
    return this.http.get<Skill[]>(this.skillUrl)
      .pipe(
        catchError((error: any) => observableThrowError(error || 'Server Error'))
      );
  }
}
