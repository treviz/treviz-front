
import { catchError } from 'rxjs/operators';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { Project } from '../../../shared/models/project/project.model';
import { environment } from 'environments/environment';
import { ProjectDto } from '../../../shared/models/project/project.model.dto';

@Injectable()
export class ProjectService {
  projectUrl = environment.apiUrl + '/projects';

  constructor (private http: HttpClient) {}

  /**
   * Fetches the projects matching the parameters
   *
   * @param {string} name Name of the project
   * @param {string[]} tags Tags of the project
   * @param {string[]} skills Skills of the project
   * @param {number} offset Offset of the search
   * @param {number} nb Number of results
   * @param {string} user Username of a user who must take part in the projects
   * @param {string} role Name of a role the specified user must have in the project
   * @param {string} community Hash of a community the projects must be in
   * @returns {Observable<Project[]>}
   */
  getProjects ({
    name = '',
    tags = [],
    skills = [],
    offset = 0,
    nb = 10,
    user = '',
    role = '',
    community = ''
  }:
               { name?: string;
                 tags?: string[];
                 skills?: string[];
                 offset?: number;
                 nb?: number;
                 user?: string;
                 role?: string;
                 community?: string;}): Observable<Project[]> {
    let params: HttpParams = new HttpParams()
      .set('offset', offset.toString())
      .set('nb', nb.toString());

    if (name !== '') {
      params = params.set('name', name);
    }

    if (user !== '') {
      params = params.set('user', user);
    }

    if (community !== '') {
      params = params.set('community', community);
    }

    if (role !== '') {
      params = params.set('role', role);
    }

    for (const tag of tags) {
      params = params.append('tags[]', tag);
    }

    for (const skill of skills) {
      params = params.append('skills[]', skill);
    }

    return this.http.get<Project[]>(this.projectUrl, { params: params }).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Fetches a project, identified by its hash
   *
   * @param {string} hash Hash of the project to fetch
   * @returns {Observable<Project>}
   */
  getProject (hash: string): Observable<Project> {
    return this.http.get<Project>(this.projectUrl + '/' + hash).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Creates a new project
   *
   * If no file is giv
   *
   * @param {ProjectDto} projectDto Project to create
   * @returns {Observable<Project>}
   */
  postProject (projectDto: ProjectDto): Observable<Project> {
    return this.http.post<Project>(this.projectUrl, projectDto).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Updates an existing project
   *
   * @param {string} project
   * @param {ProjectDto} projectDto
   * @returns {Observable<any>}
   */
  putProject (project: string, projectDto: ProjectDto) {
    const url = this.projectUrl + '/' + project;

    return this.http.put(url, projectDto).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Deletes an existing project
   *
   * @param {string} project Hash of the project to delete
   * @returns {Observable<any | any>}
   */
  deleteProject (project: string) {
    const url = this.projectUrl + '/' + project;

    return this.http.delete(url).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  getProjectBaseIllustration () {
    return this.http.get(this.projectUrl + '/base-illustrations').pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  postLogo (file: File, project: string): Observable<Project> {
    const formData: FormData = new FormData();
    formData.append('image', file, file.name);

    const url = this.projectUrl + '/' + project + '/logo';

    return this.http.post<Project>(url, formData).pipe(
      catchError(error => observableThrowError(error)));
  }
}
