
import { catchError } from 'rxjs/operators';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ProjectRole } from '../../../shared/models/project/project-role.model';

@Injectable()
export class ProjectRoleService {
  constructor (private http: HttpClient) {}

  /**
   * Fetches the roles of a project.
   *
   * @param {string} project Hash of the project from which fetch roles
   * @returns {Observable<ProjectRole[]>}
   */
  public getProjectRoles (project: string): Observable<ProjectRole[]> {
    const url = environment.apiUrl + '/projects/' + project + '/roles';

    return this.http.get<ProjectRole[]>(url).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Invite a user to an existing project.
   *
   * @param {string} project Hash of the project in which the role should be added
   * @param role Role to create for the project
   * @returns {Observable<ProjectRole>}
   */
  public postProjectRole (project: string, role: ProjectRole): Observable<ProjectRole> {
    const url = environment.apiUrl + '/projects/' + project + '/roles';

    return this.http.post<ProjectRole>(url, role).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Invite a user to an existing project.
   *
   * @param {string} role Hash of the role to update
   * @param updatedRole Role to update
   * @returns {Observable<ProjectRole>}
   */
  public putProjectRole (role: string, updatedRole: ProjectRole): Observable<ProjectRole> {
    const url = environment.apiUrl + '/projects/roles/' + role;

    return this.http.put<ProjectRole>(url, updatedRole).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Deletes a role
   *
   * @param {string} role
   * @returns {Observable<any>}
   */
  public deleteProjectRole (role: string): Observable<any> {
    const url = environment.apiUrl + '/projects/roles/' + role;

    return this.http.delete(url).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }
}
