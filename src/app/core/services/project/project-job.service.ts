import {catchError} from 'rxjs/operators';
import {Observable, throwError as observableThrowError} from 'rxjs';
import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';

import {environment} from 'environments/environment';
import {ProjectJob} from '../../../shared/models/project/project-job.model';
import {ProjectJobDto} from '../../../shared/models/project/project-job.model.dto';

@Injectable()
export class ProjectJobService {
  constructor (private http: HttpClient) {}

  /**
   * Fetches the jobs of a project.
   *
   * @param {string} project Hash of the project from which fetch jobs
   * @returns {Observable<ProjectJob[]>}
   */
  public getProjectJobs (project: string): Observable<ProjectJob[]> {
    const url = environment.apiUrl + '/projects/' + project + '/jobs';

    return this.http.get<ProjectJob[]>(url).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error'))
    );
  }

  /**
   * Fetches jobs according to a complex query.
   *
   * @param {string} holder
   * @param {string} candidate
   * @param {string[]} tags
   * @param {string[]} skills
   * @param {string} project
   * @param {string} task
   * @param {boolean} attributed
   * @param {string} name
   * @param {number} nb
   * @param {number} offset
   * @returns {Observable<ProjectJob[]>}
   */
  public getJobs ({
    holder = '',
    candidate = '',
    tags = [],
    skills = [],
    project = '',
    task = '',
    attributed = null,
    name = '',
    nb = 20,
    offset = 0
  }:
                    { holder?: string;
                      candidate?: string;
                      tags?: string[];
                      skills?: string[];
                      project?: string;
                      task?: string;
                      attributed?: boolean;
                      name?: string;
                      nb?: number;
                      offset?: number;}): Observable<ProjectJob[]> {
    const url = environment.apiUrl + '/projects/jobs';

    let params: HttpParams = new HttpParams()
      .set('offset', offset.toString())
      .set('nb', nb.toString());

    if (holder !== '') { params = params.set('holder', holder); }
    if (candidate !== '') { params = params.set('candidate', candidate); }
    if (project !== '') { params = params.set('project', project); }
    if (task !== '') { params = params.set('task', task); }
    if (name !== '') { params = params.set('name', name); }

    for (const tag of tags) {
      params = params.append('tags[]', tag);
    }

    for (const skill of skills) {
      params = params.append('skills[]', skill);
    }

    if (attributed !== null) { params = params.set('attributed', attributed.toString()); }

    return this.http.get<ProjectJob[]>(url, { params: params }).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error'))
    );
  }

  /**
   * Fetches a specific job.
   *
   * @param {string} job Hash of the job to fetch
   * @returns {Observable<ProjectJob[]>}
   */
  public getProjectJob (job: string): Observable<ProjectJob> {
    const url = environment.apiUrl + '/projects/jobs/' + job;

    return this.http.get<ProjectJob>(url).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error'))
    );
  }

  /**
   * Invite a user to an existing project.
   *
   * @param {string} project Hash of the project in which the job should be added
   * @param job Job to create for the project
   * @returns {Observable<ProjectJob>}
   */
  public postProjectJob (project: string, job: ProjectJobDto): Observable<ProjectJob> {
    const url = environment.apiUrl + '/projects/' + project + '/jobs';

    return this.http.post<ProjectJob>(url, job).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error'))
    );
  }

  /**
   * Invite a user to an existing project.
   *
   * @param {string} job Hash of the job to update
   * @param updatedJob Job to update
   * @returns {Observable<ProjectJob>}
   */
  public putProjectJob (job: string, updatedJob: ProjectJobDto): Observable<ProjectJob> {
    const url = environment.apiUrl + '/projects/jobs/' + job;

    return this.http.put<ProjectJob>(url, updatedJob).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error'))
    );
  }

  /**
   * Deletes a job
   *
   * @param {string} job
   * @returns {Observable<any>}
   */
  public deleteProjectJob (job: string): Observable<any> {
    const url = environment.apiUrl + '/projects/jobs/' + job;

    return this.http.delete(url).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error'))
    );
  }

  /**
   * Quits a job
   * @param job Hash of the job to quit
   */
  quitJob(job: string): Observable<any> {
    const url = `${environment.apiUrl}/projects/jobs/${job}/quit`;

    return this.http.post(url, null).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error'))
    );
  }
}
