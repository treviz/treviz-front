
import { catchError } from 'rxjs/operators';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ProjectCandidacy } from '../../../shared/models/project/project-candidacy.model';

import { environment } from 'environments/environment';
import { ProjectCandidacyDto } from '../../../shared/models/project/project-candidacy.model.dto';

@Injectable()
export class ProjectCandidacyService {
  constructor (private http: HttpClient) {}

  /**
   * Fetches the candidacy of a project or user.
   *
   * @param {string} project
   * @param {string} user
   * @param job
   * @returns {Observable<ProjectCandidacy[]>}
   */
  public getProjectCandidacies ({ project = '', user = '', job = '' }:
                                 { project?: string; user?: string; job?: string}): Observable<ProjectCandidacy[]> {
    let url = environment.apiUrl + '/projects/';

    /*
     * If a project hash is specified, url is <api-url>/projects/:hash/memberships
     * Otherwise, it is just <api-url>/projects/memberships
     */
    project !== '' ? (url += project + '/candidacies') : (url += 'candidacies');

    /*
     * If a username is fetch, add query param to fetch only this user's memberships.
     */
    let params = new HttpParams();
    if (user !== '') {
      params = params.set('user', user);
    }

    if (job !== '') {
      params = params.set('job', job);
    }

    return this.http.get<ProjectCandidacy[]>(url, { params: params }).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Candidate to an existing project.
   *
   * @param {string} project Hash of the project to which invite the user
   * @param {ProjectCandidacyDto} candidacy JSON Body containing the username of the user to invite and a message
   * @returns {Observable<ProjectCandidacy>}
   */
  public postProjectCandidacy (project: string, candidacy: ProjectCandidacyDto): Observable<ProjectCandidacy> {
    const url = environment.apiUrl + '/projects/' + project + '/candidacies';

    return this.http.post<ProjectCandidacy>(url, candidacy).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Deletes an existing candidacy.
   *
   * @param {string} candidacy Hash of the candidacy to delete
   * @returns {Observable<any>}
   */
  public deleteProjectCandidacy (candidacy: string): Observable<any> {
    const url = environment.apiUrl + '/projects/candidacies/' + candidacy;

    return this.http.delete(url).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Accepts an candidacy to a project.
   *
   * @param {string} candidacy
   * @returns {Observable<any>}
   */
  public acceptProjectCandidacy (candidacy: string): Observable<any> {
    const url = environment.apiUrl + '/projects/candidacies/' + candidacy + '/accept';

    return this.http.post(url, candidacy).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }
}
