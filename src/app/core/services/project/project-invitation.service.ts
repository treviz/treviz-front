
import { catchError } from 'rxjs/operators';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ProjectInvitation } from '../../../shared/models/project/project-invitation.model';

import { environment } from 'environments/environment';
import { ProjectInvitationDto } from '../../../shared/models/project/project-invitation.model.dto';

@Injectable()
export class ProjectInvitationService {
  constructor (private http: HttpClient) {}

  /**
   * Fetches the invitation of a project or user.
   *
   * @param {string} project
   * @param {string} user
   * @returns {Observable<ProjectInvitation[]>}
   */
  public getProjectInvitations ({ project = '', user = '' }: { project?: string; user?: string}): Observable<ProjectInvitation[]> {
    let url = environment.apiUrl + '/projects/';

    /*
     * If a project hash is specified, url is <api-url>/projects/:hash/memberships
     * Otherwise, it is just <api-url>/projects/memberships
     */
    project !== '' ? (url += project + '/invitations') : (url += 'invitations');

    /*
     * If a username is fetch, add query param to fetch only this user's memberships.
     */
    let params = new HttpParams();
    if (user !== '') {
      params = params.set('user', user);
    }

    return this.http.get<ProjectInvitation[]>(url, { params: params }).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Invite a user to an existing project.
   *
   * @param {string} project Hash of the project to which invite the user
   * @param {ProjectInvitationDto} invitation JSON Body containing the username of the user to invite and a message
   * @returns {Observable<ProjectInvitation>}
   */
  public postProjectInvitation (project: string, invitation: ProjectInvitationDto): Observable<ProjectInvitation> {
    const url = environment.apiUrl + '/projects/' + project + '/invitations';

    return this.http.post<ProjectInvitation>(url, invitation).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Deletes an existing invitation.
   *
   * @param {string} invitation Hash of the invitation to delete
   * @returns {Observable<any>}
   */
  public deleteProjectInvitation (invitation: string): Observable<any> {
    const url = environment.apiUrl + '/projects/invitations/' + invitation;

    return this.http.delete(url).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Accepts an invitation to a project.
   *
   * @param {string} invitation
   * @returns {Observable<any>}
   */
  public acceptProjectInvitation (invitation: string): Observable<any> {
    const url = environment.apiUrl + '/projects/invitations/' + invitation + '/accept';

    return this.http.post(url, invitation).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }
}
