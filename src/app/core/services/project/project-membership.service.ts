
import { catchError } from 'rxjs/operators';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { ProjectMembership } from '../../../shared/models/project/project-membership.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ProjectMembershipDto } from '../../../shared/models/project/project-membership.model.dto';

@Injectable()
export class ProjectMembershipService {
  constructor (private http: HttpClient) {}

  /**
   * Fetches the project memberships of a project or user.
   *
   * @param {string} project Hash of the project from which fetch memberships
   * @param {string} user Username of the user from whom fetch memberships
   * @returns {Observable<ProjectMembership[]>}
   */
  getProjectMemberships ({ project = '', user = '' }: {project?: string; user?: string}): Observable<ProjectMembership[]> {
    let url = environment.apiUrl + '/projects/';

    /*
     * If a project hash is specified, url is <api-url>/projects/:hash/memberships
     * Otherwise, it is just <api-url>/projects/memberships
     */
    project !== '' ? (url += project + '/memberships') : (url += 'memberships');

    /*
     * If a username is fetch, add query param to fetch only this user's memberships.
     */
    let params = new HttpParams();
    if (user !== '') {
      params = params.set('user', user);
    }

    return this.http.get<ProjectMembership[]>(url, { params: params }).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Updates an existing membership.
   *
   * @param membershipHash Hash of the membership to update
   * @param updatedProjectMembership Update membership
   * @returns {Observable<ProjectMembership>}
   */
  putProjectMembership (membershipHash, updatedProjectMembership: ProjectMembershipDto): Observable<ProjectMembership> {
    const url = environment.apiUrl + '/projects/memberships/' + membershipHash;

    return this.http.put<ProjectMembership>(url, updatedProjectMembership).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Deletes an existing membership.
   *
   * @param membershipHash Hash of the membership to delete
   * @returns {Observable<any>}
   */
  deleteProjectMembership (membershipHash): Observable<any> {
    const url = environment.apiUrl + '/projects/memberships/' + membershipHash;

    return this.http.delete(url).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }
}
