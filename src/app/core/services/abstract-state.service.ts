import { BehaviorSubject } from 'rxjs';

export abstract class AbstractStateService<T> {
  protected _state = new BehaviorSubject<T>(null);
  state = this._state.asObservable();

  protected setState (nextState: T) {
    this._state.next(nextState);
  }
}
