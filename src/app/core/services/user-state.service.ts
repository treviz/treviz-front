import { finalize, take } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { UserService } from './user.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User, UserRoles } from '../../shared/models/users/user.model';
import { ProjectMembershipService } from './project/project-membership.service';
import { CommunityMembershipService } from './community/community-membership.service';
import { ChatRoomService } from './chat/chat-room.service';
import { BehaviorSubject } from 'rxjs';
import { ChatStateService } from './chat-state.service';
import { AbstractStateService } from './abstract-state.service';

/**
 * Created by Bastien on 19/03/2017.
 */
@Injectable()
export class UserStateService extends AbstractStateService<User> {
  private loadingState = {
    loadingUser: true,
    loadingProjects: true,
    loadingCommunities: true,
    loadingRooms: true
  };

  /*
   * Subject matching the state of the user loading.
   */
  private _loading = new BehaviorSubject(this.loadingState);
  public loading = this._loading.asObservable();

  constructor (private userService: UserService,
              private projectMembershipService: ProjectMembershipService,
              private communityMembershipService: CommunityMembershipService,
              private chatRoomService: ChatRoomService,
              private jwtHelper: JwtHelperService,
              private stateService: ChatStateService) {
    super();
  }

  getCurrentUser (): User {
    return this._state.getValue();
  }

  loadCurrentUser () {
    const token = localStorage.getItem('access_token');

    if (!this.jwtHelper.isTokenExpired(token)) {
      const username = localStorage.getItem('username');
      const newUser = new User();
      newUser.username = localStorage.getItem('username');
      this.setState(newUser);

      /*
       * Fetch current user.
       */
      this.userService.getUser(username)
        .pipe(
          finalize(() => {
            this.loadingState.loadingUser = false;
            this._loading.next(this.loadingState);
          }),
          take(1)
        )
        .subscribe(
          user => {
            this.setState({
              ...this.getCurrentUser(),
              ...user
            });
          },
          err => console.log(err)
        );

      this.refreshProjects();
      this.refreshCommunities();
      this.refreshChatRooms();
    }
  }

  /*
  * Fetch current user project memberships
  */
  refreshProjects () {
    this.projectMembershipService.getProjectMemberships({ user: localStorage.getItem('username') })
      .pipe(
        finalize(() => {
          this.loadingState.loadingProjects = false;
          this._loading.next(this.loadingState);
        }),
        take(1)
      )
      .subscribe(
        projectMemberships => {
          const updatedUser = this.getCurrentUser();
          updatedUser.projectMemberships = projectMemberships;
          this.setState(updatedUser);
        },
        err => console.log(err)
      );
  }

  /*
   * Fetch current user community memberships.
   */
  refreshCommunities () {
    this.communityMembershipService.getCommunityMemberships({ user: localStorage.getItem('username') })
      .pipe(
        finalize(() => {
          this.loadingState.loadingCommunities = false;
          this._loading.next(this.loadingState);
        }),
        take(1)
      )
      .subscribe(
        communityMemberships => {
          const updatedUser = this.getCurrentUser();
          updatedUser.communityMemberships = communityMemberships;
          this.setState(updatedUser);
        },
        err => console.log(err)
      );
  }

  /*
   * Fetch the chat rooms in which the user takes part
   */
  refreshChatRooms () {
    this.chatRoomService.getRooms({})
      .pipe(
        finalize(() => {
          this.loadingState.loadingRooms = false;
          this._loading.next(this.loadingState);
        }),
        take(1)
      )
      .subscribe(
        rooms => this.stateService.addRooms(rooms),
        err => console.log(err)
      );
  }

  isOrganizationAdmin (): boolean {
    try {
      const token = this.jwtHelper.decodeToken(localStorage.getItem('access_token'));
      return token.roles.some(role => role === UserRoles.ROLE_ADMIN);
    } catch (e) {
      return false;
    }
  }

  clear () {
    this.setState(null);
    this.loadingState = {
      loadingUser: true,
      loadingProjects: true,
      loadingCommunities: true,
      loadingRooms: true
    };
  }
}
