
import { catchError } from 'rxjs/operators';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { User } from '../../../shared/models/users/user.model';
import { environment } from 'environments/environment';

import { Project } from '../../../shared/models/project/project.model';
import { Community } from '../../../shared/models/community/community.model';
import { Room } from '../../../shared/models/chat/room.model';
import { RoomDto } from '../../../shared/models/chat/room.model.dto';

/**
 * Created by huber on 11/07/2017.
 */
@Injectable()
export class ChatRoomService {
  public communities: Community[] = [];
  public projects: Project[] = [];

  private roomUrl = environment.apiUrl + '/rooms';

  constructor (private http: HttpClient) {}

  /**
   * Fetches rooms according to the query.
   * @param {string} project Hash of the project from which fetch the rooms.
   * @param {string} community Hash of the community from which fetch the rooms.
   * @returns {Observable<any | any>}
   */
  public getRooms ({ project = '', community = '' }: {project?: string; community?: string}): Observable<Room[]> {
    let params = new HttpParams();
    if (project !== '') { params = params.set('project', project); }
    if (community !== '') { params = params.set('community', community); }

    return this.http.get<Room[]>(this.roomUrl, { params: params })
      .pipe(
        catchError((error: any) => observableThrowError(error || 'Server Error'))
      );
  }

  /**
   * Creates a chat room
   * @param {RoomDto} room
   * @returns {Observable<Room>}
   */
  public postRoom (room: RoomDto): Observable<Room> {
    return this.http.post<Room>(this.roomUrl, room).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Adds a user to an existing chat room.
   * @param {User[]} users Array of the users to remove.
   * @param {Room} room Hash of the room to which add the users.
   * @returns {Observable<Room>}
   */
  inviteUsersToRoom (users: User[], room: string): Observable<Room> {
    const url = this.roomUrl + '/' + room + '/invite';

    const body = users.map(user => user.username);

    return this.http.post<Room>(url, body).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Removes some users from a chat room.
   * If no more user is in the room, the room will be deleted.
   * @param {User[]} users Array of the users to remove.
   * @param {Room} room Hash of the room from which remove the users.
   * @returns {Observable<Room>}
   */
  removeUserFromRoom (users: User[], room: string): Observable<Room> {
    const url = this.roomUrl + '/' + room + '/remove';

    const body = users.map(user => user.username);

    return this.http.post<Room>(url, body).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }

  /**
   * Deletes
   * @param {string} room Hash of the room to deleted
   * @returns {Observable<any>}
   */
  public deleteRoom (room: string): Observable<any> {
    const url = this.roomUrl + '/' + room;
    return this.http.delete<Room>(url).pipe(
      catchError((error: any) => observableThrowError(error || 'Server Error')));
  }
}
