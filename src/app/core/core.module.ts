import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { AuthGuard } from './services/auth/auth.gard';
import { UserService } from './services/user.service';
import { UserPreferencesService } from './services/user-preferences.service';
import { ProjectMembershipService } from './services/project/project-membership.service';
import { ProjectInvitationService } from './services/project/project-invitation.service';
import { ProjectCandidacyService } from './services/project/project-candidacy.service';
import { ProjectPreferencesService } from './services/project/project-preferences.service';
import { CommunityInvitationService } from './services/community/community-invitation.service';
import { CommunityCandidacyService } from './services/community/community-candidacy.service';
import { CommunityPreferencesService } from './services/community/community-preferences.service';
import { NavComponent } from './nav/nav.component';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { CommonModule } from '@angular/common';
import { ProjectRoleService } from './services/project/project-role.service';
import { CommunityMembershipService } from './services/community/community-membership.service';
import { CommunityRoleService } from './services/community/community-role.service';
import { BrainstormingSessionService } from './services/brainstorming/brainstorming-session.service';
import { BrainstormingIdeaService } from './services/brainstorming/brainstorming-idea.service';
import { BrainstormingEnhancementService } from './services/brainstorming/brainstorming-enhancement.service';
import { AuthService } from './services/auth/auth.service';
import { CommentsService } from './services/comments.service';
import { CommunityService } from './services/community/community.service';
import { NotificationService } from './services/notification.service';
import { PostsService } from './services/posts.service';
import { ProjectService } from './services/project/project.service';
import { SkillService } from './services/skill.service';
import { TagService } from './services/tag.service';
import { EventSourceService } from './services/event-source.service';
import { UserStateService } from './services/user-state.service';
import { ChatRoomService } from './services/chat/chat-room.service';
import { ChatMessageService } from './services/chat/chat-message.service';
import { BoardService } from './services/kanban/board.service';
import { ColumnService } from './services/kanban/column.service';
import { LabelService } from './services/kanban/label.service';
import { TaskService } from './services/kanban/task.service';
import { ProjectJobService } from './services/project/project-job.service';
import { FeedbackService } from './services/kanban/feedback.service';
import { DispatcherService } from './services/dispatcher.service';
import { OrganizationService } from './services/organization.service';
import { MailDomainService } from './services/mail-domain.service';
import { PlatformService } from './services/platform.service';
import { AdminGuard } from './services/auth/admin.guard';
import { DocumentService } from './services/document.service';
import { SharedModule } from '../shared/shared.module';
import { ChatStateService } from './services/chat-state.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MatButtonModule,
    MatChipsModule,
    MatIconModule,
    MatListModule,
    MatSidenavModule,
    MatToolbarModule,
    SharedModule
  ],
  declarations: [NavComponent],
  exports: [NavComponent],
  providers: [
    AuthGuard,
    AdminGuard,
    AuthService,
    BrainstormingSessionService,
    BrainstormingIdeaService,
    BrainstormingEnhancementService,
    ChatMessageService,
    ChatRoomService,
    CommentsService,
    CommunityService,
    CommunityCandidacyService,
    CommunityInvitationService,
    CommunityMembershipService,
    CommunityRoleService,
    CommunityPreferencesService,
    UserStateService,
    DispatcherService,
    DocumentService,
    FeedbackService,
    MailDomainService,
    NotificationService,
    OrganizationService,
    PlatformService,
    PostsService,
    ProjectService,
    ProjectCandidacyService,
    ProjectInvitationService,
    ProjectJobService,
    ProjectMembershipService,
    ProjectRoleService,
    ProjectPreferencesService,
    SkillService,
    ChatStateService,
    TagService,
    UserService,
    UserPreferencesService,
    EventSourceService,
    BoardService,
    ColumnService,
    LabelService,
    TaskService
  ]
})
export class CoreModule {
  static forRoot (): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        AuthGuard,
        AdminGuard,
        AuthService,
        BrainstormingSessionService,
        BrainstormingIdeaService,
        BrainstormingEnhancementService,
        ChatMessageService,
        ChatRoomService,
        CommentsService,
        CommunityService,
        CommunityCandidacyService,
        CommunityInvitationService,
        CommunityMembershipService,
        CommunityPreferencesService,
        CommunityRoleService,
        UserStateService,
        DispatcherService,
        DocumentService,
        FeedbackService,
        MailDomainService,
        NotificationService,
        PostsService,
        ProjectService,
        OrganizationService,
        PlatformService,
        ProjectCandidacyService,
        ProjectInvitationService,
        ProjectJobService,
        ProjectMembershipService,
        ProjectRoleService,
        ProjectPreferencesService,
        SkillService,
        ChatStateService,
        TagService,
        UserService,
        UserPreferencesService,
        EventSourceService,
        BoardService,
        ColumnService,
        LabelService,
        TaskService
      ]
    };
  }

  constructor (@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }
}
