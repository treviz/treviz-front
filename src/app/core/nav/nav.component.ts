import {
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
  ChangeDetectorRef
} from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { MatSidenav } from '@angular/material/sidenav';
import { AuthService } from '../services/auth/auth.service';
import { UserStateService } from '../services/user-state.service';
import { InfoDialogComponent } from '../../info/info.component';
import { NotificationService } from '../services/notification.service';
import Subscription, { SubscriptionPriority } from 'app/shared/models/notifications/subscription.model';
import { NotificationType } from 'app/shared/models/notifications/notifications-type.enum';
import { ChatStateService } from '../services/chat-state.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { reduceToNumber } from '../../../helpers/map';

@Component({
  selector: 'app-nav',
  templateUrl: 'nav.component.html',
  styleUrls: ['nav.component.scss']
})
export class NavComponent implements OnInit, OnDestroy {
  public username: string;
  public sidenavToggled = false;
  public component: string;
  @ViewChild('sidenav', { static: false })
  public sidenav: MatSidenav;

  public notifications = 0;

  public get smallScreen () { return window.innerWidth <= 768; }
  public get mode () { return this.smallScreen ? 'over' : 'side'; }
  public get isOpen () {
    return !this.smallScreen && this.auth.loggedIn() && (null != this.currentUserService.getCurrentUser());
  }

  private destroyed$ = new Subject<void>();

  constructor (public auth: AuthService,
              public dialog: MatDialog,
              public router: Router,
              public notificationService: NotificationService,
              public stateService: ChatStateService,
              private changeDetector: ChangeDetectorRef,
              public currentUserService: UserStateService) {}

  isHomePage (): boolean {
    return this.router.url === '/';
  }

  isCurrentRoute (route: string): boolean {
    return this.router.url.startsWith(route);
  }

  ngOnInit () {
    this
      .stateService
      .state
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        state => {
          const notif = state.chat.notifications;
          this.notifications = notif.unlinked + reduceToNumber(notif.projects) + reduceToNumber(notif.communities);
        }
      );

    const onNotification = () => {
      this.changeDetector.detectChanges();
    };

    const subscription: Subscription = {
      componentName: NavComponent.name,
      notificationTypes: [
        NotificationType.CHAT_POST_MESSAGE,
        NotificationType.CHAT_POST_ROOM
      ],
      priority: SubscriptionPriority.NAVBAR_PRIORITY,
      callback: onNotification
    };

    this.notificationService.subscribe(subscription);
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  /**
   * Logs out from the application, and reset its state:
   *  - remove the current information of the user
   *  - disconnect from the notification websocket and remove any pending notification display
   *  - remove any  other stored information
   */
  logout () {
    this.notificationService.disconnectToNotificationEs();
    this.notificationService.unsubscribe(NavComponent.name);
    this.currentUserService.clear();
    localStorage.clear();
    this.closeSideNavOnSmallScreen();
    this.router.navigate(['/login']);
  }

  openInfo () {
    this.dialog.open(InfoDialogComponent);
  }

  goTo (route: string) {
    this.router.navigate([route]);
    this.closeSideNavOnSmallScreen();
  }

  goToProfile () {
    this.router.navigate([`/users/${localStorage.getItem('username')}`]);
    this.closeSideNavOnSmallScreen();
  }

  private closeSideNavOnSmallScreen () {
    if (this.smallScreen) {
      this.sidenav.close();
    }
  }

  toggleSideNav () {
    this.sidenavToggled = true;
    if (this.auth.loggedIn()) {
      this.sidenav.toggle();
    }
  }
}
