import {Component, OnInit} from '@angular/core';
import {Community} from '../../shared/models/community/community.model';
import {CommunityService} from '../../core/services/community/community.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {CommunityMembership} from '../../shared/models/community/community-membership.model';
import {CommunityMembershipService} from '../../core/services/community/community-membership.service';
import {CommunityCandidacy} from '../../shared/models/community/community-candidacy.model';
import {CommunityInvitation} from '../../shared/models/community/community-invitation.model';
import {CommunityCandidacyService} from '../../core/services/community/community-candidacy.service';
import {CommunityInvitationService} from '../../core/services/community/community-invitation.service';
import {CandidacyDialogComponent} from '../../shared/candidacy/candidacy-dialog/candidacy-dialog.component';
import {ConfirmDialogComponent} from '../../shared/dialogs/confirm-dialog/confirm-dialog.component';
import {CommunityEditDialogComponent} from './community-edit-dialog.component';
import {CommunityManageTeamDialogComponent} from '../community-manage-team-dialog/community-manage-team-dialog.component';
import {CommunityNotificationPreferencesDialogComponent} from '../community-notification-preferences-dialog/community-notification-preferences-dialog.component';
import CommunityPermissions from '../../shared/models/community/community-permissions.model';
import {finalize, takeUntil} from 'rxjs/operators';
import {Location} from '@angular/common';
import {ConfirmMessageModel} from '../../shared/models/confirm-message.model';
import {AbstractProjectConfirmComponent} from '../../shared/abstract/abstract-project-confirm-component';
import {resizeImage} from "../../../helpers/file-helper";

/**
 * Created by huber on 29/04/2017.
 */
@Component({
  selector: 'app-community',
  templateUrl: 'community-detail.component.html',
  styleUrls: ['community-detail.component.scss']
})
export class CommunityDetailComponent extends AbstractProjectConfirmComponent implements OnInit {
    community: Community;

    memberships: CommunityMembership[] = [];
    userMembership: CommunityMembership;
    membershipsFetched = false;

    candidacies: CommunityCandidacy[] = [];
    userCandidacy: CommunityCandidacy;
    candidaciesFetched = false;

    invitations: CommunityInvitation[] = [];
    userInvitation: CommunityInvitation;
    invitationsFetched = false;
    backgroundBeingUploaded = false;

    canEdit = false;
    canUpdateCommunity = false;
    canUpdateMemberships = false;
    canManageCandidacies = false;
    canManageInvitations = false;
    canUpdateRoles = false;

    constructor (private communityService: CommunityService,
                private communityMembershipService: CommunityMembershipService,
                private communityCandidacyService: CommunityCandidacyService,
                private communityInvitationService: CommunityInvitationService,
                private route: ActivatedRoute,
                private location: Location,
                private router: Router,
                public dialog: MatDialog) {
      super(dialog);
    }

    ngOnInit () {
      this.fetchData();
    }

    private fetchData () {
      this.clear();

      this.route.params
        .subscribe(
          params => {
            this.clear();

            const hash = params.hash;
            this.communityService.getCommunity(hash)
              .pipe(takeUntil(this.destroyed$))
              .subscribe(
                community => {
                  this.community = community;
                  this.updateMemberships();
                },
                err => console.log(err)
              );

            this.communityCandidacyService.getCommunityCandidacies({ community: hash })
              .pipe(
                finalize(() => this.candidaciesFetched = true),
                takeUntil(this.destroyed$)
              )
              .subscribe(
                candidacies => {
                  this.candidacies = candidacies;
                  this.userCandidacy = candidacies.find((c: CommunityCandidacy) => c.user.username === localStorage.getItem('username'));
                },
                err => console.log(err)
              );

            this.communityInvitationService.getCommunityInvitations({ community: hash })
              .pipe(
                finalize(() => this.invitationsFetched = true),
                takeUntil(this.destroyed$)
              )
              .subscribe(
                invitations => {
                  this.invitations = invitations;
                  this.userInvitation = invitations.find((i: CommunityInvitation) => i.user.username === localStorage.getItem('username'));
                }
              );
          }
        );
    }

    public updateMemberships () {
      this.communityMembershipService.getCommunityMemberships({ community: this.community.hash })
        .pipe(
          finalize(() => this.membershipsFetched = true),
          takeUntil(this.destroyed$)
        )
        .subscribe(
          memberships => {
            this.memberships = memberships;
            this.userMembership = memberships
              .find((s: CommunityMembership) => s.user.username === localStorage.getItem('username'));
            if (this.userMembership != null) {
                this.canUpdateCommunity = this.hasPermission(CommunityPermissions.UPDATE_COMMUNITY);
                this.canManageCandidacies = this.hasPermission(CommunityPermissions.MANAGE_CANDIDACIES);
                this.canManageInvitations = this.hasPermission(CommunityPermissions.MANAGE_INVITATIONS);
                this.canUpdateMemberships = this.hasPermission(CommunityPermissions.MANAGE_MEMBERSHIP);
                this.canUpdateRoles = this.hasPermission(CommunityPermissions.MANAGE_ROLE);
                this.canEdit = this.canUpdateMemberships || this.canUpdateCommunity || this.canUpdateRoles
            }

            if (window.location.hash === '#preferences' && this.userMembership != null) {
              this.openPreferencesDialog();
            }
          },
          () => this.displayErrorDialog('An error occurred while getting the memberships of this community')
        );
    }

    public handleCandidacyAccepted (candidacy: CommunityCandidacy) {
      this.removeCandidacy(candidacy);
      this.updateMemberships();
    }

    public removeCandidacy (candidacy: CommunityCandidacy) {
      this.candidacies.splice(this.candidacies.findIndex(indexedCandidacy => indexedCandidacy.hash === candidacy.hash), 1);
    }

    leaveCommunity () {
      const dialogRef = this.dialog.open(ConfirmDialogComponent, { data: ConfirmMessageModel.COMMUNITY_LEAVE });

      dialogRef.afterClosed()
        .pipe(takeUntil(this.destroyed$))
        .subscribe(result => {
          if (result) {
            this.communityMembershipService.deleteCommunityMembership(this.userMembership.hash)
              .pipe(takeUntil(this.destroyed$))
              .subscribe(
                () => this.router.navigate(['/communities']),
                () => this.displayErrorDialog('An error occurred while deleting your membership')
              );
          }
        });
    }

    openCandidacyDialog () {
      const config = new MatDialogConfig();
      config.data = {
        community: this.community
      };

      const dialogRef = this.dialog.open(CandidacyDialogComponent, config);

      dialogRef.afterClosed()
        .pipe(takeUntil(this.destroyed$))
        .subscribe(result => {
          if (result != null) {
            if (result) {
              this.userCandidacy = result;
            } else {
              this.displayErrorDialog('An error occurred while sending the candidacy');
            }
          } else if (null === result) {
            this.location.back();
          }
        });
    }

    acceptInvitation () {
      this.communityInvitationService.acceptCommunityInvitation(this.userInvitation.hash)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(
          () => this.fetchData(),
          () => this.displayErrorDialog('An error occurred while accepting the invitation')
        );
    }

    deleteInvitation (invitation: CommunityInvitation) {
      this.communityInvitationService.deleteCommunityInvitation(invitation.hash)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(
          () => {
            this
              .invitations
              .splice(this.invitations.findIndex(el => el.hash === invitation.hash), 1);
            if (this.userInvitation === invitation) {
              this.userInvitation = null;
            }
          },
          () => this.displayErrorDialog('An error occurred while deleting the invitation')
        );
    }

    declineCandidacy (candidacy: CommunityCandidacy) {
      this.communityCandidacyService.deleteCommunityCandidacy(candidacy.hash)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(
          () => this.removeCandidacy(candidacy),
          () => this.displayErrorDialog('An error occurred while deleting the invitation')
        );
    }

    joinCommunity () {
      this.communityMembershipService.postCommunityMembership(this.community.hash)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(
          () => this.fetchData(),
          () => this.displayErrorDialog('An error occurred while joining this community')
        );
    }

    async changeBackgroundPicture (event) {
      const fileList: FileList = event.target.files;
      if (fileList.length > 0) {
        const file: File = fileList[0];
        const resizedFile = await resizeImage(file, 1024);

        this.backgroundBeingUploaded = true;
        this.communityService.postBackgroundImage(this.community.hash, resizedFile)
          .pipe(takeUntil(this.destroyed$))
          .subscribe(
            data => {
              this.community = data;
            },
            () => {
              this.displayErrorDialog('The image could not be uploaded. It must not be over 1MB');
            },
            () => this.backgroundBeingUploaded = false
          );
      }
    }

    openEditMode () {
      this.dialog
        .open(CommunityEditDialogComponent, { data: this.community })
        .afterClosed()
        .pipe(takeUntil(this.destroyed$))
        .subscribe(result => {
          if (result != null) {
            if (result) {
              this.community = result;
            } else {
              this.displayErrorDialog('An error occurred while updating the community');
            }
          }
          if (result === null) {
            this.confirmAction(ConfirmMessageModel.COMMUNITY_DELETE, () => this.deleteCommunity());
          }
        });
    }

    openTeamManagementDialog () {
      const config: MatDialogConfig = {
        maxWidth: '100vw',
        maxHeight: '100vh',
        data: {
          memberships: this.memberships,
          invitations: this.invitations,
          candidacies: this.candidacies,
          community: this.community,
          canUpdateMemberships: this.canUpdateMemberships,
          canManageCandidacies: this.canManageCandidacies,
          canManageInvitations: this.canManageInvitations,
          canUpdateRoles: this.canUpdateRoles
        }
      };

      this.dialog.open(CommunityManageTeamDialogComponent, config);
    }

    openPreferencesDialog () {
      const config = new MatDialogConfig();
      config.data = this.userMembership;

      this.dialog.open(CommunityNotificationPreferencesDialogComponent, config);
    }

    deleteCommunity () {
      this.communityService.deleteCommunity(this.community.hash)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(
          () => {
            this.location.back();
          },
          () => this.displayErrorDialog('An error occurred while deleting this community')
        );
    }

    private hasPermission (permission: CommunityPermissions): boolean {
        return this.userMembership?.role?.permissions?.includes(permission);
    }

    private clear () {
        this.community = null;
        this.memberships = [];
        this.userMembership = null;
        this.membershipsFetched = false;
        this.candidacies = [];
        this.userCandidacy = null;
        this.candidaciesFetched = false;
        this.invitations = [];
        this.userInvitation = null;
        this.invitationsFetched = false;
    }
}
