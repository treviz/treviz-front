
import { Component, OnDestroy, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CommunityService } from '../../core/services/community/community.service';
import { Community } from '../../shared/models/community/community.model';
import { CommunityDto } from '../shared/community.model.dto';
import { finalize, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-community-edit-dialog',
  templateUrl: 'community-edit-dialog.component.html',
  styleUrls: ['community-edit-dialog.component.css']
})
export class CommunityEditDialogComponent implements OnDestroy {
  hash: string;
  pendingChange = false;
  isFormValid = true;

  private destroyed$ = new Subject<void>();

  constructor (public dialogRef: MatDialogRef<CommunityEditDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public community: Community,
              private communityService: CommunityService) {
    this.hash = community.hash;
  }

  ngOnDestroy (): void {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  onStatusChange (status) {
    this.isFormValid = status;
  }

  onSubmit ({ communityDto, logo }: { communityDto: CommunityDto; logo: File|null}) {
    this.pendingChange = true;

    // communities can be open only if they are also public.
    communityDto.isOpen = communityDto.isOpen && communityDto.isVisible;

    /*
     * First, update the community without the logo.
     * if a logo is given, update it after.
     */
    this.communityService.putCommunity(this.hash, communityDto)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        data => {
          if (logo) {
            this.communityService.postLogo(this.hash, logo)
              .pipe(
                finalize(() => this.pendingChange = false),
                takeUntil(this.destroyed$)
              )
              .subscribe(
                community => this.dialogRef.close(community),
                () => {
                  alert('An error occurred while updating the community logo, please check it weights less than 1MB.');
                  this.dialogRef.close(data);
                }
              );
          } else {
            this.dialogRef.close(data);
          }
        },
        () => this.dialogRef.close(false),
        () => {
          this.pendingChange = false;
        }
      );
  }

  deleteCommunityIntent () {
    this.dialogRef.close(null);
  }
}
