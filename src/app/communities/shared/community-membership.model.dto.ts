export class CommunityMembershipDto {
  user: string;
  role: string;
}
