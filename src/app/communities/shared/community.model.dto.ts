import { Community } from '../../shared/models/community/community.model';

export class CommunityDto {
  name: string;
  isVisible: boolean;
  isOpen: boolean;
  description: string;
  website: string;
  logo: File;
  logoUrl: string;
  backgroundImage: File;
  backgroundImageUrl: string;

  static fromCommunity (community: Community): CommunityDto {
    return {
      name: community.name,
      description: community.description,
      isVisible: community.isVisible,
      isOpen: community.open,
      logoUrl: community.logoUrl,
      backgroundImageUrl: community.backgroundImageUrl,
      website: community.website,
      logo: null,
      backgroundImage: null
    };
  }
}
