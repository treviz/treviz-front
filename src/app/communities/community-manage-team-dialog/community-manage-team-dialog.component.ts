import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { User } from '../../shared/models/users/user.model';
import { CommunityInvitationDto } from '../../shared/models/community/community-invitation.model.dto';
import { CommunityInvitation } from '../../shared/models/community/community-invitation.model';
import { CommunityCandidacy } from '../../shared/models/community/community-candidacy.model';
import { CommunityMembership } from '../../shared/models/community/community-membership.model';
import { CommunityRole } from '../../shared/models/community/community-role.model';
import { Community } from '../../shared/models/community/community.model';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UserService } from '../../core/services/user.service';
import { CommunityMembershipService } from '../../core/services/community/community-membership.service';
import { CommunityRoleService } from '../../core/services/community/community-role.service';
import { CommunityInvitationService } from '../../core/services/community/community-invitation.service';
import { CommunityMembershipDto } from '../shared/community-membership.model.dto';
import { debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  moduleId: module.id,
  selector: 'app-community-manage-team-dialog',
  templateUrl: 'community-manage-team-dialog.component.html',
  styleUrls: ['community-manage-team-dialog.component.scss']
})
export class CommunityManageTeamDialogComponent implements OnInit, OnDestroy {
  /*
   * Invite users
   */
  userCtrl: FormControl;
  filteredUsers: User[] = [];
  invitation: CommunityInvitationDto = new CommunityInvitationDto();
  invitations: CommunityInvitation[] = [];
  pendingInvitationRemoval = false;
  pendingInvitationSent = false;
  canManageInvitations: boolean;

  /*
   * Manage candidacies
   */
  candidacies: CommunityCandidacy[] = [];
  canManageCandidacies: boolean;

  /*
   * Manage memberships
   */
  memberships: CommunityMembership[] = [];
  membershipToEdit: CommunityMembership;
  membershipNewRole: CommunityRole;
  canUpdateMemberships: boolean;

  /*
   * Manage roles
   */
  roles: CommunityRole[] = [];
  rolesFetched = false;
  selectedRole: CommunityRole;
  canUpdateRoles: boolean;

  /*
   * Manage community
   */
  community: Community;

  private destroyed$ = new Subject<void>();

  constructor (public dialogRef: MatDialogRef<CommunityManageTeamDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private userService: UserService,
              private communityMembershipService: CommunityMembershipService,
              private communityRoleService: CommunityRoleService,
              private communityInvitationService: CommunityInvitationService) {
    /*
     * Fill current attributes with passed value
     */
    data.memberships !== null ? this.memberships = data.memberships : this.memberships = [];
    data.candidacies !== null ? this.candidacies = data.candidacies : this.candidacies = [];
    data.invitations !== null ? this.invitations = data.invitations : this.invitations = [];
    this.canManageCandidacies = data.canManageCandidacies;
    this.canManageInvitations = data.canManageInvitations;
    this.canManageInvitations = data.canManageInvitations;
    this.canUpdateMemberships = data.canUpdateMemberships;
    this.canUpdateRoles = data.canUpdateRoles;
    this.community = data.community;

    this.membershipToEdit = new CommunityMembership();
    this.invitation = new CommunityInvitationDto();

    this.userCtrl = new FormControl();
    this.userCtrl.valueChanges
      .pipe(
        takeUntil(this.destroyed$),
        debounceTime(300),
        distinctUntilChanged()
      )
      .subscribe(
        name => {
          this.filteredUsers = [];
          if (name != null && name.length >= 2) {
            this.userService.getUsers({ name: name })
              .pipe(takeUntil(this.destroyed$))
              .subscribe(
                users => {
                  this.filteredUsers = users;
                },
                err => console.log(err)
              );
          }
        }
      );
  }

  ngOnInit () {
    this.communityRoleService.getCommunityRoles(this.community.hash)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        roles => {
          this.roles = roles;
          this.selectedRole = roles[0];
        },
        err => console.log(err),
        () => this.rolesFetched = true
      );
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  /*
   * Membership management
   */

  editMembership (membership: CommunityMembership): void {
    if (membership !== this.membershipToEdit) {
      this.membershipNewRole = this.roles.find(r => r.hash === membership.role.hash);
      this.membershipToEdit = Object.assign({}, membership);
    }
  }

  saveMembershipChanges (): void {
    const updatedMembership = new CommunityMembershipDto();
    updatedMembership.role = this.membershipToEdit.role.hash;
    updatedMembership.user = this.membershipToEdit.user.username;
    this.communityMembershipService.putCommunityMembership(this.membershipToEdit.hash, updatedMembership)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        data => {
          this.memberships.find(m => m.hash === data.hash).role = (data.role);
          this.membershipToEdit = new CommunityMembership();
        }
      );
  }

  abortMembershipChange (): void {
    this.membershipToEdit = new CommunityMembership();
  }

  deleteMembership (): void {
    this
      .communityMembershipService
      .deleteCommunityMembership(this.membershipToEdit.hash)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        () => {
          const index = this.memberships.findIndex(el => el.hash === this.membershipToEdit.hash);
          this.memberships.splice(index, 1);
          this.membershipToEdit = new CommunityMembership();
        }
      );
  }

  /*
   * Invitation management
   */

  isInvited (user): boolean {
    return (this.invitations.find(i => i.user.username === user.username) !== undefined);
  }

  isMember (user): boolean {
    return (this.memberships.find(m => m.user.username === user.username) !== undefined);
  }

  isCandidate (user): boolean {
    return (this.candidacies.find(c => c.user.username === user.username) !== undefined);
  }

  canInvite (user: User): boolean {
    return !(this.isInvited(user) || this.isMember(user) || this.isCandidate(user));
  }

  triggerInvitation (user: User): void {
    this.invitation = new CommunityInvitationDto();
    this.invitation.user = user.username;
  }

  sendInvitation (): void {
    this.pendingInvitationSent = true;
    this.communityInvitationService.postCommunityInvitation(this.community.hash, this.invitation)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        data => {
          this.invitations.push(data);
          this.invitation = new CommunityInvitationDto();
        },
        err => console.log(err),
        () => this.pendingInvitationSent = false
      );
  }

  removeInvitation (invitation: CommunityInvitation): void {
    this.pendingInvitationRemoval = true;
    this.communityInvitationService.deleteCommunityInvitation(invitation.hash)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        () => this.invitations.splice(this.invitations.indexOf(invitation), 1),
        err => console.log(err),
        () => this.pendingInvitationRemoval = false
      );
  }

  /*
   * Candidacy management
   */
  candidacyAccepted (candidacy: CommunityCandidacy): void {
    this.removeCandidacy(candidacy);
    this.communityMembershipService.getCommunityMemberships({ community: this.community.hash })
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        data => this.memberships = data,
        err => console.error(err)
      );
  }

  removeCandidacy (candidacy: CommunityCandidacy): void {
    this.candidacies.splice(this.candidacies.indexOf(candidacy), 1);
  }

  /*
   * Role management
   */

  selectRole (role: CommunityRole): void {
    this.selectedRole = role;
  }

  newRole (): void {
    this.selectedRole = new CommunityRole();
  }

  updateRole (role: CommunityRole): void {
    if (this.roles.some(r => r.hash === role.hash)) {
      this.roles.find(r => r.hash === role.hash).permissions = role.permissions;
      this.roles.find(r => r.hash === role.hash).defaultMember = role.defaultMember;
    } else {
      this.roles.push(role);
      this.selectedRole = role;
    }
  }

  deleteRole (role: CommunityRole): void {
    const index = this.roles.findIndex(r => r.hash === role.hash);
    if (index >= 0) {
      this.roles.splice(index, 1);
      this.selectedRole = this.roles[0];
    }
  }
}
