import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Community } from '../../shared/models/community/community.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { CommunityDto } from '../shared/community.model.dto';

@Component({
  selector: 'treviz-community-form',
  templateUrl: './community-form.component.html',
  styleUrls: ['./community-form.component.scss']
})
export class CommunityFormComponent implements OnInit, OnDestroy {
  @Input() community: Community;
  @Output() onSubmit = new EventEmitter<{ communityDto: CommunityDto; logo: File|null }>();
  @Output() onStatusChange = new EventEmitter<boolean>();

  communityDto: CommunityDto;
  logo: File;
  form: FormGroup;
  destroyed$ = new Subject<void>();

  constructor (private formBuilder: FormBuilder) { }

  ngOnInit (): void {
    if (null != this.community) {
      this.communityDto = CommunityDto.fromCommunity(this.community);
    } else {
      this.communityDto = new CommunityDto();
    }

    this.form = this.formBuilder.group({
      name: [this.communityDto.name, [Validators.required, Validators.maxLength(64)]],
      description: [this.communityDto.description, Validators.required]
    });

    this.form.statusChanges
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        () => this.onStatusChange.emit(this.form.valid)
      );
  }

  ngOnDestroy (): void {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  setFile (file: File) {
    this.logo = file;
  }

  submit () {
    this.communityDto.name = this.form.value.name;
    this.communityDto.description = this.form.value.description;

    this.onSubmit.emit({
      communityDto: this.communityDto,
      logo: this.logo
    });
  }
}
