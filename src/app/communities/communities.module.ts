import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

import { CommunitiesRoutingModule } from './communities-routing.module';
import { CommunityDetailComponent } from './community-detail/community-detail.component';
import { CommunityListComponent } from './community-list/community-list.component';
import { CommunityCreateComponent } from './community-create/community-create.component';
import { CommunityEditDialogComponent } from './community-detail/community-edit-dialog.component';
import { CommunityManageTeamDialogComponent } from './community-manage-team-dialog/community-manage-team-dialog.component';
import { CommunityRoleManagementComponent } from './community-role-management/community-role-management.component';
import { CommunityNotificationPreferencesDialogComponent } from './community-notification-preferences-dialog/community-notification-preferences-dialog.component';
import { CommunityFormComponent } from './community-form/community-form.component';

@NgModule({
  imports: [
    SharedModule,
    CommunitiesRoutingModule
  ],
  declarations: [
    CommunityCreateComponent,
    CommunityDetailComponent,
    CommunityEditDialogComponent,
    CommunityListComponent,
    CommunityManageTeamDialogComponent,
    CommunityRoleManagementComponent,
    CommunityNotificationPreferencesDialogComponent,
    CommunityFormComponent
  ],
  exports: [],
  providers: []
})
export class CommunitiesModule { }
