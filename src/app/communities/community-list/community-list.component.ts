import {Component, OnDestroy, OnInit} from '@angular/core';
import {Community} from '../../shared/models/community/community.model';
import {CommunityService} from '../../core/services/community/community.service';
import {debounceTime, distinctUntilChanged, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

/**
 * Created by huber on 29/04/2017.
 */
@Component({
  selector: 'app-community-list',
  templateUrl: 'community-list.component.html',
  styleUrls: ['community-list.component.scss']
})
export class CommunityListComponent implements OnInit, OnDestroy {
    communities: Community[] = [];
    personalCommunities: Community[] = [];
    communitiesFetched = false;
    personalCommunitiesFetched = false;
    moreToLoad = true;
    searchStream = new Subject<string>();
    private destroyed$ = new Subject<void>();
    private params = {
      name: '',
      offset: 0
    };

    constructor (private communityService: CommunityService) {
      this.searchStream
        .pipe(
          debounceTime(300),
          distinctUntilChanged(),
          takeUntil(this.destroyed$)
        )
        .subscribe(
          name => {
            this.params.name = name;
            this.getCommunities(false);
          }
        );
    }

    ngOnInit () {
      const username = localStorage.getItem('username');
      this.communityService.getCommunities({ user: username, nb: 1000 })
        .pipe(takeUntil(this.destroyed$))
        .subscribe(
          communities => this.personalCommunities = communities,
          err => console.log(err),
          () => this.personalCommunitiesFetched = true
        );

      this.getCommunities(false);
    }

    ngOnDestroy () {
      this.destroyed$.next();
      this.destroyed$.complete();
    }

    loadMore () {
      this.params.offset = this.communities.length;
      this.getCommunities(true);
    }

    searchByName (name: string) {
      this.searchStream.next(name);
    }

    getCommunities (append = true) {
      this.communitiesFetched = false;
      this.communityService.getCommunities(this.params)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(
          communities => {
            this.moreToLoad = communities.length === 20;
            if (append) {
              for (const community of communities) {
                this.communities.push(community);
              }
            } else {
              this.communities = communities;
            }
          },
          err => console.log(err),
          () => this.communitiesFetched = true
        );
    }
}
