import { Component, OnDestroy } from '@angular/core';
import { CommunityService } from '../../core/services/community/community.service';
import { Router } from '@angular/router';
import { ChatRoomService } from '../../core/services/chat/chat-room.service';
import { CommunityDto } from '../shared/community.model.dto';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MessageDialogComponent } from 'app/shared/dialogs/message-dialog/message-dialog.component';
import { take, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ChatStateService } from '../../core/services/chat-state.service';
import { UserStateService } from '../../core/services/user-state.service';

/**
 * Created by huber on 25/05/2017.
 */
@Component({
  selector: 'app-community-create',
  templateUrl: 'community-create.component.html',
  styleUrls: ['community-create.component.scss']
})
export class CommunityCreateComponent implements OnDestroy {
  public isFormValid = false;
  public submitted = false;
  private destroyed$ = new Subject<void>();

  constructor (private communityService: CommunityService,
              private router: Router,
              private chatService: ChatRoomService,
              private chatStateService: ChatStateService,
              private userStateService: UserStateService,
              private dialog: MatDialog) { }

  ngOnDestroy (): void {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  onStatusChange (status) {
    this.isFormValid = status;
  }

  onSubmit ({ communityDto, logo }: { communityDto: CommunityDto; logo: File|null}) {
    this.submitted = true;
    communityDto.isOpen = communityDto.isOpen && communityDto.isVisible; // communities can be open only if they are also public.

    this.communityService.postCommunity(communityDto)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        community => {
          this.chatService.getRooms({ community: community.hash })
            .pipe(take(1))
            .subscribe(
              rooms => {
                this.chatStateService.addRooms(rooms);
                this.userStateService.refreshCommunities();

                if (null != logo) {
                  this.communityService.postLogo(community.hash, logo)
                    .pipe(take(1))
                    .subscribe(
                      () => this.router.navigate(['/communities', community.hash]),
                      () => {
                        this.displayErrorDialog('An error occurred posting your logo, please check it weights less than 1MB.');
                        this.router.navigate(['/communities', community.hash]);
                      }
                    );
                } else {
                  this.router.navigate(['/communities', community.hash]);
                }
              },
              () => {
                this.displayErrorDialog('An error occurred while fetching your community\'s chatroom');
                this.router.navigate(['/communities', community.hash]);
              }
            );
        },
        () => {
          this.submitted = false;
          this.displayErrorDialog('An error occurred while creating your community, please try again later.');
        }
      );
  }

  displayErrorDialog (message: string) {
    const config = new MatDialogConfig();
    config.data = {
      message,
      failure: true,
      actionRequired: true
    };

    this.dialog.open(MessageDialogComponent, config);
  }
}
