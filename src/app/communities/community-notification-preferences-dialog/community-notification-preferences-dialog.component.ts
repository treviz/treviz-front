import { CommunityPreferencesService } from '../../core/services/community/community-preferences.service';
import { OnDestroy, Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CommunityMembership } from '../../shared/models/community/community-membership.model';
import { CommunityNotificationPreferences } from '../../shared/models/community/community-notification-preferences.model';
import { finalize, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-community-notification-preferences-dialog',
  templateUrl: 'community-notification-preferences-dialog.component.html',
  styleUrls: ['community-notification-preferences-dialog.component.scss']
})
export class CommunityNotificationPreferencesDialogComponent implements OnDestroy {
    public communityNotificationPreferences = new CommunityNotificationPreferences();
    public loading = true;
    private destroyed$ = new Subject<void>();
    private membership;

    constructor (public dialogRef: MatDialogRef<CommunityNotificationPreferencesDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public data: CommunityMembership,
                private preferencesService: CommunityPreferencesService,
                public snackBar: MatSnackBar) {
      this.membership = data;
      this.preferencesService.getCommunityPreferences(data.hash)
        .pipe(
          finalize(() => this.loading = false),
          takeUntil(this.destroyed$)
        )
        .subscribe(
          prefs => this.communityNotificationPreferences = prefs,
          () => this.snackBar.open('Your preferences could not be fetched', 'Close', { duration: 3000 })
        );
    }

    ngOnDestroy () {
      this.destroyed$.next();
      this.destroyed$.complete();
    }

    updatePreferences () {
      this.loading = true;
      this.preferencesService
        .updatePreferences(this.membership.hash, this.communityNotificationPreferences)
        .pipe(
          finalize(() => this.loading = false),
          takeUntil(this.destroyed$)
        )
        .subscribe(
          () => this.snackBar.open('Your preferences have been updated !', 'Close', { duration: 3000 }),
          () => {
            this.snackBar.open('We could not update your preferences, try again later !', 'Close', { duration: 3000 });
          }
        );
    }
}
