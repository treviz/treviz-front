import { Component, EventEmitter, Input, OnChanges, OnDestroy, Output } from '@angular/core';
import { Community } from '../../shared/models/community/community.model';
import { CommunityRole } from '../../shared/models/community/community-role.model';
import { CommunityRoleService } from '../../core/services/community/community-role.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-community-role-management',
  templateUrl: 'community-role-management.component.html',
  styleUrls: ['community-role-management.component.scss']
})
export class CommunityRoleManagementComponent implements OnChanges, OnDestroy {
  @Input()
  community: Community;

  @Input()
  role: CommunityRole;

  @Output()
  onRoleUpdate = new EventEmitter<CommunityRole>();

  @Output()
  onRoleDelete = new EventEmitter<CommunityRole>();

  permissions = {
    UPDATE_COMMUNITY: false,
    DELETE_COMMUNITY: false,
    MANAGE_ROLE: false,
    MANAGE_MEMBERSHIP: false,
    MANAGE_POST: false,
    MANAGE_CANDIDACIES: false,
    MANAGE_INVITATIONS: false,
    MANAGE_DOCUMENT: false,
    MANAGE_BRAINSTORMING_SESSION: false,
    MANAGE_BRAINSTORMING_IDEAS: false
  };

  public submitted = false;
  private destroyed$ = new Subject<void>();
  private changes = true;

  constructor (private communityRoleService: CommunityRoleService,
              public snackBar: MatSnackBar) {
    this.role = new CommunityRole();
  }

  ngOnChanges () {
    for (const permission in this.permissions) {
      if (this.permissions.hasOwnProperty(permission)) {
        this.permissions[permission] = this.role.permissions.includes(permission);
      }
    }
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  onSubmit () {
    this.submitted = true;
    this.role.permissions = [];
    const permissionsNames = Object.getOwnPropertyNames(this.permissions);
    for (const permissionName of permissionsNames) {
      if (this.permissions[permissionName]) {
        this.role.permissions.push(permissionName);
      }
    }

    /*
     * If the roles does not have any hash, it is a new one that must be created.
     * Otherwise, update it.
     */
    if (!this.role.hash) {
      this.communityRoleService.postCommunityRole(this.community.hash, this.role)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(
          data => {
            this.role = data;
            this.changes = false;
            this.onRoleUpdate.emit(data);
            this.snackBar.open('Role successfully created', 'OK', {
              duration: 1000
            });
          },
          () => this.snackBar.open('An error occurred while creating the role', 'OK', {
            duration: 2000
          }),
          () => this.submitted = false
        );
    } else {
      const updatedRole = new CommunityRole();
      updatedRole.name = this.role.name;
      updatedRole.defaultMember = this.role.defaultMember;
      updatedRole.permissions = this.role.permissions;
      const hash = this.role.hash;
      this.communityRoleService.putCommunityRole(hash, updatedRole)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(
          data => {
            this.role = data;
            this.changes = false;
            this.onRoleUpdate.emit(data);
            this.snackBar.open('Role successfully updated', 'OK', {
              duration: 1000
            });
          },
          () => this.snackBar.open('An error occurred while updating the role', 'OK', {
            duration: 2000
          }),
          () => this.submitted = false
        );
    }
  }

  onDelete () {
    this.communityRoleService.deleteCommunityRole(this.role.hash)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        () => {
          this.onRoleDelete.emit(this.role);
          this.snackBar.open('Role successfully deleted', 'OK', {
            duration: 1000
          });
        },
        () => this.snackBar.open('This role could not be deleted. Maybe it is attributed to someone.', 'OK', {
          duration: 2000
        })
      );
  }
}
