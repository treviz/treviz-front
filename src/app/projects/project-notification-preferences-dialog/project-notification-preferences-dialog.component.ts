import { ProjectPreferencesService } from '../../core/services/project/project-preferences.service';
import { OnDestroy, Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ProjectMembership } from '../../shared/models/project/project-membership.model';
import { ProjectNotificationPreferences } from '../../shared/models/project/project-notification-preferences.model';
import { finalize, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-project-notification-preferences-dialog',
  templateUrl: 'project-notification-preferences-dialog.component.html',
  styleUrls: ['project-notification-preferences-dialog.component.scss']
})
export class ProjectNotificationPreferencesDialogComponent implements OnDestroy {
    public projectNotificationPreferences = new ProjectNotificationPreferences();
    public loading = true;
    private membership;
    private destroyed$ = new Subject<void>();

    constructor (public dialogRef: MatDialogRef<ProjectNotificationPreferencesDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public data: ProjectMembership,
                private preferencesService: ProjectPreferencesService,
                public snackBar: MatSnackBar) {
      this.membership = data;
      this.preferencesService.getProjectPreferences(data.hash)
        .pipe(
          finalize(() => this.loading = false),
          takeUntil(this.destroyed$)
        )
        .subscribe(
          prefs => this.projectNotificationPreferences = prefs,
          () => this.snackBar.open('Your preferences could not be fetched', 'Close', { duration: 3000 })
        );
    }

    ngOnDestroy () {
      this.destroyed$.next();
      this.destroyed$.complete();
    }

    updatePreferences () {
      this.loading = true;
      this.preferencesService.updatePreferences(this.membership.hash, this.projectNotificationPreferences)
        .pipe(
          finalize(() => this.loading = false),
          takeUntil(this.destroyed$)
        )
        .subscribe(
          () => this.snackBar.open('Your preferences have been updated !', 'Close', { duration: 3000 }),
          () => {
            this.snackBar.open('We could not update your preferences, try again later !', 'Close', { duration: 3000 });
          }
        );
    }
}
