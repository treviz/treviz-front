import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { User } from '../../shared/models/users/user.model';
import { FormControl } from '@angular/forms';
import { UserService } from '../../core/services/user.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ProjectInvitation } from '../../shared/models/project/project-invitation.model';
import { ProjectCandidacy } from '../../shared/models/project/project-candidacy.model';
import { ProjectMembership } from '../../shared/models/project/project-membership.model';
import { ProjectRole } from '../../shared/models/project/project-role.model';
import { ProjectRoleService } from '../../core/services/project/project-role.service';
import { Project } from '../../shared/models/project/project.model';
import { ProjectMembershipDto } from '../../shared/models/project/project-membership.model.dto';
import { ProjectMembershipService } from '../../core/services/project/project-membership.service';
import { ProjectInvitationService } from '../../core/services/project/project-invitation.service';
import { ProjectCandidacyService } from '../../core/services/project/project-candidacy.service';
import { ProjectInvitationDto } from '../../shared/models/project/project-invitation.model.dto';
import { debounceTime, distinctUntilChanged, finalize, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  moduleId: module.id,
  selector: 'app-project-manage-team-dialog',
  templateUrl: 'project-manage-team-dialog.component.html',
  styleUrls: ['project-manage-team-dialog.component.scss']
})
export class ProjectManageTeamDialogComponent implements OnInit, OnDestroy {
  /*
   * Invite users
   */
  userCtrl: FormControl;
  filteredUsers: User[] = [];
  invitation: ProjectInvitationDto = new ProjectInvitationDto();
  invitations: ProjectInvitation[] = [];
  invitationSubmitted = false;
  invitationPendingRemoval = false;

  /*
   * Manage candidacies
   */
  candidacies: ProjectCandidacy[] = [];
  candidacySubmitted = false;

  /*
   * Manage memberships
   */
  memberships: ProjectMembership[] = [];
  membershipToEdit: ProjectMembership;
  membershipNewRole: ProjectRole;
  membershipSubmitted = false;

  /*
   * Manage roles
   */
  roles: ProjectRole[] = [];
  rolesFetched = false;
  selectedRole: ProjectRole;

  /*
   * Manage project
   */
  project: Project;

  private destroyed$ = new Subject<void>();

  constructor (public dialogRef: MatDialogRef<ProjectManageTeamDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private userService: UserService,
              private projectMembershipService: ProjectMembershipService,
              private projectRoleService: ProjectRoleService,
              private projectInvitationService: ProjectInvitationService,
              private projectCandidacyService: ProjectCandidacyService) {
    /*
     * Fill current attributes with passed value
     */
    data.memberships !== null ? this.memberships = data.memberships : this.memberships = [];
    data.candidacies !== null ? this.candidacies = data.candidacies : this.candidacies = [];
    data.invitations !== null ? this.invitations = data.invitations : this.invitations = [];
    this.project = data.project;

    this.membershipToEdit = new ProjectMembership();
    this.invitation = new ProjectInvitationDto();

    this.userCtrl = new FormControl();
    this.userCtrl.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        name => {
          this.filteredUsers = [];
          if (name != null && name.length >= 2) {
            this.userService.getUsers({ name: name })
              .pipe(takeUntil(this.destroyed$))
              .subscribe(
                users => {
                  this.filteredUsers = users;
                },
                err => console.log(err)
              );
          }
        }
      );
  }

  ngOnInit () {
    this.projectRoleService.getProjectRoles(this.project.hash)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        roles => {
          this.roles = roles;
          this.selectedRole = roles[0];
        },
        err => console.log(err),
        () => this.rolesFetched = true
      );
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  /*
   * Membership management
   */

  editMembership (membership: ProjectMembership): void {
    if (membership !== this.membershipToEdit) {
      this.membershipNewRole = this.roles.find(r => r.hash === membership.role.hash);
      this.membershipToEdit = Object.assign({}, membership);
    }
  }

  saveMembershipChanges (): void {
    this.membershipSubmitted = true;
    const updatedMembership = new ProjectMembershipDto();
    updatedMembership.user = this.membershipToEdit.user.username;
    updatedMembership.role = this.membershipToEdit.role.hash;
    this.projectMembershipService.putProjectMembership(this.membershipToEdit.hash, updatedMembership)
      .pipe(
        finalize(() => this.membershipSubmitted = false),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        data => {
          this.memberships.find(m => m.hash === data.hash).role = (data.role);
          this.membershipToEdit = new ProjectMembership();
        }
      );
  }

  deleteMembership (): void {
    this.membershipSubmitted = true;
    this.projectMembershipService.deleteProjectMembership(this.membershipToEdit.hash)
      .pipe(
        finalize(() => this.membershipSubmitted = false),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        () => {
          const index = this.memberships.findIndex(el => el.hash === this.membershipToEdit.hash);
          this.memberships.splice(index, 1);
          this.membershipToEdit = new ProjectMembership();
        }
      );
  }

  abortMembershipChange (): void {
    this.membershipToEdit = new ProjectMembership();
  }

  /*
   * Invitation management
   */

  isInvited (user): boolean {
    return (this.invitations.find(i => i.user.username === user.username) !== undefined);
  }

  isMember (user): boolean {
    return (this.memberships.find(m => m.user.username === user.username) !== undefined);
  }

  isCandidate (user): boolean {
    return (this.candidacies.find(c => c.user.username === user.username) !== undefined);
  }

  canInvite (user: User): boolean {
    return !(this.isInvited(user) || this.isMember(user) || this.isCandidate(user));
  }

  triggerInvitation (user: User): void {
    this.invitation = new ProjectInvitationDto();
    this.invitation.user = user.username;
  }

  sendInvitation (): void {
    this.invitationSubmitted = true;
    this.projectInvitationService.postProjectInvitation(this.project.hash, this.invitation)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        data => {
          this.invitations.push(data);
          this.invitation = new ProjectInvitationDto();
          this.filteredUsers = [];
        },
        err => console.log(err),
        () => this.invitationSubmitted = false
      );
  }

  removeInvitation (invitation: ProjectInvitation): void {
    this.invitationPendingRemoval = true;
    this.projectInvitationService.deleteProjectInvitation(invitation.hash)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        () => this.invitations.splice(this.invitations.indexOf(invitation), 1),
        err => console.log(err),
        () => this.invitationPendingRemoval = false
      );
  }

  /*
   * Candidacy management
   */
  acceptCandidacy (candidacy: ProjectCandidacy): void {
    this.candidacySubmitted = true;
    this.projectCandidacyService.acceptProjectCandidacy(candidacy.hash)
      .pipe(
        finalize(() => this.candidacySubmitted = false),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        () => {
          this.candidacies.splice(this.invitations.indexOf(candidacy), 1);
          this.projectMembershipService.getProjectMemberships({ project: this.project.hash })
            .pipe(takeUntil(this.destroyed$))
            .subscribe(
              data => this.memberships = data,
              err => console.log(err)
            );
        },
        err => console.log(err)
      );
  }

  removeCandidacy (candidacy: ProjectCandidacy): void {
    this.candidacySubmitted = true;
    this.projectCandidacyService.deleteProjectCandidacy(candidacy.hash)
      .pipe(
        finalize(() => this.candidacySubmitted = false),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        () => {
          this.candidacies.splice(this.invitations.indexOf(candidacy), 1);
        },
        err => console.log(err)
      );
  }

  /*
   * Role management
   */

  selectRole (role: ProjectRole): void {
    this.selectedRole = role;
  }

  newRole (): void {
    this.selectedRole = new ProjectRole();
  }

  updateRole (role: ProjectRole): void {
    if (this.roles.find(r => r.hash === role.hash)) {
      this.roles.find(r => r.hash === role.hash).permissions = role.permissions;
      this.roles.find(r => r.hash === role.hash).defaultMember = role.defaultMember;
    } else {
      this.roles.push(role);
      this.selectedRole = role;
    }
  }

  deleteRole (role: ProjectRole): void {
    const index = this.roles.findIndex(r => r.hash === role.hash);
    if (index >= 0) {
      this.roles.splice(index, 1);
      this.selectedRole = this.roles[0];
    }
  }
}
