import { Component, OnInit } from '@angular/core';
import { SkillService } from '../../core/services/skill.service';
import { ProjectService } from '../../core/services/project/project.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TagService } from '../../core/services/tag.service';
import { CommunityService } from '../../core/services/community/community.service';
import { Community } from '../../shared/models/community/community.model';
import { ChatRoomService } from '../../core/services/chat/chat-room.service';
import { ProjectDto } from '../../shared/models/project/project.model.dto';
import { BrainstormingIdea } from '../../shared/models/brainstorming/brainstorming-idea.model';
import { BrainstormingIdeaService } from '../../core/services/brainstorming/brainstorming-idea.service';
import { Project } from '../../shared/models/project/project.model';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { MessageDialogComponent } from 'app/shared/dialogs/message-dialog/message-dialog.component';
import { finalize, takeUntil } from 'rxjs/operators';
import { mapToArray } from '../../../helpers/map';
import { AbstractTagSkillComponent } from '../../shared/abstract/abstract-tag-skill-component';

/**
 * Created by Bastien on 15/02/2017.
 */
@Component({
  selector: 'app-create-project',
  templateUrl: 'project-create.component.html',
  styleUrls: ['project-create.component.scss']
})
export class ProjectCreateComponent extends AbstractTagSkillComponent implements OnInit {
  public projectDto: ProjectDto;

  public searchCommunityInput = '';
  public searchingCommunities = false;
  public communitiesMap = new Map<string, Community>();
  public get filteredCommunities (): Community[] {
    return mapToArray(this.communitiesMap)
      .filter((community: Community) => {
        return community.isSelected || new RegExp(this.searchCommunityInput, 'gi').test(community.name);
      });
  }

  projectBaseIllustrations;
  logoBeingUploaded: File;

  submitted = false;

  idea: BrainstormingIdea;
  ideaFetched = false;
  parentProject: Project;
  parentProjectFetched = false;

  get valid () {
    return (null != this.projectDto.name &&
      this.projectDto.name !== '' &&
      this.projectDto.shortDescription != null &&
      this.projectDto.shortDescription !== '' &&
      this.projectDto.shortDescription.length < 140 &&
      this.projectDto.description !== '' &&
      this.projectDto.description != null &&
      this.projectDto.isVisible != null
    );
  }

  constructor (protected skillService: SkillService,
              protected tagService: TagService,
              private projectService: ProjectService,
              private chatService: ChatRoomService,
              private communityService: CommunityService,
              private router: Router,
              private route: ActivatedRoute,
              private brainstormingIdeaService: BrainstormingIdeaService,
              private dialog: MatDialog) {
    super(skillService, tagService);
    this.projectDto = new ProjectDto();
  }

  ngOnInit (): void {
    this.communityService.getCommunities({ user: localStorage.getItem('username'), nb: 1000 })
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        availableCommunities => {
          this.setCommunities(availableCommunities);
          this.route.queryParams
            .pipe(takeUntil(this.destroyed$))
            .subscribe((params: Params) => {
              const idea = params.idea;
              const project = params.project;
              if (idea) {
                this.brainstormingIdeaService.getIdea(idea)
                  .pipe(
                    finalize(() => this.ideaFetched = true),
                    takeUntil(this.destroyed$)
                  )
                  .subscribe(
                    data => {
                      this.idea = data;
                      this.projectDto.shortDescription = data.content.substring(0, 140);
                      this.projectDto.description = data.content;
                      this.projectDto.idea = idea;
                      try {
                        const ideaCommunity = this.communitiesMap.get(data.session.community.hash);
                        if (ideaCommunity) {
                          ideaCommunity.isSelected = true;
                        }
                      } catch {
                        console.log('The idea this project is based one does not belong to a community');
                      }
                    }
                  );
              }

              if (project) {
                this.projectService.getProject(project)
                  .pipe(
                    finalize(() => this.parentProjectFetched = true),
                    takeUntil(this.destroyed$)
                  )
                  .subscribe(
                    data => {
                      this.parentProject = data;
                      this.projectDto.name = `${data.name} (Fork)`;
                      this.projectDto.shortDescription = data.shortDescription;
                      this.projectDto.description = data.description;
                      this.projectDto.skills = data.skills.map((skill) => skill.name);
                      this.projectDto.tags = data.tags.map((skill) => skill.name);
                      this.projectDto.communities = null != data.communities ? data.communities.map((community) => community.hash) : [];
                      this.projectDto.isVisible = data.isVisible;
                    },
                    err => console.log(err)
                  );
              }
            });
        },
        err => console.log(err)
      );

    this.projectService.getProjectBaseIllustration()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        data => this.projectBaseIllustrations = data,
        err => console.log(err)
      );
  }

  onSubmit () {
    this.projectDto.isOpen = true;
    this.submitted = true;
    this.projectDto.communities = mapToArray(this.communitiesMap)
      .filter((community) => community.isSelected)
      .map((community) => community.hash);

    this.projectService.postProject(this.projectDto)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        project => {
          this.chatService.getRooms({ project: project.hash })
            .pipe(takeUntil(this.destroyed$))
            .subscribe(
              rooms => {
                project.rooms = rooms;
                this.chatService.projects.push(project);

                if (this.logoBeingUploaded != null) {
                  this.projectService.postLogo(this.logoBeingUploaded, project.hash)
                    .subscribe(
                      () => this.router.navigate(['/projects', project.hash])
                    );
                } else {
                  this.router.navigate(['/projects', project.hash]);
                }
              },
              () => {
                this.displayErrorDialog('An error occurred while fetching your project\'s chatroom');
                this.router.navigate(['/projects', project.hash]);
              }
            );
        },
        () => {
          this.displayErrorDialog('An error occurred while creating the project, please try again later');
          this.submitted = false;
        }
      );
  }

  setSkills (skills: string[]) {
    this.projectDto.skills = skills;
  }

  setTags (tags: string[]) {
    this.projectDto.tags = tags;
  }

  setCommunities (communities: Array<Community>) {
    communities.forEach((community) => {
      if (null == this.communitiesMap.get(community.hash)) {
        this.communitiesMap.set(community.hash, community);
      }
    });
    this.searchingCommunities = false;
  }

  triggerSelect (community: Community) {
    community.isSelected = !community.isSelected;
  }

  selectPicture (imageUrl: string) {
    if (this.projectDto.logoUrl === imageUrl) {
      this.projectDto.logoUrl = '';
    } else {
      this.projectDto.logoUrl = imageUrl;
    }
  }

  setFile (file: File) {
    this.logoBeingUploaded = file;
  }

  displayErrorDialog (message: string) {
    const config = new MatDialogConfig();
    config.data = {
      message,
      failure: true,
      actionRequired: true
    };

    this.dialog.open(MessageDialogComponent, config);
  }
}
