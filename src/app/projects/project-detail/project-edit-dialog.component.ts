import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { SkillService } from '../../core/services/skill.service';
import { ProjectService } from '../../core/services/project/project.service';
import { CommunityService } from '../../core/services/community/community.service';
import { TagService } from '../../core/services/tag.service';
import { Project } from '../../shared/models/project/project.model';
import { Community } from '../../shared/models/community/community.model';
import { ProjectDto } from '../../shared/models/project/project.model.dto';
import { debounceTime, distinctUntilChanged, finalize, take, takeUntil } from 'rxjs/operators';
import { mapToArray } from '../../../helpers/map';
import { AbstractTagSkillComponent } from '../../shared/abstract/abstract-tag-skill-component';

/**
 * Created by Bastien on 26/02/2017.
 */
@Component({
  selector: 'app-edit-project-dialog',
  templateUrl: 'project-edit-dialog.component.html',
  styleUrls: ['project-edit-dialog.component.scss']
})
export class ProjectEditDialogComponent extends AbstractTagSkillComponent implements OnInit {
  public searchCommunityStream = new Subject<string>();
  public searchCommunityInput = '';
  public searchingCommunities = false;
  public communitiesMap = new Map<string, Community>();
  public get filteredCommunities (): Community[] {
    return mapToArray(this.communitiesMap)
      .filter((community: Community) => {
        return community.isSelected || new RegExp(this.searchCommunityInput, 'gi').test(community.name);
      });
  }

  public projectDto: ProjectDto;

  // True when request is being made.
  public pendingChange = false;

  constructor (public dialogRef: MatDialogRef<ProjectEditDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Project,
              protected skillService: SkillService,
              protected tagService: TagService,
              private communityService: CommunityService,
              private projectService: ProjectService) {
    super(skillService, tagService);
    this.projectDto = ProjectDto.fromProject(data);
  }

  /*
   * Load the communities of the project and those of the user.
   */
  ngOnInit (): void {
    this.loadProjectCommunities();
    this.searchCommunities('');
    this.initCommunitySearch();
  }

  initCommunitySearch () {
    this.searchCommunityStream
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        takeUntil(this.destroyed$)
      )
      .subscribe((value) => this.searchCommunities(value));
  }

  setTags (tags: string[]) {
    this.projectDto.tags = tags;
  }

  setSkills (skills: string[]) {
    this.projectDto.skills = skills;
  }

  loadProjectCommunities () {
    this.communityService
      .getCommunities({ project: this.data.hash, nb: 1000 })
      .pipe(
        takeUntil(this.destroyed$),
        finalize(() => this.searchingCommunities = false)
      )
      .subscribe(
        communities => this.addCommunities(communities, true),
        err => console.log(err)
      );
  }

  triggerCommunitySearch (name: string) {
    this.searchCommunityInput = name;
    this.searchCommunityStream.next(name);
  }

  searchCommunities (name: string) {
    this.communityService
      .getCommunities({ name, user: localStorage.getItem('username') })
      .pipe(
        take(1),
        takeUntil(this.destroyed$),
        finalize(() => this.searchingCommunities = false)
      )
      .subscribe(
        communities => this.addCommunities(communities, false),
        err => console.log(err)
      );
  }

  addCommunities (communities: Array<Community>, selected = false) {
    communities.forEach((community) => {
      community.isSelected = selected;
      if (selected || null == this.communitiesMap.get(community.hash)) {
        this.communitiesMap.set(community.hash, community);
      }
    });
    this.searchingCommunities = false;
  }

  triggerSelect (community: Community) {
    community.isSelected = !community.isSelected;
  }

  putProject () {
    this.pendingChange = true;
    this.projectDto.isOpen = true;
    this.projectDto.communities = this
      .filteredCommunities
      .filter((community) => community.isSelected)
      .map((community) => community.hash);

    this.projectService.putProject(this.data.hash, this.projectDto)
      .pipe(take(1), takeUntil(this.destroyed$))
      .subscribe(
        data => this.dialogRef.close(data),
        () => this.dialogRef.close(false)
      );
  }

  deleteProjectIntent () {
    this.dialogRef.close(null);
  }
}
