import {Component, OnInit} from '@angular/core';
import {ProjectService} from '../../core/services/project/project.service';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Location} from '@angular/common';
import {Project} from '../../shared/models/project/project.model';
import {ProjectMembershipService} from '../../core/services/project/project-membership.service';
import {ProjectInvitationService} from '../../core/services/project/project-invitation.service';
import {ProjectCandidacyService} from '../../core/services/project/project-candidacy.service';
import {ProjectMembership} from '../../shared/models/project/project-membership.model';
import {ProjectCandidacy} from '../../shared/models/project/project-candidacy.model';
import {ProjectInvitation} from '../../shared/models/project/project-invitation.model';
import {ProjectRole} from '../../shared/models/project/project-role.model';
import {ProjectEditDialogComponent} from './project-edit-dialog.component';
import {ProjectManageTeamDialogComponent} from '../project-manage-team-dialog/project-manage-team-dialog.component';
import {CandidacyDialogComponent} from '../../shared/candidacy/candidacy-dialog/candidacy-dialog.component';
import {ProjectNotificationPreferencesDialogComponent} from '../project-notification-preferences-dialog/project-notification-preferences-dialog.component';
import {take, takeUntil} from 'rxjs/operators';
import {AbstractProjectConfirmComponent} from '../../shared/abstract/abstract-project-confirm-component';
import {ConfirmMessageModel} from '../../shared/models/confirm-message.model';
import {resizeImage} from "../../../helpers/file-helper";

/**
 * Created by Bastien on 26/02/2017.
 */
@Component({
  selector: 'app-project-detail',
  templateUrl: 'project-detail.component.html',
  styleUrls: ['project-detail.component.scss']
})
export class ProjectDetailComponent extends AbstractProjectConfirmComponent implements OnInit {
  project: Project;

  memberships: ProjectMembership[] = [];
  userMembership: ProjectMembership = null;
  membershipsFetched = false;
  membershipSource: MatTableDataSource<ProjectMembership>;
  membershipsColumns = ['user', 'role'];

  candidacies: ProjectCandidacy[] = [];
  userCandidacy: ProjectCandidacy = null;
  candidaciesFetched = false;

  invitations: ProjectInvitation[] = [];
  userInvitation: ProjectInvitation = null;
  invitationsFetched = false;

  logoBeingUploaded = false;
  pendingAction = false;

  constructor (private projectService: ProjectService,
              private projectMembershipService: ProjectMembershipService,
              private projectInvitationService: ProjectInvitationService,
              private projectCandidacyService: ProjectCandidacyService,
              private route: ActivatedRoute,
              private location: Location,
              public dialog: MatDialog,
              public router: Router) {
    super(dialog);
  }

  ngOnInit () {
    this.fetchData();
  }

  private fetchData () {
    this.route.params
      .pipe(takeUntil(this.destroyed$))
      .subscribe((params: Params) => {
        this.clear();

        const hash = params.hash;

        this.projectService.getProject(hash)
          .pipe(takeUntil(this.destroyed$))
          .subscribe(project => {
            this.project = project;
          },
          err => {
            console.log(err);
            this.router.navigate(['/projects']);
          }
          );

        this.userMembership = new ProjectMembership();
        this.userMembership.role = new ProjectRole();

        this.updateMemberships(hash);

        this.projectCandidacyService.getProjectCandidacies({ project: hash })
          .pipe(takeUntil(this.destroyed$))
          .subscribe(
            candidacies => {
              this.candidacies = candidacies;
              this.userCandidacy = candidacies.find((s: ProjectCandidacy) => (s.user.username === localStorage.getItem('username')));
            },
            err => console.log(err),
            () => this.candidaciesFetched = true
          );

        this.projectInvitationService.getProjectInvitations({ project: hash })
          .pipe(takeUntil(this.destroyed$))
          .subscribe(
            invitations => {
              this.invitations = invitations;
              this.userInvitation = invitations.find((s: ProjectInvitation) => (s.user.username === localStorage.getItem('username')));
            },
            err => console.log(err),
            () => this.invitationsFetched = true
          );
      });
  }

  updateMemberships (hash) {
    this.projectMembershipService.getProjectMemberships({ project: hash })
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        memberships => {
          this.memberships = memberships;
          this.userMembership = memberships.find((s: ProjectMembership) => (s.user.username === localStorage.getItem('username')));
          this.membershipSource = new MatTableDataSource<ProjectMembership>(memberships);

          if (window.location.hash === '#preferences' && this.userMembership != null) {
            this.openNotificationPreferencesDialog();
          }
        },
        err => console.log(err),
        () => this.membershipsFetched = true
      );
  }

  declineCandidacy (candidacy: ProjectCandidacy): void {
    this.pendingAction = true;
    this.projectCandidacyService.deleteProjectCandidacy(candidacy.hash)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        () => this.removeCandidacy(candidacy),
        () => this.displayErrorDialog('An error occurred while deleting the candidacy'),
        () => this.pendingAction = false
      );
  }

  acceptInvitation (): void {
    this.pendingAction = true;
    this.projectInvitationService.acceptProjectInvitation(this.userInvitation.hash)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        () => {
          this.clear();
          this.fetchData();
        },
        () => {
          this.pendingAction = false;
          this.displayErrorDialog('An error occurred while accepting the invitation');
        }
      );
  }

  deleteInvitation (invitation: ProjectInvitation): void {
    this.pendingAction = true;
    this.projectInvitationService.deleteProjectInvitation(invitation.hash)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        () => this.invitations.splice(this.invitations.indexOf(invitation), 1),
        () => {
          this.pendingAction = false;
          this.displayErrorDialog('An error occurred while deleting the invitation');
        }
      );
  }

  handleCandidacyAccepted (candidacy: ProjectCandidacy) {
    this.removeCandidacy(candidacy);
    this.updateMemberships(this.project.hash);
  }

  removeCandidacy (candidacy: ProjectCandidacy) {
    this.candidacies.splice(this.candidacies.indexOf(candidacy), 1);
    if (this.userCandidacy === candidacy) {
      this.userCandidacy = null;
    }
  }

  hasPermission (permission: string): boolean {
    try {
      return this.userMembership.role.permissions.find(a => a === permission) !== null;
    } catch (e) {
      return false;
    }
  }

  async changeLogo (event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];

      this.logoBeingUploaded = true;
      const resizedFile = await resizeImage(file, 360);

      this.projectService.postLogo(resizedFile, this.project.hash)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(
          data => {
            this.project = data;
            this.logoBeingUploaded = false;
          },
          () => {
            this.displayErrorDialog('The picture could not be uploaded; it should weight less than 1MB');
            this.logoBeingUploaded = false;
          }
        );
    }
  }

  openEditMode () {
    const config = new MatDialogConfig();
    config.data = this.project;

    this.dialog
      .open(ProjectEditDialogComponent, config)
      .afterClosed()
      .pipe(take(1), takeUntil(this.destroyed$))
      .subscribe(result => {
        if (result != null) {
          if (result) {
            this.project = result;
          } else {
            this.displayErrorDialog('An error occurred while updating the project.');
          }
        } else if (null === result) {
          this.confirmAction(
            ConfirmMessageModel.PROJECT_DELETE,
            () => this.deleteProject()
          );
        }
      });
  }

  openTeamManagementDialog () {
    const config = new MatDialogConfig();

    config.data = {
      memberships: this.memberships,
      invitations: this.invitations,
      candidacies: this.candidacies,
      project: this.project
    };

    this.dialog.open(ProjectManageTeamDialogComponent, config);
  }

  openCandidacyDialog () {
    const config = new MatDialogConfig();
    config.data = {
      project: this.project
    };

    const dialogRef = this.dialog.open(CandidacyDialogComponent, config);

    dialogRef.afterClosed()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(result => {
        if (result != null) {
          if (result) {
            this.userCandidacy = result;
          } else {
            this.displayErrorDialog('An error occurred while sending the candidacy.');
          }
        }
      });
  }

  openNotificationPreferencesDialog () {
    const config = new MatDialogConfig();
    config.data = this.userMembership;

    this.dialog.open(ProjectNotificationPreferencesDialogComponent, config);
  }

  private clear () {
    this.membershipsFetched = false;
    this.project = null;
    this.memberships = [];
    this.candidacies = [];
    this.invitations = [];
    this.logoBeingUploaded = false;
    this.userInvitation = null;
  }

  deleteProject () {
    this.projectService.deleteProject(this.project.hash)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        () => this.location.back(),
        () => this.displayErrorDialog('An error occurred while deleting the project')
      );
  }

  leaveProject() {
    this.confirmAction(
      ConfirmMessageModel.PROJECT_LEAVE,
      () => this
        .projectMembershipService
        .deleteProjectMembership(this.userMembership.hash)
        .pipe(take(1))
        .subscribe(() => this.userMembership = null)
    )
  }
}
