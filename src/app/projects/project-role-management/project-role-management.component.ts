import { Component, EventEmitter, Input, OnChanges, OnDestroy, Output } from '@angular/core';
import { ProjectRole } from '../../shared/models/project/project-role.model';
import { ProjectRoleService } from '../../core/services/project/project-role.service';
import { Project } from '../../shared/models/project/project.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { finalize, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-project-role-management',
  templateUrl: 'project-role-management.component.html',
  styleUrls: ['project-role-management.component.scss']
})
export class ProjectRoleManagementComponent implements OnChanges, OnDestroy {
  @Input()
  project: Project;

  @Input()
  role: ProjectRole;

  @Output()
  onRoleUpdate = new EventEmitter<ProjectRole>();

  @Output()
  onRoleDelete = new EventEmitter<ProjectRole>();

  permissions = {
    UPDATE_PROJECT: false,
    DELETE_PROJECT: false,
    MANAGE_ROLE: false,
    MANAGE_MEMBERSHIP: false,
    MANAGE_POST: false,
    MANAGE_CANDIDACIES: false,
    MANAGE_INVITATIONS: false,
    MANAGE_DOCUMENT: false,
    MANAGE_KANBAN: false,
    MANAGE_CROWD_FUND: false,
    MANAGE_JOBS: false
  };

  submitted = false;

  private destroyed$ = new Subject<void>();

  constructor (private projectRoleService: ProjectRoleService,
              public snackBar: MatSnackBar) {
    this.role = new ProjectRole();
  }

  ngOnChanges () {
    for (const permission in this.permissions) {
      if (this.permissions.hasOwnProperty(permission)) {
        this.permissions[permission] = this.role.permissions.includes(permission);
      }
    }
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  onSubmit () {
    this.submitted = true;
    this.role.permissions = [];
    const permissionsNames = Object.getOwnPropertyNames(this.permissions);
    for (const permissionName of permissionsNames) {
      if (this.permissions[permissionName]) {
        this.role.permissions.push(permissionName);
      }
    }

    /*
     * If the roles does not have any hash, it is a new one that must be created.
     * Otherwise, update it.
     */
    if (!this.role.hash) {
      this.projectRoleService.postProjectRole(this.project.hash, this.role)
        .pipe(takeUntil(this.destroyed$), finalize(() => this.submitted = false))
        .subscribe(
          data => {
            this.role = data;
            this.onRoleUpdate.emit(data);
            this.snackBar.open('Role successfully created', 'OK', {
              duration: 2000
            });
          },
          () => this.snackBar.open('An error occurred while creating the role', 'OK', {
            duration: 2000
          })
        );
    } else {
      const updatedRole = new ProjectRole();
      updatedRole.name = this.role.name;
      updatedRole.defaultMember = this.role.defaultMember;
      updatedRole.defaultCreator = this.role.defaultCreator;
      updatedRole.permissions = this.role.permissions;
      const hash = this.role.hash;
      this.projectRoleService.putProjectRole(hash, updatedRole)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(
          data => {
            this.role = data;
            this.submitted = false;
            this.onRoleUpdate.emit(data);
            this.snackBar.open('Role successfully updated', 'OK', {
              duration: 1000
            });
          },
          () => this.snackBar.open('An error occurred while updating the role', 'OK', {
            duration: 2000
          })
        );
    }
  }

  onDelete () {
    this.projectRoleService.deleteProjectRole(this.role.hash)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        () => {
          this.onRoleDelete.emit(this.role);
          this.snackBar.open('Role successfully deleted', 'OK', {
            duration: 1000
          });
        },
        () => this.snackBar.open('This role could not be deleted. Maybe it is attributed to someone.', 'OK', {
          duration: 2000
        })
      );
  }
}
