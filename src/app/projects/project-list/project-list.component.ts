import { Component, OnDestroy, OnInit } from '@angular/core';
import { ProjectService } from '../../core/services/project/project.service';
import { Project } from '../../shared/models/project/project.model';
import { take, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { SearchFilters } from '../../shared/filters/filter.component';

/**
 * Created by Bastien on 15/02/2017.
 */
@Component({
  selector: 'app-project-list',
  templateUrl: 'project-list.component.html',
  styleUrls: ['project-list.component.scss']
})
export class ProjectListComponent implements OnInit, OnDestroy {
  projects: Project[] = [];
  personalProjects: Project[] = [];

  private params = {
    name: '',
    tags: [],
    skills: [],
    offset: 0,
    nb: 10,
    user: '',
    role: '',
    community: ''
  };

  personalProjectsFetched = false;
  projectsFetched = false;
  moreToLoad = true;

  private destroyed$ = new Subject<void>();

  constructor (private projectService: ProjectService) {}

  ngOnInit (): void {
    this.searchProjects(false);

    this.projectService.getProjects({ user: localStorage.getItem('username'), nb: 1000 })
      .pipe(take(1), takeUntil(this.destroyed$))
      .subscribe(
        data => this.personalProjects = data,
        err => console.log(err),
        () => this.personalProjectsFetched = true
      );
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  search (params: SearchFilters) {
    this.params.skills = params.skills;
    this.params.tags = params.tags;
    this.params.name = params.name;
    this.searchProjects(false);
  }

  loadMore () {
    if (this.projectsFetched) {
      this.searchProjects(true);
    }
  }

  searchProjects (appendResults) {
    this.projectsFetched = false;
    if (appendResults) {
      this.params.offset = this.params.offset + 10;
    } else {
      this.params.offset = 0;
    }
    this.projectService.getProjects(this.params)
      .pipe(take(1), takeUntil(this.destroyed$))
      .subscribe(
        data => {
          this.moreToLoad = data.length === 10;
          if (appendResults) {
            for (const project of data) {
              this.projects.push(project);
            }
          } else {
            this.projects = data;
          }
          this.projectsFetched = true;
        },
        err => console.log(err),
        () => this.projectsFetched = true
      );
  }
}
