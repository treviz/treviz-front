import { Component, Input } from '@angular/core';
import { BrainstormingEnhancement } from '../../models/brainstorming/brainstorming-enhancement.model';

@Component({
  moduleId: module.id,
  selector: 'app-brainstorming-enhancement-card',
  templateUrl: 'brainstorming-enhancement-card.component.html',
  styleUrls: ['brainstorming-enhancement-card.component.scss']
})

export class BrainstormingEnhancementCardComponent {
  @Input() enhancement: BrainstormingEnhancement;
}
