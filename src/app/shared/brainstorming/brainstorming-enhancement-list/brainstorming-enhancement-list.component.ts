
import { finalize, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Component, Input, OnInit } from '@angular/core';
import { BrainstormingIdea } from '../../models/brainstorming/brainstorming-idea.model';
import { BrainstormingEnhancement } from '../../models/brainstorming/brainstorming-enhancement.model';
import { BrainstormingEnhancementService } from '../../../core/services/brainstorming/brainstorming-enhancement.service';
import { BrainstormingEnhancementDto } from '../../models/brainstorming/brainstorming-enhancement.model.dto';

@Component({
  moduleId: module.id,
  selector: 'app-brainstorming-enhancement-list',
  templateUrl: 'brainstorming-enhancement-list.component.html',
  styleUrls: ['brainstorming-enhancement-list.component.scss']
})

export class BrainstormingEnhancementListComponent implements OnInit {
  @Input() idea: BrainstormingIdea;

  enhancements: BrainstormingEnhancement[] = [];

  enhancementDto: BrainstormingEnhancementDto;

  enhancementsFetched = false;

  enhancementSubmitted = false;

  private destroyed$ = new Subject<void>();

  constructor (private brainstormingEnhancementService: BrainstormingEnhancementService) {
    this.enhancementDto = new BrainstormingEnhancementDto();
  }

  ngOnInit () {
    this.brainstormingEnhancementService.getIdeaEnhancements(this.idea.hash)
      .pipe(
        finalize(() => this.enhancementsFetched = true),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        data => this.enhancements = data,
        err => console.log(err)
      );
  }

  onSubmit () {
    this.enhancementSubmitted = true;
    this.brainstormingEnhancementService.postEnhancement(this.idea.hash, this.enhancementDto)
      .pipe(
        finalize(() => {
          this.enhancementSubmitted = false;
          this.enhancementDto.message = '';
        }),
        takeUntil(this.destroyed$)
      ).subscribe(
        data => this.enhancements.push(data),
        err => console.log(err)
      );
  }
}
