/**
 * Created by huber on 26/08/2017.
 */

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BrainstormingSession} from '../../models/brainstorming/brainstorming-session.model';
import {BrainstormingIdea} from '../../models/brainstorming/brainstorming-idea.model';
import {MatDialog} from "@angular/material/dialog";
import {BrainstormingSessionCreateDialogComponent} from "../brainstorming-session-create-dialog/brainstorming-session-create-dialog.component";
import {take} from "rxjs/operators";
import {UserStateService} from "../../../core/services/user-state.service";
import CommunityPermissions from "../../models/community/community-permissions.model";

@Component({
  selector: 'app-brainstorming-session-detail',
  templateUrl: 'brainstorming-session-detail.component.html',
  styleUrls: ['brainstorming-session-detail.component.css']
})
export class BrainstormingSessionDetailComponent implements OnInit {
  @Input()
  private session: BrainstormingSession;

  @Output()
  onSessionUnselect = new EventEmitter<boolean>();

  @Output()
  onSessionUpdate = new EventEmitter<BrainstormingSession>();

  selectedIdea: BrainstormingIdea;
  canEdit: boolean;

  constructor(public dialog: MatDialog,
              private userStateService: UserStateService) {}

  ngOnInit() {
    this.canEdit = this
        .userStateService
        .getCurrentUser()
        ?.communityMemberships
        ?.some((membership) => {
          return membership.community?.hash === this.session?.community?.hash
            && membership.role?.permissions?.includes(CommunityPermissions.MANAGE_BRAINSTORMING_SESSION);
        });
  }

  selectIdea (idea: BrainstormingIdea) {
    this.selectedIdea = idea;
  }

  unselectIdea () {
    this.selectedIdea = undefined;
  }

  goBack () {
    this.onSessionUnselect.emit(true);
  }

  editSession() {
    this.dialog
      .open(BrainstormingSessionCreateDialogComponent, {
        data: {
          session: this.session
        },
        maxHeight: '100vh',
        maxWidth: '100vw'
      })
      .afterClosed()
      .pipe(take(1))
      .subscribe(
        (updatedSession) => this.onSessionUpdate.emit(updatedSession)
      )
  }
}
