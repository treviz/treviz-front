import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {Component, Inject, OnDestroy} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {BrainstormingSessionDto} from '../../models/brainstorming/brainstorming-session.model.dto';
import {BrainstormingSessionService} from '../../../core/services/brainstorming/brainstorming-session.service';

@Component({
  moduleId: module.id,
  selector: 'app-brainstorming-session-create-dialog',
  templateUrl: 'brainstorming-session-create-dialog.component.html',
  styleUrls: ['brainstorming-session-create-dialog.component.scss']
})

export class BrainstormingSessionCreateDialogComponent implements OnDestroy {
  sessionDto: BrainstormingSessionDto;
  isNew = true;

  submitted = false;

  private destroyed$ = new Subject<void>();

  constructor (public dialogRef: MatDialogRef<BrainstormingSessionCreateDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data,
              private brainstormingSessionService: BrainstormingSessionService) {
    if (data.session) {
      this.sessionDto = BrainstormingSessionDto.fromSession(data.session);
      this.isNew = false;
    } else {
      this.sessionDto = new BrainstormingSessionDto();
      this.sessionDto.isOpen = true;

      data.project ? this.sessionDto.project = data.project.hash : this.sessionDto.project = null;
      data.community ? this.sessionDto.community = data.community.hash : this.sessionDto.project = null;
    }
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  toggleSession() {
    this.sessionDto.isOpen = !this.sessionDto.isOpen;
    this.onSubmit();
  }

  onSubmit () {
    this.submitted = true;

    const subscription = this.isNew
      ? this.brainstormingSessionService.postSession(this.sessionDto)
      : this.brainstormingSessionService.putSession(this.data.session?.hash, this.sessionDto);

    subscription
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        session => this.dialogRef.close(session),
        () => this.dialogRef.close(false),
        () => this.submitted = false
      );
  }
}
