import {Component, Input} from '@angular/core';
import {Community} from '../models/community/community.model';
import {Project} from '../models/project/project.model';
import {BrainstormingSession} from '../models/brainstorming/brainstorming-session.model';
import {BrainstormingIdea} from '../models/brainstorming/brainstorming-idea.model';
import {CommunityMembership} from '../models/community/community-membership.model';

@Component({
  moduleId: module.id,
  selector: 'app-brainstorming',
  templateUrl: 'brainstorming.component.html',
  styleUrls: ['brainstorming.component.css']
})

export class BrainstormingComponent {
  @Input()
  community: Community;

  @Input()
  project: Project;

  @Input()
  userMembership: CommunityMembership;

  private selectedSession: BrainstormingSession;
  private selectedIdea: BrainstormingIdea;

  constructor () {
    this.project = null;
    this.community = null;
    this.userMembership = null;
  }

  selectSession (session: BrainstormingSession): void {
    this.selectedSession = session;
  }

  unSelectSession (): void {
    this.selectedSession = undefined;
  }

  updateSession (session: BrainstormingSession): void {
    if (this.selectedSession?.hash === session.hash) {
      this.selectedSession = session;
    }
  }
}
