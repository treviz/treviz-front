
import { finalize, take, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Component, EventEmitter, Input, Output, OnChanges } from '@angular/core';
import { BrainstormingSession } from '../../models/brainstorming/brainstorming-session.model';
import { BrainstormingIdeaService } from '../../../core/services/brainstorming/brainstorming-idea.service';
import { BrainstormingIdeaDto } from '../../models/brainstorming/brainstorming-idea.model.dto';
import { BrainstormingIdea } from '../../models/brainstorming/brainstorming-idea.model';

@Component({
  moduleId: module.id,
  selector: 'app-brainstorming-idea-list',
  templateUrl: 'brainstorming-idea-list.component.html',
  styleUrls: ['brainstorming-idea-list.component.scss']
})
export class BrainstormingIdeaListComponent implements OnChanges {
  @Input() session: BrainstormingSession;
  ideas: BrainstormingIdea[] = [];
  ideaDto: BrainstormingIdeaDto;
  ideasFetched = false;
  ideaSubmitted = false;
  filterByAscendingScore = false;
  filterByAscendingLike = false;
  @Output() onIdeaSelect = new EventEmitter<BrainstormingIdea>();

  private destroyed$ = new Subject<void>();

  constructor (private brainstormingIdeaService: BrainstormingIdeaService) {
    this.ideaDto = new BrainstormingIdeaDto();
  }

  ngOnChanges () {
    this.loadIdeas();
  }

  onSubmit () {
    this.ideaSubmitted = true;
    this.brainstormingIdeaService
      .postIdea(this.session.hash, this.ideaDto)
      .pipe(
        finalize(() => {
          this.ideaSubmitted = false;
          this.ideaDto.content = '';
        }),
        take(1),
        takeUntil(this.destroyed$)
      ).subscribe(
        data => this.ideas.push(data),
        err => console.log(err)
      );
  }

  loadIdeas () {
    this.brainstormingIdeaService.getIdeas(this.session.hash).pipe(
      takeUntil(this.destroyed$))
      .subscribe(
        data => this.ideas = data,
        err => console.log(err),
        () => this.ideasFetched = true
      );
  }

  selectIdea (idea: BrainstormingIdea) {
    this.onIdeaSelect.emit(idea);
  }

  updateIdea (updatedIdea: BrainstormingIdea) {
    const index = this.ideas.findIndex((idea: BrainstormingIdea) => idea.hash === updatedIdea.hash);
    this.ideas[index] = updatedIdea;
  }

  filterByScore () {
    if (this.filterByAscendingScore) {
      this.ideas = this.ideas.sort((a, b) => a.score - b.score);
    } else {
      this.ideas = this.ideas.sort((a, b) => b.score - a.score);
    }
    this.filterByAscendingScore = !this.filterByAscendingScore;
  }

  filterByLike () {
    if (this.filterByAscendingLike) {
      this.ideas = this.ideas.sort((a, b) => a.liked.length - b.liked.length);
    } else {
      this.ideas = this.ideas.sort((a, b) => b.liked.length - a.liked.length);
    }
    this.filterByAscendingLike = !this.filterByAscendingLike;
  }
}
