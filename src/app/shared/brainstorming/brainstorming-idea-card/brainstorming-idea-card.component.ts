
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { BrainstormingIdea } from '../../models/brainstorming/brainstorming-idea.model';
import { BrainstormingIdeaService } from '../../../core/services/brainstorming/brainstorming-idea.service';
import { UserStateService } from '../../../core/services/user-state.service';

@Component({
  moduleId: module.id,
  selector: 'app-brainstorming-idea-card',
  templateUrl: 'brainstorming-idea-card.component.html',
  styleUrls: ['brainstorming-idea-card.component.scss']
})

export class BrainstormingIdeaCardComponent implements OnDestroy {
  @Input() idea: BrainstormingIdea;
  @Output() onIdeaSelect = new EventEmitter<BrainstormingIdea>();
  @Output() onIdeaUpdate = new EventEmitter<BrainstormingIdea>();

  public loading = false;
  private destroyed$ = new Subject<void>();

  constructor (private brainstormingIdeaService: BrainstormingIdeaService,
              private currentUserService: UserStateService) {
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  triggerLike () {
    this.loading = true;
    if (this.currentUserLike()) {
      this.brainstormingIdeaService.unlikeIdea(this.idea.hash).pipe(
        takeUntil(this.destroyed$))
        .subscribe(
          data => this.idea = data,
          err => console.log(err),
          () => {
            this.onIdeaUpdate.emit(this.idea);
            this.loading = true;
          }
        );
    } else {
      this.brainstormingIdeaService.likeIdea(this.idea.hash).pipe(
        takeUntil(this.destroyed$))
        .subscribe(
          data => this.idea = data,
          err => console.log(err),
          () => {
            this.onIdeaUpdate.emit(this.idea);
            this.loading = true;
          }
        );
    }
  }

  currentUserLike () {
    return this.idea.liked.some(user => user.username === localStorage.getItem('username'));
  }

  selectIdea () {
    this.onIdeaSelect.emit(this.idea);
  }

  upvote () {
    let score = this.currentVote();
    score++;
    this.vote(score);
  }

  downvote () {
    let score = this.currentVote();
    score--;
    this.vote(score);
  }

  private vote (score: number) {
    this.brainstormingIdeaService.voteForIdea(this.idea.hash, score).pipe(
      takeUntil(this.destroyed$))
      .subscribe(
        data => this.idea = data,
        err => console.log(err),
        () => {
          this.onIdeaUpdate.emit(this.idea);
          this.loading = true;
        }
      );
  }

  currentVote (): number {
    const votes = this.idea.votes;
    const currentUser = this.currentUserService.getCurrentUser();
    const currentUserVote = votes.find((vote) => vote.user.username === currentUser.username);

    if (currentUserVote != null) {
      return currentUserVote.score;
    }
    return 0;
  }
}
