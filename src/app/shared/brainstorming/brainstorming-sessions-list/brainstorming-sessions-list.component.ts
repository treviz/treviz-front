import {finalize, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges} from '@angular/core';
import {Community} from '../../models/community/community.model';
import {Project} from '../../models/project/project.model';
import {BrainstormingSession} from '../../models/brainstorming/brainstorming-session.model';
import {BrainstormingSessionService} from '../../../core/services/brainstorming/brainstorming-session.service';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {BrainstormingSessionCreateDialogComponent} from '../brainstorming-session-create-dialog/brainstorming-session-create-dialog.component';
import {CommunityMembership} from '../../models/community/community-membership.model';
import {MessageDialogComponent} from '../../dialogs/message-dialog/message-dialog.component';
import CommunityPermissions from '../../models/community/community-permissions.model';

@Component({
  selector: 'app-brainstorming-sessions-list',
  templateUrl: 'brainstorming-sessions-list.component.html',
  styleUrls: ['brainstorming-sessions-list.component.scss']
})
export class BrainstormingSessionsListComponent implements OnChanges, OnDestroy {
  @Input()
  private community: Community;

  @Input()
  private project: Project;

  @Input()
  private userMembership: CommunityMembership;

  @Output()
  onSessionSelection = new EventEmitter<BrainstormingSession>();

  public sessions: BrainstormingSession[];

  public sessionsFetched = false;

  public destroyed$ = new Subject<void>();

  constructor (private brainstormingSessionService: BrainstormingSessionService,
              public dialog: MatDialog) { }

  ngOnChanges (changes: SimpleChanges) {
    const options = {
      community: '',
      project: ''
    };

    for (const propName in changes) {
      if (propName === 'community' && this.community !== null) {
        options.community = this.community.hash;
      } else if (propName === 'project' && this.project !== null) {
        options.project = this.project.hash;
      }
    }

    this.brainstormingSessionService.getSessions(options)
      .pipe(
        finalize(() => this.sessionsFetched = true),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        sessions => this.sessions = sessions,
        () => this.displayErrorDialog('An error occurred while fetching the brainstorming sessions.')
      );
  }

  ngOnDestroy (): void {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  canCreateSession (): boolean {
    return this.userMembership != null &&
        this.userMembership.role != null &&
        this.userMembership.role.permissions.includes(CommunityPermissions.MANAGE_BRAINSTORMING_SESSION);
  }

  openSessionCreation () {
    const config: MatDialogConfig = {
      data: {
        community: this.community,
        project: this.project
      },
      maxWidth: '100vw',
      maxHeight: '100vh'
    };

    this.dialog.open(BrainstormingSessionCreateDialogComponent, config)
      .afterClosed()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(result => {
        if (result != null) {
          if (result) {
            this.sessions.push(result);
          } else {
            this.displayErrorDialog('An error occurred while creating the brainstorming session.');
          }
        }
      });
  }

  selectSession (session: BrainstormingSession): void {
    this.onSessionSelection.emit(session);
  }

  displayErrorDialog (message: string) {
    const config = new MatDialogConfig();
    config.data = {
      message,
      failure: true,
      actionRequired: true
    };

    this.dialog.open(MessageDialogComponent, config);
  }
}
