import { Component, Input } from '@angular/core';
import { BrainstormingSession } from '../../models/brainstorming/brainstorming-session.model';

@Component({
  selector: 'app-brainstorming-session-card',
  templateUrl: 'brainstorming-session-card.component.html',
  styleUrls: ['brainstorming-session-card.component.scss']
})

export class BrainstormingSessionCardComponent {
  @Input() session: BrainstormingSession;
}
