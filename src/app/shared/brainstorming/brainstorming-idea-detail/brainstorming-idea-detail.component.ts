import { finalize, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Component, EventEmitter, Input, OnChanges, OnDestroy, Output } from '@angular/core';
import { BrainstormingIdea } from '../../models/brainstorming/brainstorming-idea.model';
import { BrainstormingEnhancementService } from '../../../core/services/brainstorming/brainstorming-enhancement.service';
import { MatDialog } from '@angular/material/dialog';
import { DocumentUploadComponent } from '../../documents/document-upload/document-upload.component';

@Component({
  moduleId: module.id,
  selector: 'app-brainstorming-idea-detail',
  templateUrl: 'brainstorming-idea-detail.component.html',
  styleUrls: ['brainstorming-idea-detail.component.scss']
})
export class BrainstormingIdeaDetailComponent implements OnChanges, OnDestroy {
  @Input() idea: BrainstormingIdea;
  @Output() onIdeaUnselect = new EventEmitter<boolean>();

  public enhancementsFetched = false;

  private destroyed$ = new Subject<void>();

  constructor (private brainstormingEnhancementsService: BrainstormingEnhancementService,
              public dialog: MatDialog) {
  }

  ngOnChanges () {
    this.enhancementsFetched = false;
    this
      .brainstormingEnhancementsService
      .getIdeaEnhancements(this.idea.hash)
      .pipe(
        finalize(() => this.enhancementsFetched = true),
        takeUntil(this.destroyed$)
      );
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  goBackToSession () {
    this.onIdeaUnselect.emit(true);
  }

  openDocumentUploadDialog () {
    const dialogRef = this.dialog.open(
      DocumentUploadComponent,
      {
        maxWidth: '100vw',
        maxHeight: '100vh'
      }
    );
    dialogRef.componentInstance.documentDto.idea = this.idea.hash;
    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        data => {
          if (data) {
            this.idea.documents.push(data);
          }
        },
        err => console.log(err)
      );
  }
}
