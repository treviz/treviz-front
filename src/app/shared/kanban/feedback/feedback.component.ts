import { Component, Input } from '@angular/core';
import { Feedback } from '../../models/kanban/feedback.model';

@Component({
  selector: 'app-feedback',
  templateUrl: 'feedback.component.html',
  styleUrls: ['feedback.component.scss']
})
export class FeedbackComponent {
  @Input() feedback: Feedback;
}
