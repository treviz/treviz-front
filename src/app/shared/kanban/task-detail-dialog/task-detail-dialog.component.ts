import {debounceTime, distinctUntilChanged, finalize, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {Component, Inject, OnDestroy} from '@angular/core';
import {Task} from '../../models/kanban/task.model';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {TaskService} from '../../../core/services/kanban/task.service';
import {TaskDto} from '../../models/kanban/task.model.dto';
import {UserService} from '../../../core/services/user.service';
import {User} from '../../models/users/user.model';
import {FormControl} from '@angular/forms';
import {Label} from '../../models/kanban/label.model';
import {FeedbackDto} from '../../models/kanban/feedback.model.dto';
import {FeedbackService} from '../../../core/services/kanban/feedback.service';
import {InternalEventType} from '../../models/internal-event.model';

@Component({
  selector: 'app-task-detail-dialog',
  templateUrl: 'task-detail-dialog.component.html',
  styleUrls: ['task-detail-dialog.component.scss']
})
export class TaskDetailDialogComponent implements OnDestroy {
  task: Task;
  canEdit = false;
  canDelete = false;
  canSubmit = false;
  taskDto: TaskDto;
  taskSubmitted = false;
  private destroyed$ = new Subject<void>();

  assignee: User;
  filteredAssignee: User[];
  assigneeCtrl: FormControl;

  supervisor: User;
  filteredSupervisor: User[];
  supervisorCtrl: FormControl;

  labels: Label[] = [];

  feedback = new FeedbackDto();
  feedbackSubmitted = false;

  edit = {
    name: false,
    description: false,
    deadline: false,
    supervisor: false,
    assignee: false,
    labels: false
  };

  constructor (private taskService: TaskService,
              private userService: UserService,
              private feedbackService: FeedbackService,
              public dialogRef: MatDialogRef<TaskDetailDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data) {
    this.task = data.task;
    data.labels != null ? this.labels = data.labels : this.labels = [];
    this.canDelete = this.task.supervisor.username === localStorage.getItem('username');
    this.canEdit = (this.canDelete || this.task.assignee?.username === localStorage.getItem('username'));
    this.taskDto = new TaskDto();
    this.taskDto.name = data.task.name;
    this.taskDto.description = data.task.description || '';
    this.taskDto.deadline = data.task.deadline;
    this.taskDto.supervisor = data.task.supervisor.username;
    this.supervisor = data.task.supervisor;
    this.taskDto.position = data.task.position;
    this.taskDto.pendingApproval = data.task.pendingApproval;
    this.taskDto.labels = data.task.labels.map(label => label.hash);
    if (data.task.assignee) {
      this.taskDto.assignee = data.task.assignee.username;
      this.assignee = data.task.assignee;
      this.canSubmit = data.task.assignee.username === localStorage.getItem('username') && !this.task.pendingApproval;
    }
    if (data.task.job) {
      this.taskDto.job = data.task.job.hash;
    }

    this.assigneeCtrl = new FormControl();
    this.assigneeCtrl.valueChanges.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      takeUntil(this.destroyed$))
      .subscribe(
        name => {
          this.filteredAssignee = [];
          if (name != null && name.length >= 2) {
            this.userService.getUsers({ name: name }).pipe(
              takeUntil(this.destroyed$))
              .subscribe(
                users => this.filteredAssignee = users,
                err => console.log(err)
              );
          }
        }
      );

    this.supervisorCtrl = new FormControl();
    this.supervisorCtrl.valueChanges.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      takeUntil(this.destroyed$))
      .subscribe(
        name => {
          this.filteredSupervisor = [];
          if (name != null && name.length >= 2) {
            this.userService.getUsers({ name: name }).pipe(
              takeUntil(this.destroyed$))
              .subscribe(
                users => this.filteredSupervisor = users,
                err => console.log(err)
              );
          }
        }
      );
  }

  ngOnDestroy (): void {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  setAssignee (username: string): void {
    const assignee = this.filteredAssignee.find(user => user.username === username);
    if (assignee) {
      this.assignee = assignee;
      this.taskDto.assignee = username;
    }
  }

  removeAssignee (): void {
    this.assignee = null;
    this.taskDto.assignee = undefined;
  }

  setSupervisor (username: string): void {
    const supervisor = this.filteredSupervisor.find(user => user.username === username);
    if (supervisor) {
      this.supervisor = supervisor;
      this.taskDto.supervisor = username;
    }
  }

  removeSupervisor (): void {
    this.supervisor = null;
    this.taskDto.supervisor = undefined;
  }

  editTask (property: string): void {
    if (!this.canEdit) { return; }

    switch (property) {
      case 'name':
        this.edit.name = true;
        break;
      case 'description':
        this.edit.description = true;
        break;
      case 'deadline':
        this.edit.deadline = true;
        break;
      case 'supervisor':
        this.edit.supervisor = true;
        break;
      case 'assignee':
        this.edit.assignee = true;
        break;
      case 'labels':
        this.edit.labels = true;
        break;
    }
  }

  deleteTask (): void {
    this.taskSubmitted = true;
    if (!this.canDelete) { return; }
    this.taskService.deleteTask(this.task.hash)
      .subscribe(
        () => this.dialogRef.close({ type: InternalEventType.DELETE, data: this.task }),
        () => this.dialogRef.close({ type: InternalEventType.DELETE }),
        () => this.taskSubmitted = false
      );
  }

  archiveTask (): void {
    this.taskSubmitted = true;
    this.taskDto.pendingApproval = false;
    this.taskDto.archived = true;
    this.taskService.putTask(this.task.hash, this.taskDto).pipe(
      takeUntil(this.destroyed$))
      .subscribe(
        task => this.dialogRef.close({ type: InternalEventType.DELETE, data: task }),
        () => this.dialogRef.close({ type: InternalEventType.DELETE }),
        () => this.taskSubmitted = false
      );
  }

  triggerLabelSelect (label: Label): void {
    if (this.isSelectedLabel(label)) {
      this.taskDto.labels.splice(this.taskDto.labels.indexOf(label.hash), 1);
    } else {
      this.taskDto.labels.push(label.hash);
    }
  }

  isSelectedLabel (label: Label): boolean {
    return this.taskDto.labels.some(labelHash => labelHash === label.hash);
  }

  pendingUpdate (): boolean {
    return this.canEdit &&
      (this.edit.name ||
      this.edit.description ||
      this.edit.deadline ||
      this.edit.supervisor ||
      this.edit.assignee ||
      this.edit.labels);
  }

  submitForApproval () {
    this.taskSubmitted = true;
    this.taskService.submitTaskForApproval(this.task.hash).pipe(
      finalize(() => this.taskSubmitted = false),
      takeUntil(this.destroyed$))
      .subscribe(
        task => this.dialogRef.close({ type: InternalEventType.UPDATE, data: task }),
        () => this.dialogRef.close({ type: InternalEventType.UPDATE })
      );
  }

  dismissSubmission () {
    this.feedback.praise = false;
    this.feedbackSubmitted = true;
    this.feedbackService.postFeedback(this.task.hash, this.feedback)
      .pipe(
        finalize(() => this.feedbackSubmitted),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        () => {
          this.taskDto.pendingApproval = false;
          this.taskService.putTask(this.task.hash, this.taskDto).pipe(
            takeUntil(this.destroyed$))
            .subscribe(
              task => this.dialogRef.close({ type: InternalEventType.UPDATE, data: task }),
              () => this.dialogRef.close({ type: InternalEventType.UPDATE })
            );
        },
        () => this.dialogRef.close({ type: InternalEventType.UPDATE })
      );
  }

  approveTask (praise: boolean) {
    this.feedback.praise = praise;
    this.feedbackSubmitted = true;
    this.feedbackService.postFeedback(this.task.hash, this.feedback).pipe(
      finalize(() => this.feedbackSubmitted),
      takeUntil(this.destroyed$))
      .subscribe(
        () => {
          this.taskDto.pendingApproval = false;
          this.taskDto.archived = true;
          this.taskService.putTask(this.task.hash, this.taskDto).pipe(
            takeUntil(this.destroyed$))
            .subscribe(
              task => this.dialogRef.close({ type: InternalEventType.DELETE, data: task }),
              () => this.dialogRef.close({ type: InternalEventType.DELETE })
            );
        },
        () => this.dialogRef.close({ type: InternalEventType.UPDATE })
      );
  }

  onSubmit (): void {
    if (this.taskDto.labels === []) { this.taskDto.labels = undefined; }
    this.taskSubmitted = true;
    this.taskService.putTask(this.task.hash, this.taskDto).pipe(
      finalize(() => this.taskSubmitted = false),
      takeUntil(this.destroyed$))
      .subscribe(
        task => this.dialogRef.close({ type: InternalEventType.UPDATE, data: task }),
        () => this.dialogRef.close({ type: InternalEventType.UPDATE })
      );
  }
}
