
import { takeUntil, finalize } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { TaskService } from '../../../core/services/kanban/task.service';
import { User } from '../../models/users/user.model';
import { Task } from '../../models/kanban/task.model';
import { FeedbackService } from '../../../core/services/kanban/feedback.service';
import { Feedback } from '../../models/kanban/feedback.model';

@Component({
  selector: 'app-feedback-list',
  templateUrl: 'feedback-list.component.html',
  styleUrls: ['feedback-list.component.scss']
})
export class FeedbackListComponent implements OnInit, OnDestroy {
  @Input() user: User;

  @Input() task: Task;

  feedbacks: Feedback[] = [];
  feedbackLoaded = false;

  private destroyed$ = new Subject<void>();

  constructor (private tasksService: TaskService,
              private feedbackService: FeedbackService) { }

  ngOnInit () {
    if (this.user) {
      this.feedbackService.getFeedback({})
        .pipe(
          finalize(() => this.feedbackLoaded = true),
          takeUntil(this.destroyed$)
        )
        .subscribe(
          data => this.feedbacks = data,
          err => console.log(err)
        );
    } else if (this.task) {
      this.feedbackService.getFeedback({ task: this.task.hash })
        .pipe(
          finalize(() => this.feedbackLoaded = true),
          takeUntil(this.destroyed$)
        )
        .subscribe(
          data => this.feedbacks = data,
          err => console.log(err)
        );
    }
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }
}
