
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Component, Inject, OnDestroy } from '@angular/core';
import { BoardService } from '../../../core/services/kanban/board.service';
import { BoardDto } from '../../models/kanban/board.model.dto';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-board-create-dialog',
  templateUrl: 'board-create-dialog.component.html',
  styleUrls: ['board-create-dialog.component.scss']
})
export class BoardCreateDialogComponent implements OnDestroy {
  boardDto: BoardDto;
  boardSubmitted = false;

  private destroyed$ = new Subject<void>();

  constructor (public dialogRef: MatDialogRef<BoardCreateDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: string,
              private boardService: BoardService) {
    this.boardDto = new BoardDto();
    this.boardDto.project = data;
    this.boardDto.archived = false;
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  onSubmit () {
    this.boardSubmitted = true;
    this.boardService.postBoard(this.boardDto).pipe(
      takeUntil(this.destroyed$))
      .subscribe(
        board => this.dialogRef.close(board),
        () => this.dialogRef.close(false),
        () => this.boardSubmitted = false
      );
  }
}
