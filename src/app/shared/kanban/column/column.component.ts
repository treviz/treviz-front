import {finalize, takeUntil} from 'rxjs/operators';
import {Component, EventEmitter, Input, OnChanges, OnDestroy, Output} from '@angular/core';
import {TaskDto} from '../../models/kanban/task.model.dto';
import {TaskService} from '../../../core/services/kanban/task.service';
import {Column} from '../../models/kanban/column.model';
import {DragulaService} from 'ng2-dragula';
import {Task} from '../../models/kanban/task.model';
import {Label} from '../../models/kanban/label.model';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {MessageDialogComponent} from 'app/shared/dialogs/message-dialog/message-dialog.component';
import {ColumnService} from 'app/core/services/kanban/column.service';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-column',
  templateUrl: 'column.component.html',
  styleUrls: ['column.component.scss']
})
export class ColumnComponent implements OnChanges, OnDestroy {
  @Input() column: Column;
  @Input() labels: Label[];
  @Input() canEdit: boolean;
  @Output() onColumnDelete = new EventEmitter<Column>();
  private initialTaskArray = [];

  taskDto: TaskDto;
  displayNewTaskInput = false;
  taskSubmitted = false;
  pending = false;
  private destroyed$ = new Subject<void>();

  constructor (private taskService: TaskService,
              public dialog: MatDialog,
              private dragulaService: DragulaService,
              private columnService: ColumnService) {
    // Display the tasks from lowest to highest position.
    this.taskDto = new TaskDto();
    this.taskDto.supervisor = localStorage.getItem('username');
    this.taskDto.labels = undefined;
    this.dragulaService.drop.pipe(
      takeUntil(this.destroyed$))
      .subscribe(() => this.onDrop());
  }

  ngOnChanges () {
    if (this.column) {
      this.column.tasks = this.column.tasks
        .sort((taskA, taskB) => taskA.position - taskB.position)
        .filter((task: Task) => !task.archived);
      this.initialTaskArray = this.column.tasks.map(task => task.hash);
    }
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  onSubmit () {
    // We give to our new task the position of the last one of the column, plus 1000. So we can fit other tasks between.
    if (this.column.tasks.length > 0) {
      this.taskDto.position = this.column.tasks[this.column.tasks.length - 1].position + 1000;
    } else {
      this.taskDto.position = 1000;
    }
    this.taskSubmitted = true;
    this.taskService.postTask(this.column.hash, this.taskDto)
      .pipe(
        finalize(() => {
          this.taskDto = new TaskDto();
          this.taskDto.supervisor = localStorage.getItem('username');
          this.taskDto.labels = undefined;
          this.taskSubmitted = false;
        }),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        task => this.column.tasks.push(task),
        () => this.displayErrorDialog('An error occurred while creating the task.')
      );
  }

  triggerTaskInput () {
    this.displayNewTaskInput = !this.displayNewTaskInput;
  }

  /**
   * Updates a task's position when it is moved inside the same board.
   */
  private onDrop () {
    let taskToUpdate;

    /*
     * Looks for a new task inside the column.
     * If their is none, look for a task that was moved inside this column.
     */
    taskToUpdate = this.column.tasks.find(task => this.initialTaskArray.indexOf(task.hash) === -1);
    if (!taskToUpdate) {
      taskToUpdate = this.findMovedTask();
    }

    if (taskToUpdate) {
      let newPosition;
      const index = this.column.tasks.findIndex(task => task.hash === taskToUpdate.hash);
      /*
       * If the task is the only one of the column, give it 0 as position.
       * If their are two tasks in the column, set its position to be under/above the next/previous task.
       */
      if (this.column.tasks.length === 1) {
        newPosition = 0;
      } else {
        if (index === 0) {
          newPosition = this.column.tasks[1].position - 1000;
        } else if (index === this.column.tasks.length - 1) {
          newPosition = this.column.tasks[this.column.tasks.length - 2].position + 1000;
        } else {
          newPosition = Math.floor((this.column.tasks[index - 1].position + this.column.tasks[index + 1].position) / 2);
        }
      }

      const updatedTask = new TaskDto();
      updatedTask.name = taskToUpdate.name;
      updatedTask.description = taskToUpdate.description;
      updatedTask.deadline = taskToUpdate.deadline;
      updatedTask.position = newPosition;
      updatedTask.column = this.column.hash;
      if (taskToUpdate.labels) {
        updatedTask.labels = taskToUpdate.labels.map(label => label.hash);
      } else {
        updatedTask.labels = undefined;
      }
      if (taskToUpdate.supervisor) {
        updatedTask.supervisor = taskToUpdate.supervisor.username;
      }
      if (taskToUpdate.assignee) {
        updatedTask.assignee = taskToUpdate.assignee.username;
      }
      if (taskToUpdate.job) {
        updatedTask.job = taskToUpdate.job.hash;
      }

      this.taskService.putTask(taskToUpdate.hash, updatedTask).pipe(
        takeUntil(this.destroyed$))
        .subscribe(
          task => this.column.tasks[index] = task,
          () => this.displayErrorDialog('An error occurred while updating the task.')
        );
    }

    this.initialTaskArray = this.column.tasks.map(task => task.hash);
  }

  /**
   * Returns the task that has been dragged inside the same column.
   */
  findMovedTask (): Task {
    const length = this.column.tasks.length;

    /*
     * If the table only contains one element, then return nothing.
     */
    if (length > 1) {
      /*
       * Checks if the first two tasks were not switched
       */
      if (this.column.tasks[0].position > this.column.tasks[1].position) {
        return this.column.tasks[0];
      }

      /*
       * Checks if the last two tasks were not switched
       */
      if (this.column.tasks[length - 2].position > this.column.tasks[length - 1].position) {
        return this.column.tasks[length - 2];
      }

      /*
       * If, in the middle of the table, one task's position is either inferior to the next's, or superior to the
       * previous's, while its predecessors and followers are correctly positions, this task is the one that has
       * been moved.
       */
      for (let i = 1; i < length - 2; i++) {
        const task = this.column.tasks[i];
        const previousTask = this.column.tasks[i - 1];
        const nextTask = this.column.tasks[i + 1];
        if (previousTask.position < nextTask.position &&
          (task.position < previousTask.position || task.position > nextTask.position)) {
          return task;
        }
      }
    }
  }

  /**
   * Updates a task, or remove it from the column if it was archived
   * @param task Task to update in the colum
   */
  updateTask (task: Task): void {
    if (task.archived) {
      this.removeTask(task);
    } else {
      const taskindex = this.column.tasks.findIndex(indexedTask => indexedTask.hash === task.hash);
      this.column.tasks[taskindex] = task;
    }
  }

  /**
   * Removes a task from the column.
   * @param task Task to remove from the column
   */
  removeTask (task: Task): void {
    const index = this.column.tasks.findIndex(indexedTask => indexedTask.hash === task.hash);
    if (index > -1) {
      this.column.tasks.splice(index, 1);
    }
  }

  deleteColumn (): void {
    this.pending = true;
    this.columnService.deleteColumn(this.column.hash).pipe(
      takeUntil(this.destroyed$))
      .subscribe(
        () => this.onColumnDelete.emit(this.column),
        () => {
          this.displayErrorDialog('An error occurred while deleting the column');
          this.pending = false;
        }
      );
  }

  displayErrorDialog (message: string) {
    const config = new MatDialogConfig();
    config.data = {
      message,
      failure: true,
      actionRequired: true
    };

    this.dialog.open(MessageDialogComponent, config);
  }
}
