import {finalize, take, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {Board} from '../../models/kanban/board.model';
import {ColumnDto} from '../../models/kanban/column.model.dto';
import {ColumnService} from '../../../core/services/kanban/column.service';
import {Column} from '../../models/kanban/column.model';
import {ProjectMembership} from '../../models/project/project-membership.model';
import {MessageDialogComponent} from 'app/shared/dialogs/message-dialog/message-dialog.component';

@Component({
  selector: 'app-board',
  templateUrl: 'board.component.html',
  styleUrls: ['board.component.scss']
})
export class BoardComponent implements OnChanges, OnDestroy {
  @Input() board: Board;
  @Input() membership: ProjectMembership; // used to decide if the user should see the edit buttons.

  canEdit = false;
  columnsFetched = false;
  private destroyed$ = new Subject<void>();
  columns: Column[] = [];
  columnDto: ColumnDto;
  columnSubmitted = false;

  constructor (private columnService: ColumnService,
              public dialog: MatDialog) {
    this.columnDto = new ColumnDto();
  }

  ngOnChanges (changes: SimpleChanges) {
    this.columns = [];
    this.columnsFetched = false;
    this
      .columnService
      .getColumns(this.board.hash)
      .pipe(
        take(1),
        finalize(() => this.columnsFetched = true),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        columns => this.columns = columns,
        err => console.log(err)
      );

    if (this.membership) {
      this.canEdit = this.membership?.role?.permissions?.some(permission => permission === 'MANAGE_KANBAN');
    }
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  onSubmit () {
    this.columnSubmitted = true;
    this.columnService.postColumn(this.board.hash, this.columnDto)
      .pipe(
        takeUntil(this.destroyed$),
        finalize(() => { this.columnDto = new ColumnDto(); this.columnSubmitted = false; })
      )
      .subscribe(
        column => this.columns.push(column),
        () => this.displayErrorDialog('An error occurred while creating the column')
      );
  }

  removeColumn (column: Column) {
    this.columns.splice(this.columns.indexOf(column), 1);
  }

  displayErrorDialog (message: string) {
    const config = new MatDialogConfig();
    config.data = {
      message,
      failure: true,
      actionRequired: true
    };

    this.dialog.open(MessageDialogComponent, config);
  }
}
