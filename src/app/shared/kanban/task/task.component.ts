
import { take, takeUntil } from 'rxjs/operators';
import { Component, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Task } from '../../models/kanban/task.model';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { TaskDetailDialogComponent } from '../task-detail-dialog/task-detail-dialog.component';
import { Label } from '../../models/kanban/label.model';
import { MessageDialogComponent } from 'app/shared/dialogs/message-dialog/message-dialog.component';
import { InternalEvent, InternalEventType } from '../../models/internal-event.model';
import { TaskService } from '../../../core/services/kanban/task.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-task',
  templateUrl: 'task.component.html',
  styleUrls: ['task.component.scss']
})
export class TaskComponent implements OnDestroy {
  @Input() task: Task;

  @Input() availableLabels: Label[];

  @Output() onTaskUpdate = new EventEmitter<Task>();

  @Output() onTaskDelete = new EventEmitter<Task>();

  private destroyed$ = new Subject<void>();

  constructor (public dialog: MatDialog,
              private taskService: TaskService) { }

  ngOnDestroy (): void {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  openEditDialog () {
    if (this.task.assignee != null) {
      this.openDialog(this.task);
    } else {
      this.taskService
        .getTask(this.task.hash)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(
          task => this.openDialog(task)
        );
    }
  }

  openDialog (task: Task) {
    const config = new MatDialogConfig();
    config.maxWidth = '100vw';
    config.maxHeight = '100vh';

    try {
      config.data = {
        task: task,
        labels: this.task.column.board.labels
      };
    } catch (e) {
      config.data = {
        task: task,
        labels: this.availableLabels
      };
    }

    const dialogRef = this.dialog.open(TaskDetailDialogComponent, config);

    dialogRef
      .afterClosed()
      .pipe(
        take(1)
      )
      .subscribe(
        (event: InternalEvent) => {
          if (event != null) {
            if (event.data == null) {
              switch (event.type) {
                case InternalEventType.DELETE:
                  this.displayErrorDialog('An error occurred while removing this task');
                  break;
                case InternalEventType.UPDATE:
                default:
                  this.displayErrorDialog('An error occurred while updating this task');
                  break;
              }
            } else {
              switch (event.type) {
                case InternalEventType.DELETE:
                  this.onTaskDelete.emit(event.data);
                  break;
                case InternalEventType.UPDATE:
                default:
                  this.task = event.data;
                  this.onTaskUpdate.emit(event.data);
                  break;
              }
            }
          }
        }
      );
  }

  displayErrorDialog (message: string) {
    const config = new MatDialogConfig();
    config.data = {
      message,
      failure: true,
      actionRequired: true
    };

    this.dialog.open(MessageDialogComponent, config);
  }
}
