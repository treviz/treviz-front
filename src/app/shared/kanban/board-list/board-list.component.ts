
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Component, Input, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';
import { BoardService } from '../../../core/services/kanban/board.service';
import { Board } from '../../models/kanban/board.model';
import { User } from '../../models/users/user.model';
import { Project } from '../../models/project/project.model';
import { BoardDto } from '../../models/kanban/board.model.dto';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { BoardCreateDialogComponent } from '../board-create-dialog/board-create-dialog.component';
import { ProjectMembership } from '../../models/project/project-membership.model';
import { BoardEditDialogComponent } from '../board-edit-dialog/board-edit-dialog.component';
import { MessageDialogComponent } from 'app/shared/dialogs/message-dialog/message-dialog.component';

@Component({
  selector: 'app-board-list',
  templateUrl: 'board-list.component.html',
  styleUrls: ['board-list.component.scss']
})
export class BoardListComponent implements OnChanges, OnDestroy {
  @Input() project: Project;
  @Input() membership: ProjectMembership = null; // used to decide if the user should see the edit buttons.
  @Input() user: User;

  boards: Board[] = [];
  selectedBoard: Board;
  newBoard: BoardDto;
  canEdit = false;
  boardsFetched = false;
  private destroyed$ = new Subject<void>();

  constructor (private boardService: BoardService,
              public dialog: MatDialog) {
    this.newBoard = new BoardDto();
  }

  ngOnChanges (changes: SimpleChanges) {
    const options = {
      project: '',
      user: '',
      archived: false
    };

    for (const propName in changes) {
      if (propName === 'project') {
        options.project = this.project.hash;
      } else if (propName === 'user') {
        options.user = this.user.username;
      }
    }

    this.boardService.getBoards(options).pipe(
      takeUntil(this.destroyed$))
      .subscribe(
        boards => {
          this.boards = boards;
          if (boards.length > 0) {
            this.selectedBoard = this.boards[0];
          }
          this.boardsFetched = true;
        },
        err => console.log(err)
      );

    if (this.membership) {
      this.canEdit = this.membership.role.permissions.some(permission => permission === 'MANAGE_KANBAN');
    }
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  selectBoard (board: Board) {
    this.selectedBoard = board;
  }

  openBoardCreationDialog () {
    const config: MatDialogConfig = {
      data: this.project?.hash ?? '',
      maxWidth: '100vw',
      maxHeight: '100vh'
    };

    this.dialog.open(BoardCreateDialogComponent, config)
      .afterClosed().pipe(
        takeUntil(this.destroyed$))
      .subscribe(
        board => {
          if (board != null) {
            if (board) {
              this.boards.push(board);
              this.selectedBoard = board;
            } else {
              this.displayErrorDialog('An error occurred while creating the board');
            }
          }
        }
      );
  }

  openBoardUpdateDialog () {
    const config = new MatDialogConfig();
    config.data = this.selectedBoard;

    const dialogRef = this.dialog.open(BoardEditDialogComponent, config);
    dialogRef.afterClosed()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        updatedBoard => {
          if (updatedBoard != null) {
            if (updatedBoard) {
              const boardIndex = this.boards.findIndex(indexedBoard => indexedBoard.hash === this.selectedBoard.hash);
              this.selectedBoard = updatedBoard;
              this.boards[boardIndex] = updatedBoard;
            } else {
              const boardIndex = this.boards.findIndex(indexedBoard => indexedBoard.hash === this.selectedBoard.hash);
              this.boards.splice(boardIndex, 1);
              this.selectedBoard = this.boards[0];
            }
          }
        },
        () => this.displayErrorDialog('An error occurred while updating the board')
      );
  }

  displayErrorDialog (message: string) {
    const config = new MatDialogConfig();
    config.data = {
      message,
      failure: true,
      actionRequired: true
    };

    this.dialog.open(MessageDialogComponent, config);
  }
}
