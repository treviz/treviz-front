import {Component, Input} from '@angular/core';
import {Community} from '../models/community/community.model';
import {coerceBooleanProperty} from "@angular/cdk/coercion";

@Component({
  moduleId: module.id,
  selector: 'app-community-thumbnail',
  templateUrl: 'community-thumbnail.component.html',
  styleUrls: ['community-thumbnail.component.scss']
})
export class CommunityThumbnailComponent {
  @Input() community: Community;
  @Input() selected: boolean;
  @Input() get small(): boolean {
    return this._small;
  }
  set small(val: boolean) {
    this._small = coerceBooleanProperty(val);
  }

  _small: boolean
}
