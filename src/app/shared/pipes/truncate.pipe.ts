/**
 * Created by huber on 15/04/2017.
 */

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncate'
})
export class TruncatePipe implements PipeTransform {
  transform (value: string, lenght: number): string {
    const limit = lenght || 140;
    const trail = '...';

    if (value == null) { value = ''; }

    return value.length > limit ? value.substring(0, limit) + trail : value;
  }
}
