import { Pipe, PipeTransform } from '@angular/core';
import { Document } from '../models/document/document.model';

@Pipe({
  name: 'fileSize'
})
export class FileSizePipe implements PipeTransform {
  transform (file: File|Document): string {
    const size = file.size
    if (size < 1e3) {
      return size + 'octets';
    } else if (size < 1e6) {
      return Math.ceil(size / 1e2) / 10 + 'ko';
    } else if (size < 1e9) {
      return Math.ceil(size / 1e5) / 10 + 'Mo';
    }
  }
}
