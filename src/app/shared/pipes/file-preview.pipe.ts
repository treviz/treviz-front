import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filePreview'
})
export class FilePreviewPipe implements PipeTransform {
  private dir = '../../../assets/images/filetypes';
  private fileTypes = [
    { extension: 'ai', file: 'ai.svg' },
    { extension: 'avi', file: 'avi.svg' },
    { extension: 'css', file: 'css.svg' },
    { extension: 'csv', file: 'csv.svg' },
    { extension: 'dbf', file: 'dbf.svg' },
    { extension: 'doc', file: 'doc.svg' },
    { extension: 'docx', file: 'doc.svg' },
    { extension: 'dwg', file: 'dwg.svg' },
    { extension: 'exe', file: 'exe.svg' },
    { extension: 'file', file: 'file.svg' },
    { extension: 'fla', file: 'fla.svg' },
    { extension: 'html', file: 'html.svg' },
    { extension: 'iso', file: 'iso.svg' },
    { extension: 'js', file: 'javascript.svg' },
    { extension: 'jpeg', file: 'jpg.svg' },
    { extension: 'jpg', file: 'jpg.svg' },
    { extension: 'json', file: 'json-file.svg' },
    { extension: 'mp3', file: 'pm3.svg' },
    { extension: 'mp4', file: 'mp4.svg' },
    { extension: 'odt', file: 'doc.svg' },
    { extension: 'pdf', file: 'pdf.svg' },
    { extension: 'png', file: 'png.svg' },
    { extension: 'ppt', file: 'ppt.svg' },
    { extension: 'pptx', file: 'ppt.svg' },
    { extension: 'psd', file: 'psd.svg' },
    { extension: 'rtf', file: 'rtf.svg' },
    { extension: 'svg', file: 'svg.svg' },
    { extension: 'txt', file: 'txt.svg' },
    { extension: 'xls', file: 'xls.svg' },
    { extension: 'xlsx', file: 'xlsx.svg' },
    { extension: 'xml', file: 'xml.svg' },
    { extension: 'zip', file: 'zip.svg' }
  ];

  transform (extension: string): string {
    const icon = this
      .fileTypes
      .find(fileType => fileType.extension === extension);
    if (icon != null) {
      return `${this.dir}/${icon.file}`;
    } else {
      return `${this.dir}/file.svg`;
    }
  }
}
