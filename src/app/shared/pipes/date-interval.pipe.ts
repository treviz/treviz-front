/**
 * Created by huber on 15/04/2017.
 */

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateInterval'
})
export class DateIntervalPipe implements PipeTransform {
  transform (datestring: string): string {
    let interval;

    if (datestring == null) { return ''; }

    const now = (new Date()).getTime();
    const date = new Date(datestring);

    if (now - date.getTime() < 60000) {
      interval = 'just now';
    } else {
      if (now - date.getTime() < 3600000) {
        interval = Math.floor((now - date.getTime()) / 60000) + ' min';
      } else {
        if (now - date.getTime() < 86400000) {
          interval = Math.floor((now - date.getTime()) / 3600000) + ' h';
        } else {
          interval = date.toLocaleDateString('fr-FR');
        }
      }
    }

    return interval;
  }
}
