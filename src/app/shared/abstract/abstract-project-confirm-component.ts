import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../dialogs/confirm-dialog/confirm-dialog.component';
import { takeUntil } from 'rxjs/operators';
import { OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { AbstractProjectErrorComponent } from './abstract-project-error-component';

export class AbstractProjectConfirmComponent extends AbstractProjectErrorComponent implements OnDestroy {
  protected destroyed$ = new Subject<void>();

  constructor (public dialog: MatDialog) {
    super(dialog);
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  protected confirmAction (messagesId: number, onSuccess: () => void, onReject: () => void = null) {
    this.dialog
      .open(ConfirmDialogComponent, { data: messagesId })
      .afterClosed()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(result => {
        if (result) {
          onSuccess();
        } else {
          if (onReject != null) {
            onReject();
          }
        }
      });
  }
}
