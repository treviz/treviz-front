import { OnDestroy } from '@angular/core';
import { take, takeUntil } from 'rxjs/operators';
import { SkillService } from '../../core/services/skill.service';
import { TagService } from '../../core/services/tag.service';
import { Subject } from 'rxjs';

export class AbstractTagSkillComponent implements OnDestroy {
  public skills: string[] = [];
  public tags: string[] = [];

  protected destroyed$ = new Subject<void>();

  constructor (protected skillService: SkillService, protected tagService: TagService) {
    this.skillService
      .getSkillsHttp()
      .pipe(take(1), takeUntil(this.destroyed$))
      .subscribe(
        skills => this.skills = skills.map((skill) => skill.name),
        () => console.log('impossible to fetch skills')
      );

    // Search by tag
    this.tagService
      .getTagsHttp()
      .pipe(take(1), takeUntil(this.destroyed$))
      .subscribe(
        tags => this.tags = tags.map((tag) => tag.name),
        () => console.log('impossible to fetch tags')
      );
  }

  ngOnDestroy (): void {
    this.destroyed$.next();
    this.destroyed$.complete();
  }
}
