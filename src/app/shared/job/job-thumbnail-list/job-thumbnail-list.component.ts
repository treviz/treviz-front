import {finalize, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';
import {ProjectJob} from '../../models/project/project-job.model';
import {ProjectJobService} from '../../../core/services/project/project-job.service';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {Project} from '../../models/project/project.model';
import {JobCreateDialogComponent} from '../job-create-dialog/job-create-dialog.component';
import {User} from '../../models/users/user.model';
import {MessageDialogComponent} from 'app/shared/dialogs/message-dialog/message-dialog.component';

@Component({
  selector: 'app-job-thumbnail-list',
  templateUrl: 'job-thumbnail-list.component.html',
  styleUrls: ['job-thumbnail-list.component.scss']
})
export class JobThumbnailListComponent implements OnChanges, OnDestroy {
  @Input() config = {
    holder: '',
    candidate: '',
    tags: [],
    skills: [],
    project: '',
    task: '',
    attributed: false,
    name: '',
    nb: 20,
    offset: 0
  };

  @Input() project: Project;

  @Input() user: User;

  @Input() displayCreateButton = false;

  jobs: ProjectJob[] = [];

  jobsFetched = false;

  moreToLoad = true;

  private destroyed$ = new Subject<void>();

  constructor (private projectJobService: ProjectJobService,
              public dialog: MatDialog) { }

  ngOnChanges (changes: SimpleChanges) {
    for (const propName in changes) {
      if (propName === 'project') {
        this.config.project = this.project.hash;
      } else if (propName === 'user') {
        this.config.holder = this.user.username;
      }
    }
    this.loadJobs();
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  loadJobs () {
    this.jobsFetched = false;
    this.config.offset = this.jobs.length;

    /*
     * If the project is defined, use the getProjectJobs method to fetch them.
     * Otherwise, use the generic getJobs method.
     */
    if (this.project) {
      this.projectJobService.getProjectJobs(this.project.hash)
        .pipe(
          takeUntil(this.destroyed$),
          finalize(() => this.jobsFetched = true)
        )
        .subscribe(
          jobs => {
            this.jobs = this.jobs.concat(jobs);
            this.moreToLoad = jobs.length === this.config.nb;
          }
        );
    } else {
      this.projectJobService.getJobs(this.config)
        .pipe(
          finalize(() => this.jobsFetched = true),
          takeUntil(this.destroyed$)
        )
        .subscribe(
          jobs => {
            this.jobs = this.jobs.concat(jobs);
            this.moreToLoad = jobs.length === this.config.nb;
          }
        );
    }
  }

  openNewJobDialog () {
    const config: MatDialogConfig = {
      data: this.project,
      maxWidth: '100vw',
      maxHeight: '100vh'
    };

    this.dialog.open(JobCreateDialogComponent, config)
      .afterClosed().pipe(
        takeUntil(this.destroyed$))
      .subscribe(result => {
        if (result != null) {
          if (result) {
            this.jobs.push(result);
          } else {
            this.displayErrorDialog('An error occurred while creating the job, do you have the correct rights ?');
          }
        }
      });
  }

  displayErrorDialog (message: string) {
    const config = new MatDialogConfig();
    config.data = {
      message,
      failure: true,
      actionRequired: true
    };

    this.dialog.open(MessageDialogComponent, config);
  }
}
