import {debounceTime, distinctUntilChanged, finalize, take, takeUntil} from 'rxjs/operators';
import {Component, Inject} from '@angular/core';
import {Project} from '../../models/project/project.model';
import {ProjectJobDto} from '../../models/project/project-job.model.dto';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ProjectJobService} from '../../../core/services/project/project-job.service';
import {User} from '../../models/users/user.model';
import {FormControl} from '@angular/forms';
import {UserService} from '../../../core/services/user.service';
import {TagService} from '../../../core/services/tag.service';
import {SkillService} from '../../../core/services/skill.service';
import {UserStateService} from '../../../core/services/user-state.service';
import {AbstractTagSkillComponent} from '../../abstract/abstract-tag-skill-component';

@Component({
  selector: 'app-job-create-dialog',
  templateUrl: 'job-create-dialog.component.html',
  styleUrls: ['job-create-dialog.component.scss']
})
export class JobCreateDialogComponent extends AbstractTagSkillComponent {
  project: Project;
  jobDto: ProjectJobDto = new ProjectJobDto();

  jobSubmitted = false;

  holder: User;
  filteredHolder: User[];
  holderCtrl: FormControl;

  contact: User;
  filteredContact: User[];
  contactCtrl: FormControl;

  constructor (private dialogRef: MatDialogRef<JobCreateDialogComponent>,
              private projectJobService: ProjectJobService,
              private userService: UserService,
              protected skillService: SkillService,
              protected tagService: TagService,
              private currentUserService: UserStateService,
              @Inject(MAT_DIALOG_DATA) public data: Project,
              public snackBar: MatSnackBar) {
    super(skillService, tagService);
    this.project = data;
    if (this.project.tags) { this.jobDto.tags = this.project.tags.map(tag => tag.name); }
    if (this.project.skills) { this.jobDto.skills = this.project.skills.map(skill => skill.name); }

    this.holderCtrl = new FormControl();
    this.holderCtrl.valueChanges.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      takeUntil(this.destroyed$))
      .subscribe(
        name => {
          this.filteredHolder = [];
          if (name != null && name.length >= 2) {
            this.userService.getUsers({ name: name }).pipe(
              takeUntil(this.destroyed$))
              .subscribe(
                users => this.filteredHolder = users,
                err => console.log(err)
              );
          }
        }
      );

    this.contactCtrl = new FormControl();
    this.contactCtrl.valueChanges.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      takeUntil(this.destroyed$))
      .subscribe(
        name => {
          this.filteredContact = [];
          if (name != null && name.length >= 2) {
            this.userService.getUsers({ name: name }).pipe(
              takeUntil(this.destroyed$))
              .subscribe(
                users => this.filteredContact = users,
                err => console.log(err)
              );
          }
        }
      );
    const currentUser = this.currentUserService.getCurrentUser();
    this.contact = currentUser;
    this.jobDto.contact = currentUser.username;
  }

  addSkill (skill: string) {
    if (skill !== '' && this.jobDto.skills.indexOf(skill) === -1) {
      this.jobDto.skills.push(skill);
    }
  }

  setSkills (skills: string[]) {
    this.jobDto.skills = skills;
  }

  setTags (tags: string[]) {
    this.jobDto.tags = tags;
  }

  setHolder (username: string): void {
    const holder = this.filteredHolder.find(user => user.username === username);
    if (holder) {
      this.holder = holder;
      this.jobDto.holder = username;
    }
  }

  removeHolder (): void {
    this.holder = null;
    this.jobDto.holder = null;
  }

  setContact (username: string): void {
    const contact = this.filteredContact.find(user => user.username === username);
    if (contact) {
      this.contact = contact;
      this.jobDto.contact = username;
    }
  }

  removeContact (): void {
    this.contact = null;
    this.jobDto.contact = null;
  }

  onSubmit () {
    if (this.contact == null || this.jobDto.contact == null) {
      this.snackBar.open('You must specify a contact for this job !', 'Close', { duration: 3000 });
    } else {
      this.jobSubmitted = true;
      this.projectJobService
        .postProjectJob(this.project.hash, this.jobDto)
        .pipe(
          take(1),
          finalize(() => this.jobSubmitted = false),
          takeUntil(this.destroyed$)
        )
        .subscribe(
          job => this.dialogRef.close(job),
          () => this.dialogRef.close(false)
        );
    }
  }
}
