import { Component, Input, OnInit } from '@angular/core';
import { ProjectJob } from '../../models/project/project-job.model';

@Component({
  selector: 'app-job-thumbnail',
  templateUrl: 'job-thumbnail.component.html',
  styleUrls: ['job-thumbnail.component.scss']
})
export class JobThumbnailComponent implements OnInit {
  @Input() job: ProjectJob;
  @Input() displayImage = true;
  tags: string[];
  public nbRemainingTags = 0;

  ngOnInit (): void {
    const tags = [...this.job.skills, ...this.job.tags];
    this.tags = tags.slice(0, 3).map((el) => el.name);
    this.nbRemainingTags = tags.length - 3;
  }
}
