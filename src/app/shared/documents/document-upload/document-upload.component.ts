
import { take } from 'rxjs/operators';
import { Component, Input } from '@angular/core';
import { DocumentDto } from '../../models/document/document.model.dto';
import { DocumentService } from '../../../core/services/document.service';
import { MatDialogRef } from '@angular/material/dialog';
import { Project } from '../../models/project/project.model';
import { BrainstormingIdea } from '../../models/brainstorming/brainstorming-idea.model';
import { Community } from '../../models/community/community.model';
import { Room } from '../../models/chat/room.model';

@Component({
  selector: 'app-document-upload',
  templateUrl: 'document-upload.component.html',
  styleUrls: ['document-upload.component.scss']
})
export class DocumentUploadComponent {
  @Input() project: Project;
  @Input() community: Community;
  @Input() idea: BrainstormingIdea;
  @Input() room: Room;

  public documentDto: DocumentDto = new DocumentDto();

  public submitted = false;

  constructor (public dialogRef: MatDialogRef<DocumentUploadComponent>,
              private documentService: DocumentService) { }

  setFile (file: File) {
    this.documentDto.file = file;
  }

  onSubmit () {
    this.submitted = true;
    this.documentService.postDocument(this.documentDto)
      .pipe(take(1))
      .subscribe(
        data => this.dialogRef.close(data),
        () => this.dialogRef.close(false),
        () => this.submitted = false
      );
  }
}
