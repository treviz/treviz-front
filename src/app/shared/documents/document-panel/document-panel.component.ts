
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Component, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Document } from '../../models/document/document.model';
import { DocumentService } from '../../../core/services/document.service';

@Component({
  selector: 'app-document-panel',
  templateUrl: 'document-panel.component.html',
  styleUrls: ['document-panel.component.css']
})
export class DocumentPanelComponent implements OnDestroy {
  @Input()
  public document: Document;

  @Output() onDocumentDelete = new EventEmitter<Document>();

  private destroyed$ = new Subject<void>();

  constructor (private documentService: DocumentService) {}

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  canDelete () {
    return this.document.owner.username === localStorage.getItem('username');
  }

  deleteFile () {
    this.documentService.deleteDocument(this.document.hash).pipe(
      takeUntil(this.destroyed$))
      .subscribe(
        () => this.onDocumentDelete.emit(this.document)
      );
  }
}
