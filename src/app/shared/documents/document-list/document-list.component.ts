
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Component, Input, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DocumentUploadComponent } from '../document-upload/document-upload.component';
import { Community } from '../../models/community/community.model';
import { Project } from '../../models/project/project.model';
import { BrainstormingIdea } from '../../models/brainstorming/brainstorming-idea.model';
import { Room } from '../../models/chat/room.model';
import { DocumentService } from '../../../core/services/document.service';
import { Document } from '../../models/document/document.model';
import { MessageDialogComponent } from '../../dialogs/message-dialog/message-dialog.component';

@Component({
  selector: 'app-document-list',
  templateUrl: 'document-list.component.html',
  styleUrls: ['document-list.component.css']
})
export class DocumentListComponent implements OnChanges, OnDestroy {
  @Input()
  public community: Community;

  @Input()
  public project: Project;

  @Input()
  public idea: BrainstormingIdea;

  @Input()
  public room: Room;

  public documents: Document[] = [];

  public loading = true;

  private destroyed$ = new Subject<void>();

  constructor (private documentService: DocumentService,
              public dialog: MatDialog) {}

  ngOnChanges (changes: SimpleChanges) {
    const options = {
      community: '',
      room: '',
      project: '',
      idea: ''
    };

    for (const propName in changes) {
      if (propName === 'community') {
        options.community = this.community.hash;
      } else if (propName === 'room') {
        options.room = this.room.hash;
      } else if (propName === 'project') {
        options.project = this.project.hash;
      } else if (propName === 'idea') {
        options.idea = this.idea.hash;
      }
    }

    this.documentService.getDocuments(options).pipe(
      takeUntil(this.destroyed$))
      .subscribe(
        docs => this.documents = docs,
        err => console.log(err),
        () => this.loading = false
      );
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  openDocumentUploadDialog () {
    const dialogRef = this.dialog.open(
      DocumentUploadComponent,
      { maxWidth: '100vw', maxHeight: '100vh' }
    );
    if (this.community !== undefined) {
      dialogRef.componentInstance.documentDto.community = this.community.hash;
    } else if (this.project !== undefined) {
      dialogRef.componentInstance.documentDto.project = this.project.hash;
    } else if (this.room !== undefined) {
      dialogRef.componentInstance.documentDto.room = this.room.hash;
    } else if (this.idea !== undefined) {
      dialogRef.componentInstance.documentDto.idea = this.idea.hash;
    }

    dialogRef.afterClosed().pipe(
      takeUntil(this.destroyed$))
      .subscribe(
        data => {
          if (data != null) {
            if (data) {
              this.documents.push(data);
            } else {
              this.displayErrorDialog();
            }
          }
        }
      );
  }

  displayErrorDialog () {
    const config = new MatDialogConfig();
    config.data = {
      message: 'An error occurred while uploading your file, please check it is less than 20MB',
      failure: true,
      actionRequired: true
    };

    this.dialog.open(MessageDialogComponent, config);
  }

  removeDocument (document: Document) {
    this.documents.splice(this.documents.indexOf(document), 1);
  }
}
