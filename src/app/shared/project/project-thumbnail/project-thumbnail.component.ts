import {Component, Input, OnInit} from '@angular/core';
import {Project} from '../../models/project/project.model';

@Component({
  moduleId: module.id,
  selector: 'app-project-thumbnail',
  templateUrl: 'project-thumbnail.component.html',
  styleUrls: ['project-thumbnail.component.scss']
})
export class ProjectThumbnailComponent implements OnInit {
  @Input() project: Project;
  public firstTags: string[] = [];
  public nbRemainingTags = 0;

  ngOnInit (): void {
    const tags = [...this.project.tags, ...this.project.skills];
    this.nbRemainingTags = tags.length - 3;
    this.firstTags = tags.slice(0, 3).map((el) => el.name);
  }
}
