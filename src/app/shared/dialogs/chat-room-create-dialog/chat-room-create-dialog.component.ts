
import { debounceTime, takeUntil, distinctUntilChanged } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Component, Inject, OnDestroy } from '@angular/core';
import { RoomDto } from '../../models/chat/room.model.dto';
import { User } from '../../models/users/user.model';
import { FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UserService } from 'app/core/services/user.service';
import { ChatRoomService } from 'app/core/services/chat/chat-room.service';
import { UserStateService } from 'app/core/services/user-state.service';
import { ChatStateService } from '../../../core/services/chat-state.service';

@Component({
  selector: 'app-chat-room-create-dialog',
  templateUrl: 'chat-room-create-dialog.component.html',
  styleUrls: ['chat-room-create-dialog.component.scss']
})
export class ChatRoomCreateDialogComponent implements OnDestroy {
  public room = new RoomDto();
  public filteredUsers: User[];
  public userCtrl: FormControl;
  public usersToInvite: User[] = [];
  public isSubmitted = false;
  private destroyed$ = new Subject<void>();

  constructor (public dialogRef: MatDialogRef<ChatRoomCreateDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data,
              private chatRoomService: ChatRoomService,
              private userStateService: UserStateService,
              private chatStateService: ChatStateService,
              private userService: UserService) {
    if (data.project !== '') {
      this.room.project = data.project;
    } else if (data.community !== '') {
      this.room.community = data.project;
    }

    this.userCtrl = new FormControl();
    this.userCtrl.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        name => {
          this.filteredUsers = [];
          if (name != null && name.length >= 2) {
            this.userService.getUsers({ name: name }).pipe(
              takeUntil(this.destroyed$))
              .subscribe(
                users => this.filteredUsers = users,
                err => console.log(err)
              );
          }
        }
      );
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  addUser (user: User): void {
    if (user != null) {
      this.usersToInvite.push(user);
    }
  }

  addUserFromUsername (name): void {
    const user = this.filteredUsers.filter((s) => new RegExp(name, 'gi').test(s.username))[0];
    this.addUser(user);
  }

  removeUser (user: User): void {
    this.usersToInvite.splice(this.usersToInvite.indexOf(user), 1);
  }

  onSubmit (): void {
    this.isSubmitted = true;
    for (const user of this.usersToInvite) {
      this.room.users.push(user.username);
    }
    this.chatRoomService.postRoom(this.room).pipe(
      takeUntil(this.destroyed$))
      .subscribe(
        room => {
          this.chatStateService.addRooms([room]);
          this.dialogRef.close(room);
        },
        () => this.dialogRef.close(false),
        () => this.isSubmitted = false
      );
  }
}
