import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-message-dialog',
  templateUrl: 'message-dialog.component.html',
  styleUrls: ['message-dialog.component.scss']
})
export class MessageDialogComponent {
    public success = false;
    public failure = false;
    public message: string;
    public actionRequired = false;

    constructor (public dialogRef: MatDialogRef<MessageDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public data: {
                    message: string;
                    success: boolean|null;
                    failure: boolean|null;
                    actionRequired: boolean|null;
                }) {
      this.message = data.message;
      this.success = data.success;
      this.failure = data.failure;
      this.actionRequired = data.actionRequired;
    }

    confirm () {
      this.dialogRef.close(true);
    }
}
