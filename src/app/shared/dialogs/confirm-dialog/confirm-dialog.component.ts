import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ConfirmMessageModel } from '../../models/confirm-message.model';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: 'confirm-dialog.component.html',
  styleUrls: ['confirm-dialog.component.scss']
})
export class ConfirmDialogComponent {
  public messageId: number;
  public messageEnum = ConfirmMessageModel;

  constructor (public dialogRef: MatDialogRef<ConfirmDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: ConfirmMessageModel) {
    this.messageId = data;
  }

  confirm () {
    this.dialogRef.close(true);
  }

  abort () {
    this.dialogRef.close(false);
  }
}
