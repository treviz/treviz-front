
import { map, startWith, take, takeUntil } from 'rxjs/operators';
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { User, UserRoles } from '../../models/users/user.model';
import { UserDto } from '../../models/users/user.model.dto';
import { FormControl } from '@angular/forms';
import { SkillService } from '../../../core/services/skill.service';
import { Observable } from 'rxjs';
import { TagService } from '../../../core/services/tag.service';
import { UserService } from '../../../core/services/user.service';
import { OrganizationService } from '../../../core/services/organization.service';
import Organization from '../../models/organization.model';
import { AbstractTagSkillComponent } from '../../abstract/abstract-tag-skill-component';

@Component({
  selector: 'app-user-edit-dialog',
  templateUrl: 'user-edit-dialog.component.html',
  styleUrls: ['user-edit-dialog.component.scss']
})
export class UserEditDialogComponent extends AbstractTagSkillComponent {
  userDto: UserDto;

  organizationCtrl: FormControl;
  filteredOrganizations: Observable<Organization[]>;
  organizations: Organization[] = [];

  admin = false;
  submitted: boolean;

  constructor (public dialogRef: MatDialogRef<UserEditDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: {user: User; admin: boolean},
              protected skillService: SkillService,
              protected tagService: TagService,
              private userService: UserService,
              private organizationService: OrganizationService) {
    super(skillService, tagService);

    this.userDto = UserDto.fromUser(data.user);
    this.admin = data.user.roles.some((role) => role === UserRoles.ROLE_ADMIN);

    this.organizationCtrl = new FormControl();
    this.organizationService
      .getOrganizations()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        organizations => {
          this.organizations = organizations;
          if (this.userDto.organization) {
            this.organizationCtrl.setValue(this.userDto.organization);
          }
        },
        () => console.log('impossible to fetch organizations')
      );

    this.filteredOrganizations = this.organizationCtrl
      .valueChanges
      .pipe(
        startWith(null),
        map(name => this.filterOrganization(name))
      );

    this.submitted = false;
  }

  setSkills (skills: string[]) {
    this.userDto.skills = skills;
  }

  setTags (tags: string[]) {
    this.userDto.interests = tags;
  }

  filterOrganization (val: string) {
    return val ? this.organizations.filter((org) => new RegExp(val, 'gi').test(org.name)) : this.organizations;
  }

  onSubmit () {
    this.submitted = true;

    if (this.userDto.skills === []) {
      this.userDto.skills = undefined;
    }
    if (this.userDto.interests === []) {
      this.userDto.skills = undefined;
    }

    if (this.admin) {
      this.userDto.roles = [UserRoles.ROLE_ADMIN];
    }

    this.userDto.organization = this.organizationCtrl.value;

    this.userService
      .putUser(this.data.user.username, this.userDto)
      .pipe(take(1), takeUntil(this.destroyed$))
      .subscribe(
        data => this.dialogRef.close(data),
        () => {
          this.submitted = false;
          this.dialogRef.close(false);
        }
      );
  }
}
