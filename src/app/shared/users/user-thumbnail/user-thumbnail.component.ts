import { Component, Input } from '@angular/core';
import { User } from '../../models/users/user.model';

@Component({
  moduleId: module.id,
  selector: 'app-user-thumbnail',
  templateUrl: 'user-thumbnail.component.html',
  styleUrls: ['user-thumbnail.component.scss']
})
export class UserThumbnailComponent {
  @Input() user: User;
}
