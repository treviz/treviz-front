import { Component, Input } from '@angular/core';
import { User } from '../../models/users/user.model';

@Component({
  selector: 'app-user-avatar-circle',
  templateUrl: 'user-avatar-circle.component.html',
  styleUrls: ['user-avatar-circle.component.scss']
})
export class UserAvatarCircleComponent {
    @Input() user: User;
    @Input() small = false;
}
