import { Component, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Post } from '../../models/newsfeed/post.model';
import { PostDto } from '../../models/newsfeed/post.model.dto';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { CommentDto } from '../../models/newsfeed/comment.model.dto';
import { CommentsService } from '../../../core/services/comments.service';

@Component({
  selector: 'app-post-update-dialog',
  templateUrl: './comment-update-dialog.component.html',
  styleUrls: ['./comment-update-dialog.component.scss']
})
export class CommentUpdateDialogComponent implements OnDestroy {
  public commentDto: CommentDto;
  public submitted = false;
  private destroyed$ = new Subject<void>();

  constructor (private dialogRef: MatDialogRef<CommentUpdateDialogComponent>,
              @Inject(MAT_DIALOG_DATA) private data: Post,
              private commentService: CommentsService) {
    this.commentDto = PostDto.from(data);
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  onSubmit () {
    this.commentService.putComment(this.data.hash, this.commentDto)
      .pipe(
        takeUntil(this.destroyed$)
      )
      .subscribe(
        (updatedPost) => this.dialogRef.close(updatedPost),
        () => this.dialogRef.close(false)
      );
  }

  close () {
    this.dialogRef.close();
  }
}
