import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Comment } from '../../models/newsfeed/comment.model';
import { CommentsService } from '../../../core/services/comments.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ConfirmDialogComponent } from '../../dialogs/confirm-dialog/confirm-dialog.component';
import { MessageDialogComponent } from '../../dialogs/message-dialog/message-dialog.component';
import { CommentUpdateDialogComponent } from '../comment-update-dialog/comment-update-dialog.component';
import { ConfirmMessageModel } from '../../models/confirm-message.model';

@Component({
  selector: 'app-comment',
  templateUrl: 'comment.component.html',
  styleUrls: ['comment.component.scss']
})
export class CommentComponent {
  @Input()
  comment: Comment;

  @Output()
  onCommentUpdate = new EventEmitter<Comment>();

  @Output()
  onCommentDelete = new EventEmitter<Comment>();

  public get isAuthor () {
    return this.comment.author.username === localStorage.getItem('username');
  }

  public submitted = false;
  private destroyed$ = new Subject<void>();

  constructor (private commentService: CommentsService,
              private dialog: MatDialog) {
  }

  openEditDialog () {
    const config: MatDialogConfig = {
      maxWidth: '100vw !important',
      data: this.comment
    };

    const dialog = this.dialog.open(CommentUpdateDialogComponent, config);
    dialog
      .afterClosed()
      .pipe(
        takeUntil(this.destroyed$)
      )
      .subscribe(
        (result) => {
          switch (result) {
            case false:
              this.displayErrorDialog('An error occurred while updating the post.');
              break;
            case null:
            case undefined:
              break;
            default:
              this.onCommentUpdate.emit(result);
              break;
          }
        }
      );
  }

  deleteComment () {
    this.dialog.open(ConfirmDialogComponent, {
      data: ConfirmMessageModel.COMMENT_DELETE
    })
      .afterClosed()
      .pipe(
        takeUntil(this.destroyed$)
      )
      .subscribe(
        (ok) => {
          if (ok) {
            this.submitted = true;
            this.commentService
              .deleteComment(this.comment.hash)
              .pipe(
                takeUntil(this.destroyed$)
              )
              .subscribe(
                () => this.onCommentDelete.emit(this.comment)
              );
          }
        }
      );
  }

  displayErrorDialog (message: string) {
    const config: MatDialogConfig = {
      data: {
        message,
        failure: true,
        actionRequired: true
      }
    };

    this.dialog.open(MessageDialogComponent, config);
  }
}
