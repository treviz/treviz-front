import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Post } from '../../models/newsfeed/post.model';
import { CommentDto } from '../../models/newsfeed/comment.model.dto';
import { CommentsService } from '../../../core/services/comments.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { PostUpdateDialogComponent } from '../post-update-dialog/post-update-dialog.component';
import { MessageDialogComponent } from '../../dialogs/message-dialog/message-dialog.component';
import { ConfirmDialogComponent } from '../../dialogs/confirm-dialog/confirm-dialog.component';
import { PostsService } from '../../../core/services/posts.service';
import { Comment } from '../../models/newsfeed/comment.model';
import { UserStateService } from '../../../core/services/user-state.service';
import { ConfirmMessageModel } from '../../models/confirm-message.model';

@Component({
  selector: 'app-post',
  templateUrl: 'post.component.html',
  styleUrls: ['post.component.scss']
})
export class PostComponent implements OnInit, OnDestroy {
  @Input()
  post: Post;

  @Input()
  showOrigin = false;

  @Output()
  onPostDelete = new EventEmitter<Post>();

  @Output()
  onPostUpdate = new EventEmitter<Post>();

  commentsIndex = 0;
  comment = new CommentDto();
  submitted = false;
  edit = false;

  get comments () {
    return this.post.comments.slice(this.commentsIndex, this.post.comments.length);
  }

  get isAuthor () {
    return this.post.author.username === localStorage.getItem('username');
  }

  private destroyed$ = new Subject<void>();

  constructor (private currentUserService: UserStateService,
              private commentService: CommentsService,
              private postsService: PostsService,
              private dialog: MatDialog) {}

  ngOnInit () {
    this.commentsIndex = Math.max(this.post.comments.length - 5, 0);
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  openEditDialog () {
    const config: MatDialogConfig = {
      maxWidth: '100vw !important',
      data: this.post
    };

    const dialog = this.dialog.open(PostUpdateDialogComponent, config);
    dialog
      .afterClosed()
      .pipe(
        takeUntil(this.destroyed$)
      )
      .subscribe(
        (result) => {
          switch (result) {
            case false:
              this.displayErrorDialog('An error occurred while updating the post.');
              break;
            case null:
            case undefined:
              break;
            default:
              this.onPostUpdate.emit(result);
              break;
          }
        }
      );
  }

  deletePost () {
    this.dialog.open(ConfirmDialogComponent, {
      data: ConfirmMessageModel.POST_DELETE
    })
      .afterClosed()
      .pipe(
        takeUntil(this.destroyed$)
      )
      .subscribe(
        (ok) => {
          if (ok) {
            this.submitted = true;
            this.postsService
              .deletePost(this.post.hash)
              .pipe(
                takeUntil(this.destroyed$)
              )
              .subscribe(
                () => this.onPostDelete.emit(this.post)
              );
          }
        }
      );
  }

  submitComment () {
    if (this.comment.message !== '' && this.comment.message != null) {
      this.submitted = true;
      this.commentService.postComment(this.post.hash, this.comment).pipe(
        takeUntil(this.destroyed$))
        .subscribe(
          data => {
            this.post.comments.push(data);
            this.comment.message = '';
          },
          err => console.log(err),
          () => this.submitted = false
        );
    }
  }

  onCommentDelete (comment: Comment) {
    this.post.comments.splice(
      this.post.comments.findIndex((el: Comment) => el.hash === comment.hash), 1
    );
  }

  onCommentUpdate (comment: Comment) {
    const index = this.post.comments.findIndex((el: Comment) => el.hash === comment.hash);
    if (index > -1) {
      this.post.comments[index] = comment;
    }
  }

  displayMoreComments () {
    this.commentsIndex = Math.max(0, this.commentsIndex - 5);
  }

  displayErrorDialog (message: string) {
    const config: MatDialogConfig = {
      data: {
        message,
        failure: true,
        actionRequired: true
      }
    };

    this.dialog.open(MessageDialogComponent, config);
  }
}
