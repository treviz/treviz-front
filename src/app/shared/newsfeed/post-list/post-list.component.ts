
import { finalize, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Community } from '../../models/community/community.model';
import { Project } from '../../models/project/project.model';
import { PostsService } from '../../../core/services/posts.service';
import { Post } from '../../models/newsfeed/post.model';
import { PostDto } from '../../models/newsfeed/post.model.dto';
import { Task } from '../../models/kanban/task.model';
import { ProjectJob } from '../../models/project/project-job.model';
import { Document } from '../../models/document/document.model';

@Component({
  selector: 'app-post-list',
  templateUrl: 'post-list.component.html',
  styleUrls: ['post-list.component.scss']
})
export class PostListComponent implements OnInit, OnDestroy {
  @Input()
  public community: Community;

  @Input()
  public project: Project;

  @Input()
  public task: Task;

  @Input()
  public job: ProjectJob;

  @Input()
  public document: Document;

  @Input()
  public canPost = true;

  /**
   * When set to true, the posts will display where they come from (project, community, task...)
   * This is useful in the home post thread, where posts are mixed from various sources.
   * @type { boolean }
   */
  @Input()
  public showOrigin = false;

  public posts: Post[] = [];

  public postDto = new PostDto();

  public loading = true;

  public firstLoad = true;

  public submitted = false;

  private destroyed$ = new Subject<void>();

  private options = {
    community: '',
    project: '',
    task: '',
    job: '',
    document: '',
    offset: 0
  };

  constructor (private postsService: PostsService) {}

  ngOnInit () {
    if (this.community) {
      this.options.community = this.community.hash;
      this.postDto.community = this.community.hash;
    } else if (this.project) {
      this.options.project = this.project.hash;
      this.postDto.project = this.project.hash;
    } else if (this.task) {
      this.options.task = this.task.hash;
      this.postDto.task = this.task.hash;
    } else if (this.job) {
      this.options.job = this.job.hash;
      this.postDto.job = this.job.hash;
    } else if (this.document) {
      this.options.document = this.document.hash;
      this.postDto.document = this.document.hash;
    }

    this.postsService.getPosts(this.options)
      .pipe(
        finalize(() => {
          this.loading = false;
          this.firstLoad = false;
        }),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        posts => this.posts = this.posts.concat(posts),
        err => console.log(err)
      );
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  publishPost () {
    this.submitted = true;

    this.postsService.postPost(this.postDto).pipe(
      takeUntil(this.destroyed$))
      .subscribe(
        data => {
          this.posts.unshift(data);
          this.postDto.message = '';
          this.postDto.link = '';
          this.submitted = false;
        },
        err => console.log(err)
      );
  }

  updatePost (updatedPost: Post) {
    const indexToUpdate = this.posts.findIndex((post) => post.hash === updatedPost.hash);
    if (indexToUpdate > -1) {
      this.posts[indexToUpdate] = updatedPost;
    }
  }

  removePost (removedPost: Post) {
    const indexToRemove = this.posts.indexOf(removedPost);
    this.posts.splice(indexToRemove, 1);
  }

  private loadPosts (offset: number) {
    this.loading = true;
    this.options.offset = offset;
    this.postsService.getPosts(this.options)
      .pipe(
        finalize(() => this.loading = false),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        posts => this.posts = this.posts.concat(posts),
        err => console.log(err),
        () => this.loading = false
      );
  }
}
