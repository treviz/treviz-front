import { Component, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Post } from '../../models/newsfeed/post.model';
import { PostsService } from '../../../core/services/posts.service';
import { PostDto } from '../../models/newsfeed/post.model.dto';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-post-update-dialog',
  templateUrl: './post-update-dialog.component.html',
  styleUrls: ['./post-update-dialog.component.scss']
})
export class PostUpdateDialogComponent implements OnDestroy {
  public postDto: PostDto;
  public submitted = false;
  private destroyed$ = new Subject<void>();

  constructor (private dialogRef: MatDialogRef<PostUpdateDialogComponent>,
              @Inject(MAT_DIALOG_DATA) private data: Post,
              private postsService: PostsService) {
    this.postDto = PostDto.from(data);
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  onSubmit () {
    this.postsService.putPost(this.data.hash, this.postDto)
      .pipe(
        takeUntil(this.destroyed$)
      )
      .subscribe(
        (updatedPost) => this.dialogRef.close(updatedPost),
        () => this.dialogRef.close(false)
      );
  }

  close () {
    this.dialogRef.close();
  }
}
