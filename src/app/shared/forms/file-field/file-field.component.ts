import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';
import {coerceBooleanProperty} from "@angular/cdk/coercion";
import {resizeImage} from "../../../../helpers/file-helper";

@Component({
  selector: 'treviz-file-field',
  templateUrl: './file-field.component.html',
  styleUrls: ['./file-field.component.scss']
})
export class FileFieldComponent implements OnChanges {
  @Input() file: File|null;
  @Input() mimeTypes: string = '';
  @Input() maxWidth: number = 360;
  @Input() get reduceImageSize(): boolean {
    return this._reduceImageSize;
  }
  set reduceImageSize(val) {
    this._reduceImageSize = coerceBooleanProperty(val);
  }

  @Output() onFileDrop = new EventEmitter<File>();

  public imgUrl: SafeUrl;
  public extension: string;
  private _reduceImageSize;

  constructor (private sanitizer: DomSanitizer) { }

  ngOnChanges (): void {
    this.extension = null;
    this.imgUrl = null;

    if (null != this.file) {
      if (this.isImage(this.file)) {
        const url = URL.createObjectURL(this.file);
        this.imgUrl = this.sanitizer.bypassSecurityTrustUrl(url);
      } else {
        this.extension = this.file != null ? this.file.name.split('.').pop() : '';
      }
    }
  }

  addFile (file) {
    if (this.isImage(file) && this.reduceImageSize) {
      resizeImage(file, this.maxWidth)
        .then((resizedImage) => this.onFileDrop.emit(resizedImage));
    } else {
      this.onFileDrop.emit(file);
    }
  }

  removeFile () {
    this.onFileDrop.emit(null);
  }

  allowDrop (ev) {
    ev.preventDefault();
  }

  handleChange(ev) {
    const file = this.getFileToHandle(ev.target?.files);
    if (null != file) {
      this.addFile(file);
    }
  }

  handleDrop (ev) {
    this.allowDrop(ev);
    const file = this.getFileToHandle(ev.dataTransfer.files);
    if (null != file) {
      this.addFile(file);
    }
  }

  private getFileToHandle(files: FileList): File {
    if (this.mimeTypes?.length === 0) {
      return files[0];
    }

    const nbFiles = files.length;
    const allowedTypes = new Map<string, boolean>();
    this
      .mimeTypes
      .split(', ')
      .forEach(type => allowedTypes.set(type, true));

    for (let i = 0; i < nbFiles; i++) {
      const file = files[i];
      if (allowedTypes.get(file.type)) {
        return file;
      }
    }
  }

  private isImage(file: File) {
    return {
      'image/png': true,
      'image/jpeg': true,
      'image/svg+xml': true
    }[file.type] ?? false;
  }
}
