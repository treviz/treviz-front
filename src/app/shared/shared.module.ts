/* Basics Angular Modules */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/* Material Design Modules */

import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatBadgeModule } from '@angular/material/badge';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatStepperModule } from '@angular/material/stepper';
import { MatInputModule } from '@angular/material/input';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatChipsModule } from '@angular/material/chips';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatMenuModule } from '@angular/material/menu';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTableModule } from '@angular/material/table';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCardModule } from '@angular/material/card';
import { MatNativeDateModule } from '@angular/material/core';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';

import 'hammerjs';

/* Module for markdown support */
import { NgxMdModule } from 'ngx-md';

/* Module for drag and drop */
import { DragulaModule, DragulaService } from 'ng2-dragula';

/* Shared components of the application */
import { ProjectThumbnailComponent } from './project/project-thumbnail/project-thumbnail.component';
import { CommunityThumbnailComponent } from './community-thumbnail/community-thumbnail.component';
import { UserThumbnailComponent } from './users/user-thumbnail/user-thumbnail.component';
import { UserAvatarCircleComponent } from './users/user-avatar-circle/user-avatar-circle.component';
import { UserEditDialogComponent } from './users/user-edit-dialog/user-edit-dialog.component';
import { DocumentListComponent } from './documents/document-list/document-list.component';
import { DocumentUploadComponent } from './documents/document-upload/document-upload.component';
import { PostComponent } from './newsfeed/post/post.component';
import { PostListComponent } from './newsfeed/post-list/post-list.component';
import { PostUpdateDialogComponent } from './newsfeed/post-update-dialog/post-update-dialog.component';
import { CommentComponent } from './newsfeed/comment/comment.component';
import { CommentUpdateDialogComponent } from './newsfeed/comment-update-dialog/comment-update-dialog.component';
import { ProjectThumbnailListComponent } from './project/project-thumbnail-list/project-thumbnail-list.component';
import { CandidacyDialogComponent } from './candidacy/candidacy-dialog/candidacy-dialog.component';
import { ConfirmDialogComponent } from './dialogs/confirm-dialog/confirm-dialog.component';
import { MessageDialogComponent } from './dialogs/message-dialog/message-dialog.component';
import { ChatRoomCreateDialogComponent } from './dialogs/chat-room-create-dialog/chat-room-create-dialog.component';

import { BrainstormingComponent } from './brainstorming/brainstorming.component';
import { BrainstormingSessionsListComponent } from './brainstorming/brainstorming-sessions-list/brainstorming-sessions-list.component';
import { BrainstormingSessionCardComponent } from './brainstorming/brainstorming-session-card/brainstorming-session-card.component';
import { BrainstormingSessionDetailComponent } from './brainstorming/brainstorming-session-detail/brainstorming-session-detail.component';
import { BrainstormingIdeaDetailComponent } from './brainstorming/brainstorming-idea-detail/brainstorming-idea-detail.component';
import { BrainstormingIdeaCardComponent } from './brainstorming/brainstorming-idea-card/brainstorming-idea-card.component';
import { BrainstormingEnhancementListComponent } from './brainstorming/brainstorming-enhancement-list/brainstorming-enhancement-list.component';
import { BrainstormingEnhancementCardComponent } from './brainstorming/brainstorming-enhancement-card/brainstorming-enhancement-card.component';
import { BrainstormingIdeaListComponent } from './brainstorming/brainstorming-idea-list/brainstorming-idea-list.component';
import { BrainstormingSessionCreateDialogComponent } from './brainstorming/brainstorming-session-create-dialog/brainstorming-session-create-dialog.component';
import { DocumentPanelComponent } from './documents/document-panel/document-panel.component';
import { BoardComponent } from './kanban/board/board.component';
import { BoardListComponent } from './kanban/board-list/board-list.component';
import { ColumnComponent } from './kanban/column/column.component';
import { TaskComponent } from './kanban/task/task.component';
import { TaskDetailDialogComponent } from './kanban/task-detail-dialog/task-detail-dialog.component';
import { BoardCreateDialogComponent } from './kanban/board-create-dialog/board-create-dialog.component';
import { BoardEditDialogComponent } from './kanban/board-edit-dialog/board-edit-dialog.component';
import { JobThumbnailComponent } from './job/job-thumbnail/job-thumbnail.component';
import { JobCreateDialogComponent } from './job/job-create-dialog/job-create-dialog.component';
import { JobThumbnailListComponent } from './job/job-thumbnail-list/job-thumbnail-list.component';
import { CandidacyThumbnailComponent } from './candidacy/candidacy-thumbnail/candidacy-thumbnail.component';
import { CandidacyThumbnailListComponent } from './candidacy/candidacy-thumbnail-list/candidacy-thumbnail-list.component';
import { CommunityCandidacyListComponent } from './candidacy/community-candidacy-list/community-candidacy-list.component';
import { ProjectCandidacyListComponent } from './candidacy/project-candidacy-list/project-candidacy-list.component';
import { FeedbackComponent } from './kanban/feedback/feedback.component';
import { InvitationThumbnailComponent } from './invitations/invitation-thumbnail/invitation-thumbnail.component';
import { InvitationThumbnailListComponent } from './invitations/invitation-thumbnail-list/invitation-thumbnail-list.component';
import { FeedbackListComponent } from './kanban/feedback-list/feedback-list.component';

/*
 * Pipes
 */
import { DateIntervalPipe } from './pipes/date-interval.pipe';
import { TruncatePipe } from './pipes/truncate.pipe';
import { FilterDialogComponent } from './filters/filter-dialog.component';
import { FilterComponent } from './filters/filter.component';
import { ChipAutocompleteComponent } from './filters/chip-autocomplete.component';
import { FileFieldComponent } from './forms/file-field/file-field.component';
import { FileSizePipe } from './pipes/file-size.pipe';
import { FilePreviewPipe } from './pipes/file-preview.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,

    MatAutocompleteModule,
    MatButtonModule,
    MatBadgeModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDialogModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatStepperModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatTableModule,
    MatTabsModule,
    MatTooltipModule,

    NgxMdModule.forRoot(),

    DragulaModule
  ],
  declarations: [
    /*
     * Brainstorming component
     */
    BrainstormingComponent,
    BrainstormingSessionCreateDialogComponent,
    BrainstormingSessionsListComponent,
    BrainstormingSessionCardComponent,
    BrainstormingSessionDetailComponent,
    BrainstormingIdeaListComponent,
    BrainstormingIdeaCardComponent,
    BrainstormingIdeaDetailComponent,
    BrainstormingEnhancementListComponent,
    BrainstormingEnhancementCardComponent,
    /*
     * New feed components (posts, comments...)
     */
    PostComponent,
    PostListComponent,
    PostUpdateDialogComponent,
    CommentUpdateDialogComponent,
    CommentComponent,
    PostUpdateDialogComponent,
    CommentUpdateDialogComponent,
    /*
     * Documents components
     */
    DocumentListComponent,
    DocumentPanelComponent,
    DocumentUploadComponent,
    /*
     * Job Components
     */
    JobCreateDialogComponent,
    JobThumbnailComponent,
    JobThumbnailListComponent,
    /*
     * Job thumbnail
     */
    ProjectThumbnailComponent,
    ProjectThumbnailListComponent,
    /*
     * Community thumbnail
     */
    CommunityThumbnailComponent,
    /*
     * Kanban components
     */
    BoardComponent,
    BoardListComponent,
    BoardCreateDialogComponent,
    BoardEditDialogComponent,
    ColumnComponent,
    FeedbackComponent,
    FeedbackListComponent,
    TaskComponent,
    TaskDetailDialogComponent,
    /*
     * General purpose components
     */
    CandidacyThumbnailComponent,
    CandidacyThumbnailListComponent,
    CommunityCandidacyListComponent,
    ProjectCandidacyListComponent,
    CandidacyDialogComponent,
    InvitationThumbnailComponent,
    InvitationThumbnailListComponent,
    ConfirmDialogComponent,
    MessageDialogComponent,
    ChatRoomCreateDialogComponent,

    /*
     * User-centered components
     */
    UserThumbnailComponent,
    UserAvatarCircleComponent,
    UserEditDialogComponent,

    /*
     * Pipes
     */
    TruncatePipe,
    DateIntervalPipe,
    FilterDialogComponent,
    FilterComponent,
    ChipAutocompleteComponent,
    FileSizePipe,

    FileFieldComponent,

    FilePreviewPipe
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,

    /* Export material design modules for further use */
    MatAutocompleteModule,
    MatButtonModule,
    MatBadgeModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatStepperModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatTableModule,
    MatTabsModule,
    MatTooltipModule,
    MatProgressBarModule,

    /* Exports Markdown Module */
    NgxMdModule,

    DragulaModule,

    BrainstormingComponent,
    CommunityThumbnailComponent,
    CommentComponent,
    ConfirmDialogComponent,
    MessageDialogComponent,
    ChatRoomCreateDialogComponent,
    CandidacyDialogComponent,
    CandidacyThumbnailComponent,
    CandidacyThumbnailListComponent,
    CommunityCandidacyListComponent,
    ProjectCandidacyListComponent,
    DocumentListComponent,
    DocumentPanelComponent,
    DocumentUploadComponent,
    InvitationThumbnailComponent,
    InvitationThumbnailListComponent,
    JobCreateDialogComponent,
    JobThumbnailComponent,
    JobThumbnailListComponent,
    ProjectThumbnailComponent,
    ProjectThumbnailListComponent,
    PostComponent,
    PostListComponent,
    PostUpdateDialogComponent,
    UserThumbnailComponent,
    UserAvatarCircleComponent,
    UserEditDialogComponent,

    /* Kanban components */
    BoardComponent,
    BoardListComponent,
    BoardCreateDialogComponent,
    BoardEditDialogComponent,
    ColumnComponent,
    FeedbackComponent,
    FeedbackListComponent,
    TaskComponent,
    TaskDetailDialogComponent,

    /* Pipes */
    TruncatePipe,
    DateIntervalPipe,
    FilterComponent,
    ChipAutocompleteComponent,

    /* Form helpers */
    FileFieldComponent
  ],
  providers: [
    DragulaService
  ]
})
export class SharedModule { }
