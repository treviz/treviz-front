export default interface PlatformSettings {
  host: string;
  termsOfService: string;
  privacyNotice: string;
  documentation: string;
  version: string;
  open: boolean;
}
