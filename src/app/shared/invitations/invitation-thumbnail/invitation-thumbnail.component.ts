
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Component, EventEmitter, Input, Output, OnDestroy } from '@angular/core';
import { ProjectInvitation } from '../../models/project/project-invitation.model';
import { ProjectInvitationService } from '../../../core/services/project/project-invitation.service';
import { CommunityInvitation } from '../../models/community/community-invitation.model';
import { CommunityInvitationService } from '../../../core/services/community/community-invitation.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MessageDialogComponent } from 'app/shared/dialogs/message-dialog/message-dialog.component';

@Component({
  selector: 'app-invitation-thumbnail',
  templateUrl: 'invitation-thumbnail.component.html',
  styleUrls: ['invitation-thumbnail.component.scss']
})
export class InvitationThumbnailComponent implements OnDestroy {
  @Input() projectInvitation: ProjectInvitation;

  @Input() communityInvitation: CommunityInvitation;

  @Input() canApprove = false;

  @Input() canDiscard = false;

  @Output() onProjectInvitationRemoval = new EventEmitter<ProjectInvitation>();

  @Output() onCommunityInvitationRemoval = new EventEmitter<CommunityInvitation>();

  public pending = false;
  private destroyed$ = new Subject<void>();

  constructor (private projectInvitationService: ProjectInvitationService,
              private communityInvitationService: CommunityInvitationService,
              public dialog: MatDialog) {
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  /*
   * Invitation management
   */
  acceptInvitation (): void {
    this.pending = true;
    if (this.projectInvitation) {
      this.projectInvitationService.acceptProjectInvitation(this.projectInvitation.hash).pipe(
        takeUntil(this.destroyed$))
        .subscribe(
          () => {
            this.onProjectInvitationRemoval.emit(this.projectInvitation);
          },
          () => {
            this.displayErrorDialog('An error occurred while accepting the invitation');
            this.pending = false;
          }
        );
    } else if (this.communityInvitation) {
      this.communityInvitationService.acceptCommunityInvitation(this.communityInvitation.hash).pipe(
        takeUntil(this.destroyed$))
        .subscribe(
          () => {
            this.onCommunityInvitationRemoval.emit(this.communityInvitation);
          },
          () => {
            this.displayErrorDialog('An error occurred while accepting the invitation');
            this.pending = false;
          }
        );
    }
  }

  removeInvitation (): void {
    this.pending = true;
    if (this.projectInvitation) {
      this.projectInvitationService.deleteProjectInvitation(this.projectInvitation.hash).pipe(
        takeUntil(this.destroyed$))
        .subscribe(
          () => {
            this.onProjectInvitationRemoval.emit(this.projectInvitation);
          },
          () => {
            this.displayErrorDialog('An error occurred while removing the invitation');
            this.pending = false;
          }
        );
    } else if (this.communityInvitation) {
      this.communityInvitationService.deleteCommunityInvitation(this.communityInvitation.hash).pipe(
        takeUntil(this.destroyed$))
        .subscribe(
          () => {
            this.onCommunityInvitationRemoval.emit(this.communityInvitation);
          },
          () => {
            this.displayErrorDialog('An error occurred while removing the invitation');
            this.pending = false;
          }
        );
    }
  }

  displayErrorDialog (message: string) {
    const config = new MatDialogConfig();
    config.data = {
      message,
      failure: true,
      actionRequired: true
    };

    this.dialog.open(MessageDialogComponent, config);
  }
}
