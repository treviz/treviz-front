export enum ConfirmMessageModel {
  PROJECT_DELETE,
  COMMUNITY_DELETE,
  POST_DELETE,
  COMMENT_DELETE,
  MESSAGE_DELETE,
  CHAT_ROOM,
  MAIL_DOMAIN_DELETE,
  COMMUNITY_LEAVE,
  PROJECT_LEAVE,
  ACCOUNT_DELETE,
  USER_DELETE,
  ORGANIZATION_DELETE,
  QUIT_JOB
}
