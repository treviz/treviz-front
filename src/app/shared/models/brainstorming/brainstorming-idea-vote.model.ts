import { User } from '../users/user.model';

export class BrainstormingIdeaVote {
    score: number;
    user: User;
}
