/**
 * Created by huber on 26/08/2017.
 */
import {Community} from '../community/community.model';
import {Project} from '../project/project.model';

export class BrainstormingSession {
  hash: string;
  name: string;
  isOpen: boolean;
  description: string;
  nbIdeas: number;
  community?: Community;
  project?: Project;
}
