import { Community } from '../community/community.model';
import { User } from '../users/user.model';
import { Project } from '../project/project.model';
import { Comment } from '../newsfeed/comment.model';
import { Document } from '../document/document.model';
import { BrainstormingSession } from './brainstorming-session.model';
import { BrainstormingIdeaVote } from './brainstorming-idea-vote.model';

/**
 * Created by huber on 26/08/2017.
 */
export class BrainstormingIdea {
  hash: string;
  content: string;
  community: Community;
  comments: Comment[];
  liked: User[];
  forked: Project[];
  documents: Document[];
  author: User;
  session: BrainstormingSession;
  score: number;
  currentVote: number;
  votes: BrainstormingIdeaVote[];

  constructor () {
    this.comments = [];
    this.liked = [];
    this.forked = [];
    this.documents = [];
  }
}
