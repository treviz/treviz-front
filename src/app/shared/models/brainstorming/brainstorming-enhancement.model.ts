import { User } from '../users/user.model';

/**
 * Created by huber on 26/08/2017.
 */
export class BrainstormingEnhancement {
  hash: string;
  message: string;
  publicationDate: Date;
  author: User;
}
