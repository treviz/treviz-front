/**
 * Created by huber on 26/08/2017.
 */
import {BrainstormingSession} from "./brainstorming-session.model";

export class BrainstormingSessionDto {
  name: string;
  isOpen: boolean|string;
  description: string;
  project: string; // Hash of the project to which create the session.
  community: string; // Hash of the community to which create the session.

  static fromSession(session: BrainstormingSession): BrainstormingSessionDto {
    const sessionDto = new BrainstormingSessionDto();
    sessionDto.name = session.name;
    sessionDto.isOpen = session.isOpen;
    sessionDto.description = session.description;
    sessionDto.project = session.project?.hash;
    sessionDto.community = session.community?.hash;
    return sessionDto;
  }
}
