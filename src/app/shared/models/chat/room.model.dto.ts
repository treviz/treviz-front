/**
 * Created by huber on 11/07/2017.
 */

export class RoomDto {
  name: string;
  description: string;
  users: Array<string>;
  community: string;
  project: string;

  constructor () {
    this.users = [];
  }
}
