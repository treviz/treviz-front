import { Project } from '../project/project.model';
import { Community } from '../community/community.model';
import { Message } from './message.model';
import { User } from '../users/user.model';

/**
 * Created by huber on 11/07/2017.
 */
export class Room {
  hash: string;
  name: string;
  description: string;
  lastActive: Date; // ex: 2015-01-31T03:12:03.581Z
  created: Date;
  project: Project;
  community: Community;
  messages: Message[];
  users: User[];

  isActive: boolean;
  nbNotifications = 0;

  constructor () {
    this.messages = [];
    this.users = [];
    this.isActive = false;
  }
}
