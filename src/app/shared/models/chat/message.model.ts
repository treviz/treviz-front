import { User } from '../users/user.model';

/**
 * Created by huber on 11/07/2017.
 */
export class Message {
  hash: string;
  room_hash: string;
  owner: User;
  text: string;
  posted: Date; // ex : "2015-02-03T03:48:12.600Z"
}
