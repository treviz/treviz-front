/**
 * Created by huber on 11/07/2017.
 */
import { Message } from './message.model';

export class MessageDto {
  text: string;

  static from (message: Message): MessageDto {
    return { text: message.text };
  }
}
