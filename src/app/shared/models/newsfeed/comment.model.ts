import { User } from '../users/user.model';

export class Comment {
  hash: string;
  message: string;
  author: User;
  publicationDate: Date;
  interval: string;
}
