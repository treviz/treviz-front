import { User } from '../users/user.model';
import { Comment } from './comment.model';
import { Project } from '../project/project.model';
import { Community } from '../community/community.model';
import { Task } from '../kanban/task.model';
import { Document } from '../document/document.model';
import { ProjectJob } from '../project/project-job.model';

export class Post {
  id: number;
  hash: string;
  message: string;
  link: string;
  author: User;
  comments: Comment[];
  publicationDate: Date;
  interval: string;
  project: Project;
  community: Community;
  document: Document;
  task: Task;
  job: ProjectJob;

  constructor () {
    this.comments = [];
  }
}
