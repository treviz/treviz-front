import { Post } from './post.model';

export class PostDto {
  message: string;
  link: string;
  project: string;
  community: string;
  task: string;
  job: string;
  document: string;

  static from (post: Post): PostDto {
    const postDto = new PostDto();
    postDto.message = post.message;
    postDto.link = post.link;
    if (null != post.project) { postDto.project = post.project.hash; }
    if (null != post.community) { postDto.community = post.community.hash; }
    if (null != post.job) { postDto.job = post.task.hash; }
    if (null != post.document) { postDto.document = post.document.hash; }

    return postDto;
  }
}
