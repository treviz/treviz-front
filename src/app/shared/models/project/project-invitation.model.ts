import { Project } from './project.model';
import { User } from '../users/user.model';

/**
 * Created by Bastien on 16/03/2017.
 */
export class ProjectInvitation {
  hash: string;
  project: Project;
  user: User;
  message: string;
}
