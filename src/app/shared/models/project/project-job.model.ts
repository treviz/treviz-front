import {Project} from './project.model';
import {User} from '../users/user.model';
import {Tag} from '../tags/tag.model';
import {Skill} from '../tags/skill.model';
import {ProjectCandidacy} from './project-candidacy.model';
import {Task} from '../kanban/task.model';

export class ProjectJob {
  hash: string;
  name: string;
  description: string;
  project: Project;
  holder: User;
  contact: User;
  tasks: Task[];
  tags: Tag[];
  skills: Skill[];
  applications: ProjectCandidacy[];
  nbApplications: number;

  constructor () {
    this.tags = [];
    this.applications = [];
  }
}
