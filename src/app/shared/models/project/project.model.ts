import { Skill } from '../tags/skill.model';
import { Tag } from '../tags/tag.model';
import { ProjectMembership } from './project-membership.model';
import { Community } from '../community/community.model';
import { Room } from '../chat/room.model';
import { BrainstormingIdea } from '../brainstorming/brainstorming-idea.model';

export class Project {
  hash: string;
  name: string;
  description: string;
  shortDescription: string;
  skills: Skill[];
  tags: Tag[];
  members: ProjectMembership[];
  communities: Community[];
  isVisible: boolean;
  isOpen: boolean;
  logoUrl: string;
  rooms: Room[];
  idea: BrainstormingIdea;
  nbMembers: number;
  nbJobsOpen: number;
  nbBoards: number;

  // this is for additonal control in chat
  isSelected = true;

  pendingNotification = 0;

  constructor () {
    this.skills = [];
    this.communities = [];
    this.members = [];
    this.rooms = [];
  }
}
