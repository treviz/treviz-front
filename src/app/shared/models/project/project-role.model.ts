import { Project } from './project.model';

export class ProjectRole {
  hash: string;
  name: string;
  permissions: string[];
  project: Project;
  global: boolean;
  defaultCreator: boolean;
  defaultMember: boolean;

  constructor () {
    this.permissions = [];
  }
}
