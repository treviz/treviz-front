import { Project } from './project.model';
import { User } from '../users/user.model';
import { ProjectJob } from './project-job.model';

/**
 * Created by Bastien on 16/03/2017.
 */

export class ProjectCandidacy {
  hash: string;
  project: Project;
  user: User;
  message: string;
  job: ProjectJob;
}
