import { Project } from './project.model';

export class ProjectDto {
  name: string;
  description: string;
  shortDescription: string;
  isVisible: boolean;
  isOpen: boolean;
  logo: File;
  logoUrl: string;
  skills: string[] = [];
  tags: string[] = [];
  communities: string[] = [];
  idea: string;

  public static fromProject (project: Project): ProjectDto {
    const dto = new ProjectDto();
    dto.name = project.name;
    dto.description = project.description;
    dto.shortDescription = project.shortDescription;
    dto.isVisible = project.isVisible;
    dto.isOpen = project.isOpen;
    dto.logoUrl = project.logoUrl;
    dto.skills = project.skills.map(skill => skill.name);
    dto.tags = project.tags.map(tag => tag.name);
    dto.idea = null != project.idea ? project.idea.hash : null;
    return dto;
  }
}
