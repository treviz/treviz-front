import { Project } from './project.model';
import { User } from '../users/user.model';
import { ProjectRole } from './project-role.model';

/**
 * Created by Bastien on 16/03/2017.
 */
export class ProjectMembership {
  hash: string;
  role: ProjectRole;
  project: Project;
  user: User;
}
