export class ProjectMembershipDto {
  user: string;
  role: string;
}
