/**
 * Created by Bastien on 16/03/2017.
 */

export class ProjectCandidacyDto {
  message: string;
  job: string;
}
