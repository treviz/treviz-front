import {ProjectJob} from './project-job.model';

export class ProjectJobDto {
  name: string;
  description: string;
  tasks: string[];
  tags: string[];
  skills: string[];
  holder: string;
  contact: string;

  public static fromProjectJob (job: ProjectJob): ProjectJobDto {
    const jobDto = new ProjectJobDto();
    jobDto.name = job.name;
    jobDto.description = job.description;
    jobDto.tasks = job.tasks.map(task => task.hash);
    jobDto.tags = job.tags.map(tag => tag.name);
    jobDto.skills = job.skills.map(skill => skill.name);
    jobDto.holder = null != job.holder ? job.holder.username : null;
    jobDto.contact = null != job.contact ? job.contact.username : null;
    return jobDto;
  }
}
