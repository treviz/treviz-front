export class ProjectNotificationPreferences {
    public onNewTask: boolean;
    public onAssignedTask: boolean;
    public onTaskSubmission: boolean;
    public onTaskApprovalOrRefusal: boolean;
    public onFork: boolean;
    public onNewDocument: boolean;
    public onCandidacy: boolean;
    public onInvitationAccepted: boolean;
    public onInvitationRejected: boolean;
    public onPost: boolean;
}
