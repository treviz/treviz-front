import { User } from '../users/user.model';

export class Document {
  id: number;
  name: string;
  description: string;
  license: string;
  date: string;
  hash: string;
  fileUrl: string;
  size: number;
  owner: User;
  extension: string;
}
