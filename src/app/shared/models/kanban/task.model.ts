import {Label} from './label.model';
import {User} from '../users/user.model';
import {ProjectJob} from '../project/project-job.model';
import {Column} from './column.model';

export class Task {
  hash: string;
  name: string;
  deadline: Date;
  archived: boolean;
  pendingApproval: boolean;
  position: number;
  description: string;
  labels: Label[];
  supervisor: User;
  assignee: User;
  voters: User[];
  job: ProjectJob;
  column: Column;

  constructor () {
    this.labels = [];
    this.voters = [];
  }
}
