import { User } from '../users/user.model';
import { Task } from './task.model';

export class Feedback {
  hash: string;
  feedback: string;
  praise: boolean;
  receiver: User;
  giver: User;
  task: Task;
}
