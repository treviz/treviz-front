import { Task } from './task.model';
import { Board } from './board.model';

export class Column {
  hash: string;
  name: string;
  tasks: Task[];
  board: Board;

  constructor () {
    this.tasks = [];
  }
}
