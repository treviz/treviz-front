import { Column } from './column.model';
import { Label } from './label.model';
import { Project } from '../project/project.model';

export class Board {
  hash: string;
  name: string;
  description: string;
  archived: boolean;
  columns: Column[];
  labels: Label[];
  project: Project;

  constructor () {
    this.columns = [];
    this.labels = [];
  }
}
