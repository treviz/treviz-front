/**
 * Created by Bastien on 16/03/2017.
 */

export class CommunityInvitationDto {
  user: string;
  message: string;
}
