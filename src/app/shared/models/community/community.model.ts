import { CommunityMembership } from './community-membership.model';
import { Project } from '../project/project.model';
import { Room } from '../chat/room.model';

export class Community {
  id: number;
  hash: string;
  name: string;
  description = '';
  logoUrl: string;
  isVisible = true;
  open = true;
  website: string;
  nbMembers: number;
  nbProjects?: number;
  backgroundImageUrl: string;

  members: CommunityMembership[];
  projects: Project[];

  rooms: Room[];

  // THIS IS JUST FOR FORM CONTROL
  isSelected = false;

  pendingNotification = 0;

  constructor () {
    this.members = [];
    this.projects = [];
    this.rooms = [];
  }
}
