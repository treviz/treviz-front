export class CommunityNotificationPreferences {
    public onPost: boolean;
    public onCandidacy: boolean;
    public onInvitationAccepted: boolean;
    public onInvitationRejected: boolean;
    public onNewBrainstorming: boolean;
    public onNewProject: boolean;
    public onNewDocument: boolean;
}
