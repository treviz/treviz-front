import { Community } from './community.model';

/**
 * Created by Bastien on 16/03/2017.
 */

export class CommunityRole {
  hash: string;
  name: string;
  community: Community;
  permissions: string[] = [];
  defaultMember: boolean|string;
  defaultCreator: boolean;
  global: boolean;
}
