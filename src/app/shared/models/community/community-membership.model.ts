import { User } from '../users/user.model';
import { Community } from './community.model';
import { CommunityRole } from './community-role.model';

/**
 * Created by Bastien on 16/03/2017.
 */
export class CommunityMembership {
  id: number;
  hash: string;
  role: CommunityRole;
  community: Community;
  user: User;
}
