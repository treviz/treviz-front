import { NotificationType } from './notifications-type.enum';
import { Notification } from './notification.model';

export default interface Subscription {
    componentName: string;
    notificationTypes: NotificationType[];
    priority: number;
    callback: (notification: Notification, handled: string[]) => any;
}

export enum SubscriptionPriority {
    CHAT_ROOM_PRIORITY = 3,
    CHAT_LIST_PRIORITY = 2,
    NOTIFICATION_DISPATCHER_PRIORITY = 1,
    NAVBAR_PRIORITY = 0 // The navbar should be the last component to receive the notifications, so it can see if they were already handled
}
