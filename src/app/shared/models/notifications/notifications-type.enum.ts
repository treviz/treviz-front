export enum NotificationType {
    CHAT_POST_MESSAGE = 'chat.message.created',
    CHAT_UPDATE_MESSAGE = 'chat.message.updated',
    CHAT_DELETE_MESSAGE = 'chat.message.deleted',
    CHAT_POST_ROOM = 'chat.room.created',
    CHAT_UPDATE_ROOM = 'chat.room.updated',
    CHAT_DELETE_ROOM = 'chat.room.deleted',
}
