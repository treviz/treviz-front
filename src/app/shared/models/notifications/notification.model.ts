import { NotificationType } from './notifications-type.enum';

/**
 * Created by huber on 05/08/2017.
 */
export class Notification {
  public type: NotificationType;
  public content;
}
