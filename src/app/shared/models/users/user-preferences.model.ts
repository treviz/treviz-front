export class UserPreferences {
    public lightTheme: boolean;
    public onCandidacyChange: boolean;
    public onDirectMessage: boolean;
    public onChatRoomMessage: boolean;
    public onInvitation: boolean;
}
