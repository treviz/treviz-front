import { User, UserRoles } from './user.model';

export class UserDto {
  username: string;
  firstName: string;
  lastName: string;
  password: string;
  description: string;
  email: string;
  skills: string[];
  interests: string[];
  avatarUrl: string;
  backgroundImageUrl: string;
  welcome: boolean;
  roles: UserRoles[];
  organization?: string;
  agreedWithTerms: boolean;

  static fromUser (user: User) {
    const userDto = new UserDto();
    userDto.username = user.username;
    userDto.firstName = user.firstName;
    userDto.lastName = user.lastName;
    user.description ? userDto.description = user.description : userDto.description = '';
    userDto.avatarUrl = user.avatarUrl;
    userDto.backgroundImageUrl = user.backgroundImageUrl;
    userDto.welcome = user.welcome;
    userDto.roles = user.roles;
    userDto.organization = user.organization;
    if (user.skills) {
      userDto.skills = user.skills.map(a => a.name);
    }
    if (user.interests) {
      userDto.interests = user.interests.map(a => a.name);
    }
    return userDto;
  }

  constructor () {
    this.skills = [];
    this.interests = [];
  }
}
