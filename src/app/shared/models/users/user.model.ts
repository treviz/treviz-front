import { Skill } from '../tags/skill.model';
import { Tag } from '../tags/tag.model';
import { ProjectMembership } from '../project/project-membership.model';
import { CommunityMembership } from '../community/community-membership.model';
import { Room } from '../chat/room.model';

export enum UserRoles {
  ROLE_USER = 'ROLE_USER',
  ROLE_ADMIN = 'ROLE_ADMIN'
}

export class User {
  username: string;
  firstName: string;
  lastName: string;
  password: string;
  description: string;
  email: string;
  skills: Skill[];
  interests: Tag[];
  avatarUrl: string;
  backgroundImageUrl: string;
  // These attributes are just used in the front-end.
  newsletter: boolean;
  projectMemberships: ProjectMembership[];
  communityMemberships: CommunityMembership[];
  rooms: Room[];
  welcome: boolean;
  agreedWithTerms?: boolean;
  roles: UserRoles[];
  organization?: string;

  constructor () {
    this.skills = [];
    this.interests = [];
    this.projectMemberships = [];
    this.communityMemberships = [];
    this.rooms = [];
    this.newsletter = false;
    this.roles = [];
    this.agreedWithTerms = false;
  }
}
