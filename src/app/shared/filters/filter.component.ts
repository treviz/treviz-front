import { Component, EventEmitter, Input, Output } from '@angular/core';
import { SkillService } from '../../core/services/skill.service';
import { TagService } from '../../core/services/tag.service';
import { debounceTime, distinctUntilChanged, take, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { FilterDialogComponent, FilterDialogInterface } from './filter-dialog.component';
import { AbstractTagSkillComponent } from '../abstract/abstract-tag-skill-component';

export interface SearchFilters {
  name: string;
  skills: string[];
  tags: string[];
}

@Component({
  selector: 'treviz-filter-component',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent extends AbstractTagSkillComponent {
  @Input() placeholder: string;
  @Input() displayFiltersOnLargeScreens = false;
  @Output() onFiltersUpdate = new EventEmitter<SearchFilters>();

  get nbActiveFilters (): string {
    const nbFilters = this.params.skills.length + this.params.tags.length;
    return nbFilters > 0 ? nbFilters.toString() : '';
  }

  public get smallScreen () { return window.innerWidth <= 768; }

  params: SearchFilters = {
    name: '',
    skills: [],
    tags: []
  };

  private searchStream = new Subject<string>();

  constructor (public dialog: MatDialog,
              protected skillService: SkillService,
              protected tagService: TagService) {
    super(skillService, tagService);
    this.searchStream
      .pipe(debounceTime(300), distinctUntilChanged(), takeUntil(this.destroyed$))
      .subscribe(val => {
        this.params.name = val;
        this.onFiltersUpdate.emit(this.params);
      });
  }

  setSkills (skills: string[]) {
    this.params.skills = skills;
    this.onFiltersUpdate.emit(this.params);
  }

  setTags (tags: string[]) {
    this.params.tags = tags;
    this.onFiltersUpdate.emit(this.params);
  }

  search (val: string) {
    this.searchStream.next(val);
  }

  openFilterDialog () {
    const data: FilterDialogInterface = {
      skills: this.skills,
      tags: this.tags,
      selectedSkills: this.params.skills,
      selectedTags: this.params.tags
    };

    this.dialog
      .open(FilterDialogComponent, { data, autoFocus: false, maxWidth: '100vw', maxHeight: '1OOvh' })
      .afterClosed()
      .pipe(take(1), takeUntil(this.destroyed$))
      .subscribe(
        params => {
          this.setSkills(params.skills);
          this.setTags(params.tags);
          this.onFiltersUpdate.emit(this.params);
        }
      );
  }
}
