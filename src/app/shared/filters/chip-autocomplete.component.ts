import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { map, startWith, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'treviz-chip-autocomplete',
  templateUrl: './chip-autocomplete.component.html',
  styleUrls: ['./chip-autocomplete.component.scss']
})
export class ChipAutocompleteComponent implements OnInit, OnDestroy {
  @Input() elements: string[];
  @Input() selectedElements: string[];
  @Input() label: string;
  @Input() placeholder: string;

  @Output() onSelectionUpdate = new EventEmitter<string[]>();

  formCtrl: FormControl;
  filteredElements: Observable<string[]>;

  private destroyed$ = new Subject<void>();

  ngOnInit (): void {
    this.formCtrl = new FormControl();
    this.filteredElements = this
      .formCtrl
      .valueChanges
      .pipe(
        startWith(null),
        map(value => this.filterElements(value)),
        takeUntil(this.destroyed$)
      );
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  add (elt: string) {
    if (elt !== '' && !this.selectedElements.some(value => value === elt)) {
      this.onSelectionUpdate.emit([...this.selectedElements, elt]);
    }
  }

  remove (elt: string) {
    const updatedArray = [];
    const nbSelectedElements = this.selectedElements.length;
    for (let i = 0; i < nbSelectedElements; i++) {
      if (this.selectedElements[i] !== elt) {
        updatedArray.push(this.selectedElements[i]);
      }
    }
    this.onSelectionUpdate.emit(updatedArray);
  }

  private filterElements (val: string) {
    return val ? this.elements.filter((element) => new RegExp(val, 'gi').test(element)) : this.elements;
  }
}
