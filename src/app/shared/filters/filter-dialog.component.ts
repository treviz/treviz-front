import { Component, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Subject } from 'rxjs';

export interface FilterDialogInterface {
  skills: string[];
  tags: string [];
  selectedSkills: string[];
  selectedTags: string[];
}

@Component({
  selector: 'treviz-filter-dialog',
  templateUrl: './filter-dialog.component.html',
  styleUrls: ['./filter-dialog.component.scss']
})
export class FilterDialogComponent implements OnDestroy {
  params = {
    tags: [],
    skills: []
  };

  skills: string[] = [];
  tags: string[] = [];

  submitted: boolean;
  private destroyed$ = new Subject<void>();

  constructor (public dialogRef: MatDialogRef<FilterDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: FilterDialogInterface) {
    this.tags = data.tags;
    this.skills = data.skills;
    this.params.tags = data.selectedTags;
    this.params.skills = data.selectedSkills;
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  setSkills (skills: string[]) {
    this.params.skills = skills;
  }

  setTags (tags: string[]) {
    this.params.tags = tags;
  }

  emptyFilters () {
    this.params = {
      tags: [],
      skills: []
    };
    this.onSubmit();
  }

  onSubmit () {
    this.dialogRef.close(this.params);
  }
}
