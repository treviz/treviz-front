
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Component, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CommunityCandidacyService } from '../../../core/services/community/community-candidacy.service';
import { ProjectCandidacyService } from '../../../core/services/project/project-candidacy.service';
import { ProjectCandidacyDto } from '../../models/project/project-candidacy.model.dto';
import { CommunityCandidacyDto } from '../../models/community/community-candidacy.model.dto';
import { Project } from '../../models/project/project.model';
import { Community } from '../../models/community/community.model';
import { ProjectJob } from '../../models/project/project-job.model';

@Component({
  selector: 'app-candidacy-dialog',
  templateUrl: 'candidacy-dialog.component.html',
  styleUrls: ['candidacy-dialog.component.scss']
})
export class CandidacyDialogComponent implements OnDestroy {
  public message: string;
  public submitted = false;
  private destroyed$ = new Subject<void>();
  private readonly project: Project;
  private readonly job: ProjectJob;
  private readonly community: Community;

  constructor (public dialogRef: MatDialogRef<CandidacyDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private projectCandidacyService: ProjectCandidacyService,
              private communityCandidacyService: CommunityCandidacyService) {
    this.project = data.project || null;
    this.job = data.job || null;
    this.community = data.community || null;
  }

  ngOnDestroy (): void {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  onSubmit () {
    this.submitted = true;
    if (null != this.project) {
      const candidacy = new ProjectCandidacyDto();
      candidacy.message = this.message;

      this.projectCandidacyService.postProjectCandidacy(this.project.hash, candidacy).pipe(
        takeUntil(this.destroyed$))
        .subscribe(
          data => this.dialogRef.close(data),
          () => this.dialogRef.close(false),
          () => this.submitted = false
        );
    } else if (null != this.job) {
      const candidacy = new ProjectCandidacyDto();
      candidacy.message = this.message;
      candidacy.job = this.job.hash;

      this.projectCandidacyService.postProjectCandidacy(this.job.project.hash, candidacy).pipe(
        takeUntil(this.destroyed$))
        .subscribe(
          data => this.dialogRef.close(data),
          () => this.dialogRef.close(false),
          () => this.submitted = false
        );
    } else if (null != this.community) {
      const candidacy = new CommunityCandidacyDto();
      candidacy.message = this.message;

      this.communityCandidacyService.postCommunityCandidacy(this.community.hash, candidacy).pipe(
        takeUntil(this.destroyed$))
        .subscribe(
          data => this.dialogRef.close(data),
          () => this.dialogRef.close(false),
          () => this.submitted = false
        );
    }
  }
}
