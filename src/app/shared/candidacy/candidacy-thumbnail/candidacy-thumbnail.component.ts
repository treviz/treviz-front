
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { ProjectCandidacy } from '../../models/project/project-candidacy.model';
import { ProjectCandidacyService } from '../../../core/services/project/project-candidacy.service';
import { CommunityCandidacy } from '../../models/community/community-candidacy.model';
import { CommunityCandidacyService } from '../../../core/services/community/community-candidacy.service';

@Component({
  selector: 'app-candidacy-thumbnail',
  templateUrl: 'candidacy-thumbnail.component.html',
  styleUrls: ['candidacy-thumbnail.component.scss']
})
export class CandidacyThumbnailComponent implements OnDestroy {
  @Input() projectCandidacy: ProjectCandidacy;

  @Input() communityCandidacy: CommunityCandidacy;

  @Input() canApprove = false;

  @Input() canDiscard = false;

  @Output() onProjectCandidacyRemoval = new EventEmitter<ProjectCandidacy>();
  @Output() onProjectCandidacyAccepted = new EventEmitter<ProjectCandidacy>();

  @Output() onCommunityCandidacyRemoval = new EventEmitter<CommunityCandidacy>();
  @Output() onCommunityCandidacyAccepted = new EventEmitter<CommunityCandidacy>();

  public submitted = false;
  private destroyed$ = new Subject<void>();

  constructor (private projectCandidacyService: ProjectCandidacyService,
              private communityCandidacyService: CommunityCandidacyService) {
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  /*
   * Candidacy management
   */
  acceptCandidacy (): void {
    this.submitted = true;
    if (this.projectCandidacy) {
      this.projectCandidacyService.acceptProjectCandidacy(this.projectCandidacy.hash).pipe(
        takeUntil(this.destroyed$))
        .subscribe(
          () => this.onProjectCandidacyAccepted.emit(this.projectCandidacy),
          err => {
            console.log(err);
            this.submitted = true;
          }
        );
    } else if (this.communityCandidacy) {
      this.communityCandidacyService.acceptCommunityCandidacy(this.communityCandidacy.hash).pipe(
        takeUntil(this.destroyed$))
        .subscribe(
          () => this.onCommunityCandidacyAccepted.emit(this.communityCandidacy),
          err => {
            console.log(err);
            this.submitted = true;
          }
        );
    }
  }

  removeCandidacy (): void {
    this.submitted = true;
    if (this.projectCandidacy) {
      this.projectCandidacyService.deleteProjectCandidacy(this.projectCandidacy.hash).pipe(
        takeUntil(this.destroyed$))
        .subscribe(
          () => this.onProjectCandidacyRemoval.emit(this.projectCandidacy),
          err => {
            console.log(err);
            this.submitted = true;
          }
        );
    } else if (this.communityCandidacy) {
      this.communityCandidacyService.deleteCommunityCandidacy(this.communityCandidacy.hash).pipe(
        takeUntil(this.destroyed$))
        .subscribe(
          () => this.onCommunityCandidacyRemoval.emit(this.communityCandidacy),
          err => {
            console.log(err);
            this.submitted = true;
          }
        );
    }
  }
}
