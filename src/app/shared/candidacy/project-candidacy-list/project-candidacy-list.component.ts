import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ProjectCandidacy } from '../../models/project/project-candidacy.model';

@Component({
  selector: 'app-project-candidacy-list',
  templateUrl: 'project-candidacy-list.component.html',
  styleUrls: ['project-candidacy-list.component.scss']
})
export class ProjectCandidacyListComponent {
    @Input() candidacies: ProjectCandidacy[] = [];
    @Input() canApprove = false;
    @Output() onCandidacyAccepted = new EventEmitter<ProjectCandidacy>();
    @Output() onCandidacyRefused = new EventEmitter<ProjectCandidacy>();

    public handleCandidacyRemoved (candidacy: ProjectCandidacy) {
      this.onCandidacyRefused.emit(candidacy);
    }

    public handleCandidacyAccepted (candidacy: ProjectCandidacy) {
      this.onCandidacyAccepted.emit(candidacy);
    }
}
