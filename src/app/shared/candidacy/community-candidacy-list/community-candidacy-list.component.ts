import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CommunityCandidacy } from '../../models/community/community-candidacy.model';

@Component({
  selector: 'app-community-candidacy-list',
  templateUrl: 'community-candidacy-list.component.html',
  styleUrls: ['community-candidacy-list.component.scss']
})
export class CommunityCandidacyListComponent {
    @Input() candidacies: CommunityCandidacy[] = [];
    @Input() canApprove = false;
    @Output() onCandidacyAccepted = new EventEmitter<CommunityCandidacy>();
    @Output() onCandidacyRefused = new EventEmitter<CommunityCandidacy>();

    public handleCandidacyRemoved (candidacy: CommunityCandidacy) {
      this.onCandidacyRefused.emit(candidacy);
    }

    public handleCandidacyAccepted (candidacy: CommunityCandidacy) {
      this.onCandidacyAccepted.emit(candidacy);
    }
}
