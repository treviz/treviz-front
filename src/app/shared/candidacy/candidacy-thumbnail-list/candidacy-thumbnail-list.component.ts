import { finalize, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { ProjectCandidacy } from '../../models/project/project-candidacy.model';
import { ProjectCandidacyService } from '../../../core/services/project/project-candidacy.service';
import { ProjectJob } from '../../models/project/project-job.model';
import { User } from '../../models/users/user.model';
import { Project } from '../../models/project/project.model';
import { CommunityCandidacy } from '../../models/community/community-candidacy.model';
import { CommunityCandidacyService } from '../../../core/services/community/community-candidacy.service';
import { Community } from '../../models/community/community.model';

@Component({
  selector: 'app-candidacy-thumbnail-list',
  templateUrl: 'candidacy-thumbnail-list.component.html',
  styleUrls: ['candidacy-thumbnail-list.component.scss']
})
export class CandidacyThumbnailListComponent implements OnInit, OnDestroy {
  @Input() job: ProjectJob;

  @Input() project: Project;

  @Input() user: User;

  @Input() community: Community;

  @Input() canApprove = false;

  @Input() canDiscard = false;

  @Input() displayNoneMessage = true;

  @Output() onCandidacyAccepted = new EventEmitter<ProjectCandidacy|CommunityCandidacy>();

  @Output() onCandidacyRefused = new EventEmitter<ProjectCandidacy|CommunityCandidacy>();

  projectCandidacies: ProjectCandidacy[] = [];
  projectCandidaciesFetched = false;
  communityCandidacies: CommunityCandidacy[] = [];
  communityCandidaciesFetched = false;

  private destroyed$ = new Subject<void>();

  constructor (private projectCandidacyService: ProjectCandidacyService,
              private communityCandidacyService: CommunityCandidacyService) { }

  ngOnInit () {
    const projectOptions = {
      job: '',
      project: '',
      user: ''
    };

    const communityOptions = {
      community: '',
      user: ''
    };

    if (this.job) {
      projectOptions.job = this.job.hash;
    }
    if (this.project) {
      projectOptions.project = this.project.hash;
    }
    if (this.user) {
      projectOptions.user = this.user.username;
      communityOptions.user = this.user.username;
    }
    if (this.community) {
      communityOptions.community = this.community.hash;
    }

    this.projectCandidacyService.getProjectCandidacies(projectOptions).pipe(
      finalize(() => this.projectCandidaciesFetched = true),
      takeUntil(this.destroyed$))
      .subscribe(
        data => this.projectCandidacies = data,
        err => console.log(err)
      );

    this.communityCandidacyService.getCommunityCandidacies(communityOptions)
      .pipe(
        finalize(() => this.communityCandidaciesFetched = true),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        data => {
          this.communityCandidaciesFetched = true;
          this.communityCandidacies = data;
        },
        err => console.log(err)
      );
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  manageProjectCandidacy (candidacy: ProjectCandidacy) {
    this.onCandidacyAccepted.emit(candidacy);
    this
      .projectCandidacies
      .splice(this.projectCandidacies.findIndex(indexedCandidacy => indexedCandidacy.hash === candidacy.hash), 1);
  }

  manageCommunityCandidacy (candidacy: CommunityCandidacy) {
    this.onCandidacyAccepted.emit(candidacy);
    this.communityCandidacies
      .splice(
        this.communityCandidacies.findIndex(indexedCandidacy => indexedCandidacy.hash === candidacy.hash),
        1
      );
  }
}
