import { OnInit, Component, ViewChild, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ChatRoomCreateDialogComponent } from 'app/shared/dialogs/chat-room-create-dialog/chat-room-create-dialog.component';
import { ChatRoomService } from '../../core/services/chat/chat-room.service';
import { Room } from '../../shared/models/chat/room.model';
import { UserStateService } from '../../core/services/user-state.service';
import { Project } from '../../shared/models/project/project.model';
import { Community } from '../../shared/models/community/community.model';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { MessageDialogComponent } from 'app/shared/dialogs/message-dialog/message-dialog.component';
import { distinctUntilChanged, map, startWith, switchMap, takeUntil } from 'rxjs/operators';
import { ChatStateService } from '../../core/services/chat-state.service';

interface ChatServers {
  projects: Array<Project>;
  communities: Array<Community>;
}

interface Server {
  project?: Project;
  community?: Community;
}

/**
 * Created by huber on 11/07/2017.
 */
@Component({
  selector: 'app-chat',
  templateUrl: 'chat-list.component.html',
  styleUrls: ['chat-list.component.scss']
})
export class ChatListComponent implements OnInit, OnDestroy {
  // Handles the list of all servers
  chatServers: ChatServers = {
    projects: [],
    communities: []
  };

  notifications: {
    unlinked: number;
    projects: Map<string, number>;
    communities: Map<string, number>;
  };

  roomsInWhichMember: Observable<Room[]>;
  roomsInWhichNotMember: Observable<Room[]>;
  selectedServer = new BehaviorSubject<Server>({});
  selectedRoom: Room;

  @ViewChild('chatlist', { static: false })
  chatlist: MatSidenav;

  public get mode () { return window.innerWidth <= 768 ? 'over' : 'side'; }

  private destroyed$ = new Subject<void>();

  constructor (private chatRoomService: ChatRoomService,
              public currentUserService: UserStateService,
              public dialog: MatDialog,
              private stateService: ChatStateService,
              private cdr: ChangeDetectorRef) {}

  /**
   * At component initiation, remove the notification for new messages.
   * Fetch rooms that are not linked to any project or community.
   */
  ngOnInit () {
    this
      .currentUserService
      .state
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        user => {
          this.chatServers = {
            projects: user.projectMemberships.map((membership) => membership.project),
            communities: user.communityMemberships.map((membership) => membership.community)
          };
        }
      );

    this.observeState();
  }

  /**
   * Unsubscribe from observables at component destruction.
   */
  ngOnDestroy () {
    this.deactivateRoom();
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  observeState () {
    const user = this
      .currentUserService
      .getCurrentUser();

    this
      .stateService
      .state
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        state => {
          this.notifications = state.chat.notifications;
        }
      );

    this.roomsInWhichMember = this
      .selectedServer
      .pipe(
        distinctUntilChanged(),
        switchMap(server => {
          return this.stateService
            .state
            .pipe(
              map(state => {
                let rooms = state
                  .chat
                  .rooms
                  .filter((room) => room.users.some((participant) => user.username === participant.username));

                if (null != server.project) {
                  rooms = rooms.filter((room) => room.project?.hash === server.project.hash);
                } else if (null != server.community) {
                  rooms = rooms.filter((room) => room.community?.hash === server.community.hash);
                } else {
                  rooms = rooms.filter((room) => null == room.project && null == room.community);
                }

                return rooms;
              }),
              takeUntil(this.destroyed$)
            );
        }),
        takeUntil(this.destroyed$)
      );

    this.roomsInWhichNotMember = this
      .selectedServer
      .pipe(
        distinctUntilChanged(),
        switchMap(server => {
          if (null != server.project || null != server.community) {
            const options = {
              project: server.project?.hash ?? '',
              community: server.community?.hash ?? ''
            };
            return this
              .chatRoomService
              .getRooms(options)
              .pipe(
                takeUntil(this.destroyed$),
                map(rooms => rooms.filter(room => !room.users.some((participant) => participant.username === user.username))),
                startWith([])); // Allows us to reset the room list before the new ones are fetched
          } else {
            return of([]);
          }
        }),
        takeUntil(this.destroyed$)
      );
  }

  /**
   * Display the rooms of a project, community, or those linked to none.
   * Unsubscribe from previous observables, and clears the current selected room.
   * @param {Project} project
   * @param {Community} community
   */
  showRooms ({ project = null, community = null }: Server): void {
    const server = this.selectedServer.getValue();
    if (server.project?.hash !== project?.hash || server.community?.hash !== community?.hash) {
      this.selectedServer.next({ project, community });
      this.selectedRoom = null;
      this.cdr.detectChanges();
    }
  }

  goToRoom (room: Room) {
    this.deactivateRoom();
    this.selectedRoom = room;
    this.stateService.setActive(room);
    this.cdr.detectChanges();
    if (window.innerWidth <= 768) {
      this.chatlist.close();
    }
  }

  deactivateRoom () {
    if (null != this.selectedRoom) {
      this.stateService.setInactive(this.selectedRoom);
    }
  }

  /**
   * Opens the Chat room creation dialog.
   * When the dialog is closed, adds the created chat room to the list.
   */
  openCreateDialog () {
    const config = new MatDialogConfig();
    config.data = {
      project: this.selectedServer.value.project?.hash ?? '',
      community: this.selectedServer.value.community?.hash ?? ''
    };
    config.maxWidth = '100vw';
    config.maxHeight = '100vh';

    const dialogRef = this.dialog.open(ChatRoomCreateDialogComponent, config);
    dialogRef.afterClosed()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(
        data => {
          if (data != null) {
            if (data) {
              this.goToRoom(data);
            } else {
              this.displayErrorDialog('An error occurred while creating the chat room');
            }
          }
        },
        err => console.log(err)
      );
  }

  displayErrorDialog (message: string) {
    const config = new MatDialogConfig();
    config.data = {
      message,
      failure: true,
      actionRequired: true
    };

    this.dialog.open(MessageDialogComponent, config);
  }
}
