import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Message } from '../../shared/models/chat/message.model';
import { ChatMessageService } from '../../core/services/chat/chat-message.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { takeUntil } from 'rxjs/operators';
import { ChatMessageEditDialogComponent } from '../chat-message-edit-dialog/chat-message-edit-dialog.component';
import { ConfirmMessageModel } from '../../shared/models/confirm-message.model';
import { AbstractProjectConfirmComponent } from '../../shared/abstract/abstract-project-confirm-component';

@Component({
  moduleId: module.id,
  selector: 'app-chat-message',
  templateUrl: 'chat-message.component.html',
  styleUrls: ['chat-message.component.scss']
})
export class ChatMessageComponent extends AbstractProjectConfirmComponent implements OnInit {
  @Input() message: Message;

  @Output() onMessageUpdate = new EventEmitter<Message>();

  @Output() onMessageDelete = new EventEmitter<Message>();

  submitted = false;
  canEdit: boolean;

  constructor (private messageService: ChatMessageService,
              public dialog: MatDialog) {
    super(dialog);
  }

  ngOnInit (): void {
    this.canEdit = (this.message.owner.username === localStorage.getItem('username'));
  }

  openEditDialog () {
    const config: MatDialogConfig = {
      maxWidth: '100vw !important',
      data: this.message
    };

    const dialog = this.dialog.open(ChatMessageEditDialogComponent, config);
    dialog
      .afterClosed()
      .pipe(
        takeUntil(this.destroyed$)
      )
      .subscribe(
        (result) => {
          switch (result) {
            case false:
              this.displayErrorDialog('An error occurred while updating the message.');
              break;
            case null:
            case undefined:
              break;
            default:
              this.onMessageUpdate.emit(result);
              break;
          }
        }
      );
  }

  deleteMessage () {
    this.confirmAction(ConfirmMessageModel.MESSAGE_DELETE,
      () => {
        this.submitted = true;
        this.messageService
          .deleteMessage(this.message.hash)
          .pipe(takeUntil(this.destroyed$))
          .subscribe(
            () => this.onMessageDelete.emit(this.message)
          );
      }
    );
  }
}
