import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ChatListComponent } from './chat-list/chat-list.component';
import { ChatRoutingModule } from './chat-routing.module';
import { ChatRoomDetailDialogComponent } from './chat-room-detail-dialog/chat-room-detail-dialog.component';
import { ChatRoomComponent } from './chat-room/chat-room.component';
import { ChatMessageComponent } from './chat-message/chat-message.component';
import { ChatMessageEditDialogComponent } from './chat-message-edit-dialog/chat-message-edit-dialog.component';

@NgModule({
  imports: [
    SharedModule,
    ChatRoutingModule,
    MatSidenavModule,
    MatToolbarModule
  ],
  declarations: [
    ChatListComponent,
    ChatMessageComponent,
    ChatRoomComponent,
    ChatRoomDetailDialogComponent,
    ChatMessageEditDialogComponent
  ],
  exports: [],
  providers: []
})
export class ChatModule { }
