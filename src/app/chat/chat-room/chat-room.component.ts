import { finalize, takeUntil } from 'rxjs/operators';
import {
  AfterViewChecked,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  ViewChild
} from '@angular/core';
import { Room } from '../../shared/models/chat/room.model';
import { MessageDto } from '../../shared/models/chat/message.model.dto';
import { MatSidenav } from '@angular/material/sidenav';
import { MatDialog } from '@angular/material/dialog';
import { NotificationService } from '../../core/services/notification.service';
import { ChatRoomDetailDialogComponent } from '../chat-room-detail-dialog/chat-room-detail-dialog.component';
import { ChatMessageService } from '../../core/services/chat/chat-message.service';
import { UserStateService } from '../../core/services/user-state.service';
import { NotificationType } from 'app/shared/models/notifications/notifications-type.enum';
import Subscription, { SubscriptionPriority } from 'app/shared/models/notifications/subscription.model';
import { Notification } from 'app/shared/models/notifications/notification.model';
import { Message } from '../../shared/models/chat/message.model';
import { ChatStateService } from '../../core/services/chat-state.service';
import { ConfirmDialogComponent } from '../../shared/dialogs/confirm-dialog/confirm-dialog.component';
import { BehaviorSubject, Subject } from 'rxjs';
import { ChatRoomService } from '../../core/services/chat/chat-room.service';
import { ConfirmMessageModel } from '../../shared/models/confirm-message.model';

/**
 * Created by huber on 22/07/2017.
 */
@Component({
  selector: 'app-chat-room',
  templateUrl: 'chat-room.component.html',
  styleUrls: ['chat-room.component.scss']
})
export class ChatRoomComponent implements OnChanges, OnDestroy, AfterViewChecked {
    @Input()
    room: Room;

    @Input()
    chatlist: MatSidenav;

    /**
     * We need to create an observable entity to make the messages update in real time.
     */
    messages = new BehaviorSubject<Message[]>([]);

    firstLoad = true;
    messageToPost = new MessageDto();
    submitted = false;
    noMoreMessagesToLoad = false;
    loadingMessages = false;
    newMessages = true;
    isRoomMember: boolean;

    @ViewChild('chatScrollMessages', { static: false })
    private chatMessagesContainer: ElementRef;

    private destroyed$ = new Subject<void>();

    constructor (private chatMessageService: ChatMessageService,
                private notificationService: NotificationService,
                private currentUserService: UserStateService,
                private changeDetector: ChangeDetectorRef,
                public stateService: ChatStateService,
                private chatRoomService: ChatRoomService,
                public dialog: MatDialog) { }

    /**
     * When the input room changes, checks if the user is member of the room.
     * If he or she is, fetch the messages.
     */
    ngOnChanges () {
      this.clear();

      this
        .stateService
        .state
        .pipe(takeUntil(this.destroyed$))
        .subscribe(
          () => {
            this.messages.next(this.room?.messages ?? []);
            this.changeDetector.detectChanges();
          }
        );

      if (this.room != null) {
        this.isRoomMember = this.room.users.some(user => user.username === localStorage.getItem('username'));
        if (this.isRoomMember) {
          this.loadMessages();
        } else {
          this.noMoreMessagesToLoad = true;
          this.firstLoad = false;
        }

        const onMessage = (msg: Notification) => {
          if (msg.content.room_hash === this.room.hash) {
            this.newMessages = true;
            this.scrollToBottom();
          }
        };

        const subscription: Subscription = {
          componentName: ChatRoomComponent.name,
          notificationTypes: [
            NotificationType.CHAT_POST_MESSAGE,
            NotificationType.CHAT_DELETE_MESSAGE,
            NotificationType.CHAT_UPDATE_MESSAGE
          ],
          priority: SubscriptionPriority.CHAT_ROOM_PRIORITY,
          callback: onMessage
        };

        this.notificationService.subscribe(subscription);
      }
    }

    ngOnDestroy () {
      this.notificationService.unsubscribe(ChatRoomComponent.name);
      this.destroyed$.next();
      this.destroyed$.complete();
    }

    ngAfterViewChecked () {
      if (this.newMessages && !this.loadingMessages) {
        this.scrollToBottom();
        this.newMessages = false;
      }
    }

    loadMessages () {
      this.loadingMessages = true;
      this.chatMessageService
        .getMessages(this.room.hash, { limit: 20, offset: this.room.messages?.length ?? 0 })
        .pipe(
          finalize(() => {
            this.firstLoad = false;
            this.loadingMessages = false;
          }),
          takeUntil(this.destroyed$)
        )
        .subscribe(
          data => {
            this.noMoreMessagesToLoad = (data.length < 20);
            if (data.length > 0) {
              this.stateService.addPreviousMessages(data);
            }
          },
          err => console.log(err)
        );
    }

    scrollToBottom (): void {
      try {
        if (null != this.chatMessagesContainer) {
          this.chatMessagesContainer.nativeElement.scrollTop = this.chatMessagesContainer.nativeElement.scrollHeight;
        }
      } catch (err) {
        console.log(err);
      }
    }

    sendMessage () {
      this.submitted = true;

      // Change \n with \n\n so that markdown module interprets this as a line break.
      this.messageToPost.text = this.messageToPost.text.split('\n').join('\n\n');
      this.chatMessageService
        .postMessage(this.room.hash, this.messageToPost)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(
          data => {
            // Add the message to the room.
            this.stateService.addMessage(data, false);

            if (data.room_hash === this.room.hash) {
              this.newMessages = true;
              this.messageToPost = new MessageDto();
            }
          },
          err => console.log(err),
          () => this.submitted = false
        );
    }

    private clear (): void {
      this.messages.next([]);
      this.notificationService.unsubscribe(ChatRoomComponent.name);
      this.noMoreMessagesToLoad = false;
      this.loadingMessages = false;
      this.newMessages = true;
      this.messageToPost = new MessageDto();
      this.submitted = false;
    }

    openDialog () {
      const dialogRef = this.dialog.open(ChatRoomDetailDialogComponent, {
        maxWidth: '100vw',
        maxHeight: '100vh'
      });
      dialogRef.componentInstance.room = this.stateService.rooms.find((el) => el.hash === this.room.hash);
      dialogRef
        .afterClosed()
        .pipe(takeUntil(this.destroyed$))
        .subscribe(
          data => {
            if (null != data) {
              if (data) {
                this.stateService.updateRoom(data);
              } else {
                this.confirmRoomDeletion();
              }
            }
          },
          err => console.log(err)
        );
    }

    private confirmRoomDeletion () {
      this.dialog.open(ConfirmDialogComponent, {
        data: ConfirmMessageModel.CHAT_ROOM
      })
        .afterClosed()
        .pipe(takeUntil(this.destroyed$))
        .subscribe(
          ok => {
            if (ok) {
              this.chatRoomService.deleteRoom(this.room.hash)
                .pipe(takeUntil(this.destroyed$))
                .subscribe(
                  () => this.stateService.deleteRoom(this.room),
                  err => console.log(err)
                );
            }
          },
          err => console.log(err)
        );
    }

    updateMessage (message: Message) {
      this.stateService.updateMessage(message);
    }

    deleteMessage (message: Message) {
      this.stateService.deleteMessage(message);
    }
}
