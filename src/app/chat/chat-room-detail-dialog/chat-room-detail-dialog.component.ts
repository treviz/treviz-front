import { distinctUntilChanged, debounceTime, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Component, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Room } from '../../shared/models/chat/room.model';
import { User } from '../../shared/models/users/user.model';
import { UserService } from '../../core/services/user.service';
import { ChatRoomService } from '../../core/services/chat/chat-room.service';
import { UserStateService } from '../../core/services/user-state.service';

@Component({
  selector: 'app-chat-room-detail',
  templateUrl: 'chat-room-detail-dialog.component.html',
  styleUrls: ['chat-room-detail-dialog.component.scss']
})
export class ChatRoomDetailDialogComponent implements OnDestroy {
  public room: Room;

  public filteredUsers: User[];
  public userCtrl: FormControl;
  public usersToInvite: User[] = [];

  public isSubmitted = false;

  private destroyed$ = new Subject<void>();

  constructor (public dialogRef: MatDialogRef<ChatRoomDetailDialogComponent>,
              private chatRoomService: ChatRoomService,
              private currentUserService: UserStateService,
              private userService: UserService) {
    this.userCtrl = new FormControl();
    this.userCtrl.valueChanges.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      takeUntil(this.destroyed$))
      .subscribe(
        name => {
          this.filteredUsers = [];
          if (name != null && name.length >= 2) {
            this.userService.getUsers({ name: name })
              .subscribe(
                data => this.filteredUsers = data,
                err => console.log(err)
              );
          }
        }
      );
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  addUser (user: User): void {
    if (user != null) {
      this.usersToInvite.push(user);
    }
  }

  addUserFromUsername (name): void {
    const user = this.filteredUsers.filter((s) => new RegExp(name, 'gi').test(s.username))[0];
    this.addUser(user);
  }

  removeUser (user: User): void {
    this.usersToInvite.splice(this.usersToInvite.indexOf(user), 1);
  }

  inviteUser () {
    this.isSubmitted = true;
    this.chatRoomService.inviteUsersToRoom(this.usersToInvite, this.room.hash).pipe(
      takeUntil(this.destroyed$))
      .subscribe(
        data => this.dialogRef.close(data),
        err => console.log(err),
        () => this.isSubmitted = false
      );
  }

  leaveRoom () {
    this.chatRoomService.removeUserFromRoom([this.currentUserService.getCurrentUser()], this.room.hash).pipe(
      takeUntil(this.destroyed$))
      .subscribe(
        data => this.dialogRef.close(data),
        err => console.log(err),
        () => this.isSubmitted = false
      );
  }

  deleteRoom () {
    this.dialogRef.close(false);
  }
}
