import { Component, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Message } from '../../shared/models/chat/message.model';
import { MessageDto } from '../../shared/models/chat/message.model.dto';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ChatMessageService } from '../../core/services/chat/chat-message.service';

@Component({
  selector: 'treviz-chat-message-edit-dialog',
  templateUrl: './chat-message-edit-dialog.component.html',
  styleUrls: ['./chat-message-edit-dialog.component.scss']
})
export class ChatMessageEditDialogComponent implements OnDestroy {
  messageDto: MessageDto;
  submitted = false;
  private destroyed$ = new Subject<void>();

  constructor (private dialogRef: MatDialogRef<ChatMessageEditDialogComponent>,
              @Inject(MAT_DIALOG_DATA) private data: Message,
              private messageService: ChatMessageService) {
    this.messageDto = MessageDto.from(data);
  }

  ngOnDestroy () {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  onSubmit () {
    this.messageService.putMessage(this.data.hash, this.messageDto)
      .pipe(
        takeUntil(this.destroyed$)
      )
      .subscribe(
        (updatedPost) => this.dialogRef.close(updatedPost),
        () => this.dialogRef.close(false)
      );
  }

  close () {
    this.dialogRef.close();
  }
}
