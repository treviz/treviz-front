import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatMessageEditDialogComponent } from './chat-message-edit-dialog.component';

describe('ChatMessageEditDialogComponent', () => {
  let component: ChatMessageEditDialogComponent;
  let fixture: ComponentFixture<ChatMessageEditDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatMessageEditDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatMessageEditDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
