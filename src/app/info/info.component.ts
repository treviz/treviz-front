import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
/**
 * Created by Bastien on 19/03/2017.
 */

@Component({
  selector: 'info',
  templateUrl: './info.component.html',
  styleUrls: ['info.component.css']
})
export class InfoDialogComponent {
  constructor (public dialogRef: MatDialogRef<InfoDialogComponent>) {}
}
