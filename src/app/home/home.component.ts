
import { takeUntil, finalize } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { TaskService } from '../core/services/kanban/task.service';
import { ProjectJobService } from '../core/services/project/project-job.service';
import { Task } from '../shared/models/kanban/task.model';
import { ProjectJob } from '../shared/models/project/project-job.model';
import { UserStateService } from '../core/services/user-state.service';
import { MatDialog } from '@angular/material/dialog';
import { WelcomeHomeDialogComponent } from './welcome-home-dialog.component';

/**
 * Created by huber on 18/02/2017.
 */
@Component({
  selector: 'app-home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  tasks: Task[] = [];
  tasksFetched = false;

  supervisedTasks: Task[] = [];
  supervisedTasksLoaded = false;

  jobs: ProjectJob[] = [];
  jobsFetched = false;

  private destroyed$ = new Subject<void>();

  constructor (private taskService: TaskService,
              private jobService: ProjectJobService,
              public currentUserService: UserStateService,
              public dialog: MatDialog) { }

  ngOnInit (): void {
    this.taskService
      .getTasks({ assignee: localStorage.getItem('username') })
      .pipe(
        finalize(() => {
          this.tasksFetched = true;
          this.displayWelcomeDialog();
        }),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        data => this.tasks = data,
        err => console.log(err)
      );

    this.taskService
      .getTasks({ supervisor: localStorage.getItem('username') })
      .pipe(
        finalize(() => {
          this.supervisedTasksLoaded = true;
          this.displayWelcomeDialog();
        }),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        data => this.supervisedTasks = data,
        err => console.log(err)
      );

    this.jobService
      .getJobs({ holder: localStorage.getItem('username') })
      .pipe(
        finalize(() => {
          this.jobsFetched = true;
          this.displayWelcomeDialog();
        }),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        data => this.jobs = data,
        err => console.log(err)
      );
  }

  ngOnDestroy (): void {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  /**
   * Updates a task, or removes it if it was archived.
   * @param task Task to update
   */
  updateTask (task: Task): void {
    if (task.archived) {
      this.deleteTask(task);
    } else {
      this.supervisedTasks[this.supervisedTasks.findIndex(indexedTask => indexedTask.hash === task.hash)] = task;
    }
  }

  /**
   * Removes a task from the display.
   * @param task Task to remove from the display.
   */
  deleteTask (task: Task): void {
    this.supervisedTasks.splice(this.supervisedTasks.findIndex(indexedTask => indexedTask.hash === task.hash), 1);
  }

  displayWelcomeDialog () {
    if (!localStorage.getItem('welcome') &&
      this.currentUserService.getCurrentUser().welcome &&
      this.jobsFetched && this.supervisedTasksLoaded && this.tasksFetched) {
      this.dialog.open(WelcomeHomeDialogComponent, {
        maxWidth: '100vw',
        maxHeight: '100vh'
      });
    }
  }
}
