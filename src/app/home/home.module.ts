import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { HomeComponent } from './home.component';
import { WelcomeHomeDialogComponent } from './welcome-home-dialog.component';
import { HomeRoutingModule } from './home-routing.module';

@NgModule({
  imports: [
    HomeRoutingModule,
    SharedModule
  ],
  declarations: [
    HomeComponent,
    WelcomeHomeDialogComponent
  ],
  exports: [],
  providers: []
})
export class HomeModule {}
