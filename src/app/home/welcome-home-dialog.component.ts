
import { finalize, takeUntil } from 'rxjs/operators';
import { Component, OnDestroy } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { UserService } from 'app/core/services/user.service';
import { UserStateService } from 'app/core/services/user-state.service';
import { UserDto } from 'app/shared/models/users/user.model.dto';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-welcome-dialog',
  templateUrl: 'welcome-home-dialog.component.html',
  styleUrls: ['welcome-home-dialog.component.scss']
})
export class WelcomeHomeDialogComponent implements OnDestroy {
    public displayOnStartup = true;
    public submitted = false;
    public destroyed$ = new Subject<void>();

    constructor (public dialogRef: MatDialogRef<WelcomeHomeDialogComponent>,
                private currentUserService: UserStateService,
                private userService: UserService) {
      localStorage.setItem('welcome', 'true');
    }

    ngOnDestroy () {
      this.destroyed$.next();
      this.destroyed$.complete();
    }

    getUserFullName () {
      const user = this.currentUserService.getCurrentUser();
      return `${user.firstName} ${user.lastName}`;
    }

    onSubmit () {
      const user = this.currentUserService.getCurrentUser();
      const userDto = UserDto.fromUser(user);
      userDto.welcome = this.displayOnStartup;
      if (!this.displayOnStartup) {
        this.submitted = true;
        this.userService.putUser(user.username, userDto).pipe(
          finalize(() => this.submitted = false),
          takeUntil(this.destroyed$)
        )
          .subscribe(
            data => this.dialogRef.close(data),
            () => this.dialogRef.close(false)
          );
      } else {
        this.dialogRef.close();
      }
    }
}
