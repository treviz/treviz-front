#!/bin/sh

echo "TREVIZ FRONTEND"
echo "==============="
echo ""

export API_HOST=${API_HOST:="api.treviz.org"}
export NOTIFICATION_API_HOST=${NOTIFICATION_API_HOST:="events.treviz.org"}

ENVIRONMENT='$API_HOST:$NOTIFICATION_API_HOST'

echo "Using configuration:"
echo "  * API URL: $API_HOST"
echo "  * Notification URL: $NOTIFICATION_API_HOST"
echo ""

echo "Applying configuration to built files..."
echo ""

FILES=$(find . -name "main.*.js")
for file in $FILES
do
  echo "Replacing values in $file"
  touch "$file.tmp"
  OLD_HASH=$(sha1sum $file | cut -d' ' -f1)
  envsubst "$ENVIRONMENT" <"$file" >"$file.tmp"
  mv "$file.tmp" "$file"
  NEW_HASH=$(sha1sum $file | cut -d' ' -f1)
  echo "Updating hash table for Service worker..."
  sed -ri -e "s!$OLD_HASH!$NEW_HASH!g" ./ngsw.json
done

echo "Environment variables have been correctly set, bootstrapping application"

nginx -g "daemon off;"
