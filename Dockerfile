FROM node:12.22.9-buster-slim as builder
WORKDIR /var/www/app

ARG LANG

COPY ./package*.json ./
RUN npm install
COPY . ./
RUN /bin/sh build.sh

FROM nginx:alpine
WORKDIR /usr/share/nginx/html
COPY --from=builder /var/www/app/dist ./
COPY ./bootstrap.sh ./
COPY ./webserver-conf/nginx.conf /etc/nginx/
RUN rm /etc/nginx/conf.d/default.conf
COPY ./webserver-conf/treviz-front.conf /etc/nginx/conf.d/default.conf

CMD ["/bin/sh", "bootstrap.sh"]
